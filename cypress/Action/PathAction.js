import PathPage from "../support/PageObject/PathPage";

const pathspage = new PathPage();

class PathAction
{
    //To create path
    create_path(pathdata){
        pathspage.click_path_side_menu();
        pathspage.click_new_path_button();
        pathspage.enter_path_name(pathdata);
        pathspage.click_save_button_at_path_creation_page();
    }

    //To add module into the created path
    add_module_into_created_path(pathdata,modulesdata,basedatas){
        pathspage.click_path_side_menu();
        pathspage.open_created_path(pathdata);
        pathspage.click_modules_tab();
        // pathspage.filter_created_base_on_element_page(basedatas);
        cy.wrap(modulesdata).each((data)=>{
            pathspage.select_module_to_path(data.module_name);
        })
        pathspage.click_save_button_at_add_module_page();
        cy.wrap(modulesdata).each((data)=>{
        pathspage.verify_success_message_after_added_module_into_path(data.module_name);
        })
        // pathspage.click_path_side_menu();
    }
}
export default PathAction