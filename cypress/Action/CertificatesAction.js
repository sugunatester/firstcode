import CertificatesPage from "../support/PageObject/CertificatesPage";

const certificatespage = new CertificatesPage();

class CertificatesAction {

    //To open certificate module
    open_certificates_module(){
        certificatespage.click_certificates_side_menu();
    }
}
export default CertificatesAction