import BasePage from '../support/PageObject/samplepage.js';
import HomePage from '../support/PageObject/HomePage.js';

const homePage = new HomePage;
const basePage = new BasePage;

class HomeAction {

    admin_user_sign_in(admin_user) {
        homePage.click_connection_button();
        homePage.enter_admin_email_id(admin_user);
        homePage.enter_admin_password(admin_user);
        homePage.click_sign_in_button();
    }

    verify_admin_exist_and_deleted(admin_user, common_data) {
        this.admin_user_sign_in(admin_user);
        homePage.verify_and_delete_registered_admin_if_logged(common_data)
    }

    delete_admin_account(admin, link) {
        this.admin_user_sign_in(admin)
        homePage.delete_if_admin_account_is_exist(link)

    }

    delete_admin_account_if_signed(link) {
        cy.wait(5000)
        homePage.delete_if_admin_account_is_exist(link)

    }

    register_as_administraion(admin_user) {
        homePage.click_register_button();
        homePage.enter_email_id_on_registeration_page(admin_user);
        homePage.enter_first_name_on_registeration_page(admin_user);
        homePage.enter_last_name_on_registeration_page(admin_user);
        homePage.enter_password_on_registeration_page(admin_user);
        homePage.enter_confirm_password_on_registeration_page(admin_user);
        homePage.enter_enterprise_name_on_registeration_page(admin_user);
        homePage.click_register_button_on_registeration_page();
    }

    login_deleted_account_and_verify(admindata){
        this.admin_user_sign_in(admindata)
        homePage.verify_error_message_for_unregistered_account()
    }

}


export default HomeAction;

