import UserPage from '../support/PageObject/UserPage.js';
import BasePage from '../support/PageObject/BasePage.js';
import HomePage from '../support/PageObject/HomePage.js';
import HomeAction from './HomeAction.js';
import QuestionnairePage from "../support/PageObject/QuestionnairePage";
import SurveysPage from "../support/PageObject/SurveysPage";


const userpage = new UserPage;
const basepage = new BasePage;
const homepage = new HomePage;
const homeaction = new HomeAction;
const questionnairepage = new QuestionnairePage()
const surveypage = new SurveysPage();


class UserAction{

    rand(){
        userpage.my_random_number()
    }

    invite_user_by_admin(user, mail_sub, randno){
        cy.wait(1000);
        basepage.click_admin_user_menu();
        userpage.click_invite_tab_on_user_page();
        userpage.enter_mail_subject(mail_sub, randno)
        userpage.enter_mail_message(mail_sub)
        // userpage.enter_start_date_as_current_date()
        // userpage.enter_end_date_as_tomorrow_date()
        userpage.click_emails_and_mobile();
        cy.wrap(user).each((data) => {
            userpage.enter_user_email_id_on_user_page_on_invite_window(data.user_name, data.domain);
            userpage.click_save_button_invite_user_page();
            userpage.click_invite_button();
            userpage.verify_invited_user_is_existed(data.user_name, data.domain)
        })
        homepage.log_out_admin();
    }

    register_by_user_self_after_getting_invitation_link(user){
        cy.wrap(user).each((users) => {
            let username = users.user_name;
            let fn = users.first_name;
            let ln = users.last_name;
            let dm = users.domain;
            let pwd = users.password;
            cy.log("user-name is--"+username)
            cy.log("domain is--"+dm)
            cy.log("first name is--"+fn)
            cy.log("last name is--"+ln)
            cy.log("password is--"+pwd)
            userpage.enter_user_first_name_on_user_registeration_window(fn);
            userpage.enter_user_last_name_on_user_registeration_window(ln);
            userpage.enter_user_password_on_user_registeration_window(pwd);
            userpage.enter_user_confirm_password_on_user_registeration_window(pwd);
            userpage.click_register_button_on_user_registeration_window();
            cy.wait(5000)
            userpage.log_out_user();
        })

    }

    login_by_active_user(user){
        homepage.click_connection_button();
        userpage.enter_user_mail_id_on_connection_window(user);
        userpage.enter_user_password_on_connection_window(user);
        userpage.click_sign_in_button_by_user();
        userpage.verify_GDPR_message_is_accept_if_displayed();
    }

    create_one_user_by_admin(user){
        basepage.click_admin_user_menu();
        userpage.click_new_user_button();
        cy.wrap(user).each((data) => {
            userpage.enter_user_first_name_on_create_user_page(data.first_name);
            userpage.enter_user_last_name_on_create_user_page(data.last_name);
            userpage.enter_user_email_id_on_create_user_page(data.user_name, data.domain);
            userpage.enable_email_on_perferred_channel_option();
            userpage.click_save_button_on_create_user_page();
            cy.reload()
            userpage.verify_created_user_is_existed(data.last_name, data.first_name);
        })
    }

    activate_the_created_user_by_admin(user){
        basepage.click_admin_user_menu();
        userpage.click_all_status_filter_field_on_user_list_page()
        userpage.filter_the_created_status_on_user_list_page()
        cy.wrap(user).each((data) => {
            userpage.click_activate_button_on_created_user(data);
            userpage.verify_the_user_is_active(data);
        })
        homepage.log_out_admin();
    }

    activate_the_created_user_by_admin_without_logout(user){
        basepage.click_admin_user_menu();
        cy.wrap(user).each((data) => {
            userpage.click_all_status_filter_field_on_user_list_page()
            userpage.filter_the_created_status_on_user_list_page()
            userpage.click_activate_button_on_created_user(data);
            userpage.verify_the_user_is_active(data);
        })

    }

    login_by_user_on_experquiz_account(user,settingdata) {
        userpage.click_connection_button()
        cy.wait(5000)
        cy.wrap(user).each((data) => {
            userpage.enter_user_mail_id_on_connection_window(data.user_name, data.domain)
            userpage.enter_user_password_on_connection_window(data.password)
            userpage.click_sign_in_button_by_user()
            cy.wait(5000)
            userpage.verify_GDPR_message_is_accept_if_displayed(settingdata)
            userpage.log_out_user()
        })
    }

    login_by_user_on_experquiz_account_without_logout(user) {
        cy.reload()
        userpage.click_connection_button()
        cy.wait(5000)
        cy.wrap(user).each((data) => {
            userpage.enter_user_mail_id_on_connection_window(data.user_name, data.domain)
            userpage.enter_user_password_on_connection_window(data.password)
            userpage.click_sign_in_button_by_user()
        })
    }

    login_by_user_after_getting_password(user, settingdata, user_password) {
        // cy.log(Cypress.env('my_passwords'))
        userpage.click_connection_button()
        cy.wrap(user).each((data) => {
            userpage.enter_user_mail_id_on_connection_window(data.user_name, data.domain)
        })
        // userpage.enter_user_password_on_connection_window_after_getting_password()
        userpage.enter_user_password_on_connection_window_after_getting_password_from_email_and_sms_log(user_password)
        userpage.click_sign_in_button_by_user()
        userpage.verify_GDPR_message_is_accept_if_displayed(settingdata)
        userpage.log_out_user()
    }

    login_by_user_after_getting_password_without_logout(user, user_password) {
        userpage.click_connection_button()
        cy.wrap(user).each((data) => {
            userpage.enter_user_mail_id_on_connection_window(data.user_name, data.domain)
        })
        // userpage.enter_user_password_on_connection_window_after_getting_password()
        userpage.enter_user_password_on_connection_window_after_getting_password_from_email_and_sms_log(user_password)
        userpage.click_sign_in_button_by_user()

    }

    click_questionnaire_invitation_from_userdashboard_page(questionnaire){
        userpage.click_questionnaire_invitation(questionnaire)
    }


    click_free_to_play_questionnaire_from_userdashboard_page(questionnaire){
        userpage.click_free_to_play_questionnaire(questionnaire)
    }

    click_survey_invitation_card_from_user_dashboard_page(surveydata){
        userpage.click_survey_invitation(surveydata)
    }

    verify_cycle_event_is_exist_in_userdashboard_page(cycle){
        cy.wait(50000)
        cy.reload()
        let steps = cycle.cycle_step
        cy.wrap(steps).each((stepdata, index)=> {
            cy.log('my type is-----', stepdata.type)
            if (stepdata.type === "questionnaire") {
                userpage.verify_cycle_event_of_questionnaire_invitation_is_exist(stepdata)
            }
            else if (stepdata.type === "form") {
                userpage.verify_cycle_event_of_survey_invitation_is_exist(stepdata)
            }
        })
        userpage.log_out_user()

    }

    verify_all_side_menu_is_exist_on_user_account(){
        userpage.verify_all_user_side_menu_is_exist()
        userpage.log_out_user()
    }

    create_group(group){
        basepage.click_admin_user_menu()
        userpage.click_group_tab_on_user_list_page()
        userpage.click_create_group_button()
        userpage.enter_group_name(group)
        userpage.click_create_button_on_group_popup()
    }

    assign_user_on_created_group(group, usersdatas){
        basepage.click_admin_user_menu()
        userpage.click_group_tab_on_user_list_page()
        userpage.click_group_filter_field()
        userpage.select_created_group(group)
        userpage.assign_user(usersdatas)
        userpage.click_save_button_after_user_assign()
        basepage.click_admin_user_menu()
        userpage.click_group_tab_on_user_list_page()
        userpage.verify_user_on_created_group(group, usersdatas)
    }

    verify_deleted_ongoing_cycle_is_exist_on_user_dashboard_page(cycle) {
        let steps = cycle.cycle_step
        cy.wrap(steps).each((stepdata, index)=> {
            cy.log('my type is-----', stepdata.type)
            if (stepdata.type === "questionnaire") {
                cy.xpath("//div[normalize-space(text())='" + stepdata["questionnaire_name"] + "']")
                    .should('not.exist')
            }
            else if (stepdata.type === "form") {
                cy.xpath("//div[normalize-space(text())='" + stepdata["form_name"] + "']")
                    .should('not.exist')
            }
        })
        userpage.log_out_user()
    }

    verify_GDPR_for_contact_user_and_accept_GDPR_message_while_playing_test(settingdata){
        userpage.verify_GDPR_for_contact_user_and_accept_GDPR_message(settingdata)

    }

    play_invited_practice_training_by_active_user(usersdatas, trainingdata, training_ques){
        userpage.click_training_invitation_from_active_user_dashboard(trainingdata)
        // userpage.accept_training_invitation_by_active_user()
        userpage.click_questionnaire_on_practice_section_of_training(training_ques)
    }

    play_invited_practice_cycle_training_by_active_user(usersdatas, trainingdata, cy_ques){
        userpage.click_training_invitation_from_active_user_dashboard(trainingdata)
        // userpage.accept_training_invitation_by_active_user()
        userpage.click_cycle_questionnaire_on_practice_section_of_training(cy_ques)
    }

    play_invited_evaluation_training_by_active_user(usersdatas, trainingdata, training_ques){
        userpage.click_training_invitation_from_active_user_dashboard(trainingdata)
        userpage.click_questionnaire_on_evaluation_section_of_training(training_ques)
    }

    define_boss_for_admin_user(userdata, admindata){
        basepage.click_admin_user_menu();
        userpage.click_edit_user_button(userdata)
        userpage.click_define_boss_button_on_edit_user_page()
        userpage.select_boss_for_admin(admindata)
        userpage.verify_boss_user_is_added_in_hierarchy_section(admindata)
        userpage.click_save_button_on_create_user_page()
    }

    play_cycle_event_on_user_dashboard_page(cycle, question, survey){
        let cyc = cycle.cycle_step
        cy.wrap(cyc).each((stepdata)=> {
            cy.log('my type is-----', stepdata.type)
            if(stepdata.type === "questionnaire") {
                userpage.click_questionnaire_invitation(stepdata)
                questionnairepage.verify_start_button_before_playing_static_questionnaire()
                questionnairepage.select_answers_in_static_player_by_active_user(question)
            }
            if(stepdata.type === "form")
            {
                userpage.click_survey_invitation(stepdata)
                surveypage.fill_form(survey)
            }
        })
        userpage.log_out_user()
    }





}
export default UserAction;

