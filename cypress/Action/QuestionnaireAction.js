import QuestionnairePage from '../support/PageObject/QuestionnairePage.js';
import AdminDashboardPage from '../support/PageObject/AdminDashboardPage.js';
import HomePage from '../support/PageObject/HomePage.js';
import UserPage from "../support/PageObject/UserPage";
import SurveysPage from "../support/PageObject/SurveysPage";

const questionnairepage = new QuestionnairePage;
const admindashboardpage = new AdminDashboardPage();
const homepage = new HomePage();
const userpage = new UserPage()
const surveypage = new SurveysPage();


class QuestionnaireAction {

        create_static_type_questionnaire(basedatas, questionnaire) {
        admindashboardpage.click_admin_questionnaire_menu()
        questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
        questionnairepage.click_new_questionnaire_button()
        questionnairepage.enter_questionnaire_name(questionnaire)
        questionnairepage.select_static_type_questionnaire()
        questionnairepage.click_create_button_on_questionnaire_create_page()
    }

    create_two_static_type_questionnaire(basedatas, questionnaire) {
        cy.wrap(questionnaire).each((ques)=>{
            admindashboardpage.click_admin_questionnaire_menu()
            questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
            questionnairepage.click_new_questionnaire_button()
            questionnairepage.enter_questionnaire_name(ques)
            questionnairepage.select_static_type_questionnaire()
            questionnairepage.click_create_button_on_questionnaire_create_page()
        })

    }

    create_dynamic_type_questionnaire(basedatas, questionnaire) {
        admindashboardpage.click_admin_questionnaire_menu()
        questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
        questionnairepage.click_new_questionnaire_button()
        questionnairepage.enter_questionnaire_name(questionnaire)
        questionnairepage.select_dynamic_type_questionnaire()
        questionnairepage.click_create_button_on_questionnaire_create_page()
    }

    create_three_dynamic_questionnaire(basedatas, questionnaire) {
        cy.wrap(questionnaire).each((ques)=> {
            admindashboardpage.click_admin_questionnaire_menu()
            questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
            questionnairepage.click_new_questionnaire_button()
            questionnairepage.enter_questionnaire_name(ques)
            questionnairepage.select_dynamic_type_questionnaire()
            questionnairepage.click_create_button_on_questionnaire_create_page()
        })
    }

    select_all_question_on_created_static_type_questionnaire(basedatas, questionnaire){
        admindashboardpage.click_admin_questionnaire_menu()
        questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
        questionnairepage.open_created_questionnaire(questionnaire)
        questionnairepage.click_selection_tab()
        questionnairepage.select_all_question()
        questionnairepage.click_save_button_on_selection_tab()
        questionnairepage.verify_success_message_after_questionnaire_saved(questionnaire);
    }

    select_all_question_on_created_two_static_type_questionnaire(basedatas, questionnaire){
        cy.wrap(questionnaire).each((ques)=> {
            admindashboardpage.click_admin_questionnaire_menu()
            questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
            questionnairepage.open_created_questionnaire(ques)
            questionnairepage.click_selection_tab()
            questionnairepage.select_all_question()
            questionnairepage.click_save_button_on_selection_tab()
            questionnairepage.verify_success_message_after_questionnaire_saved(ques);
        })
    }

    enter_question_count_on_created_dynamic_type_questionnaire(basedatas, questionnaire){
        admindashboardpage.click_admin_questionnaire_menu()
        questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
        questionnairepage.open_created_questionnaire(questionnaire)
        questionnairepage.click_question_tab_on_opened_questionnaire()
        questionnairepage.enter_question_count_on_created_dynamic_questionnaire(questionnaire)
        questionnairepage.click_save_button_on_question_tab_after_enter_question_count()
    }

    enter_question_count_on_created_three_dynamic_type_questionnaire(basedatas, questionnaire){
        cy.wrap(questionnaire).each((ques)=> {
            admindashboardpage.click_admin_questionnaire_menu()
            questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
            questionnairepage.open_created_questionnaire(ques)
            questionnairepage.click_question_tab_on_opened_questionnaire()
            questionnairepage.enter_question_count_on_created_dynamic_questionnaire(ques)
            questionnairepage.click_save_button_on_question_tab_after_enter_question_count()
        })
    }

    play_static_trial_questionnaire(basedatas, questiondatas, questionnaire, settingdata){
        cy.wait(1000)
        admindashboardpage.click_admin_questionnaire_menu()
        questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
        questionnairepage.click_trial_play_questionnaire_button(questionnaire)
        userpage.verify_GDPR_message_is_accept_if_displayed(settingdata)
        questionnairepage.verify_start_button_before_playing_static_questionnaire()
        questionnairepage.select_answers_in_static_player_by_active_user(questiondatas)

    }

    play_dynamic_trial_questionnaire(basedatas, question, questionnaire, settingdata) {
        cy.wait(1000)
        admindashboardpage.click_admin_questionnaire_menu()
        questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
        questionnairepage.click_trial_play_questionnaire_button(questionnaire)
        userpage.verify_GDPR_message_is_accept_if_displayed(settingdata)
        questionnairepage.verify_start_button_before_playing_dynamic_questionnaire(question)
        questionnairepage.selectAnswersInPlayerForDynamicQuestionnaires(question, questionnaire)

    }

    select_answer_on_playing_static_questionnaire_by_contact_user(question){
        questionnairepage.verify_start_button_before_playing_static_questionnaire(question)
        questionnairepage.select_answers_in_static_player_by_contact_user(question)
    }

    select_answer_on_playing_static_questionnaire_by_active_user(question){
        questionnairepage.verify_start_button_before_playing_static_questionnaire(question)
        questionnairepage.select_answers_in_static_player_by_active_user(question)
        userpage.log_out_user()
    }

    select_answer_on_training_questionnaire_test_of_static_questionnaire_by_active_user(question){
        questionnairepage.verify_start_button_before_playing_static_questionnaire(question)
        questionnairepage.select_answers_in_training_questionnaire_of_static_player_by_active_user(question)
        userpage.log_out_user()
    }

    select_answer_on_static_questionnaire_from_mail_account(question, settingdata){
        questionnairepage.verify_start_button_before_playing_static_questionnaire(question)
        userpage.verify_GDPR_message_is_accept_if_displayed(settingdata)
        questionnairepage.select_answers_in_static_player_by_contact_user(question)
    }

    select_answer_on_playing_dynamic_questionnaire(question, questionnaire){
        questionnairepage.verify_start_button_before_playing_static_questionnaire(question)
        questionnairepage.selectAnswersInPlayerForDynamicQuestionnaires(question, questionnaire)
        userpage.log_out_user()
    }

    playing_public_dynamic_questionnaire_by_guest_user(question, questionnaire, settingdata){
        questionnairepage.verify_start_button_before_playing_static_questionnaire(question)
        userpage.verify_GDPR_for_contact_user_and_accept_GDPR_message(settingdata)
        questionnairepage.selectAnswersInPlayerForDynamicQuestionnaires(question, questionnaire)
        userpage.click_exit_button_on_questionnaire_testscore_page_while_contact_user_play_test()
    }

    invite_questionnaire_to_the_user(basedatas, questionnaire, user, mail_sub, randno) {
        admindashboardpage.click_admin_questionnaire_menu()
        questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
        questionnairepage.open_created_questionnaire(questionnaire)
        questionnairepage.click_invitation_tab_on_open_questionnaire()
        questionnairepage.enter_mail_subject(mail_sub, randno)
        questionnairepage.enter_mail_message(mail_sub)
        questionnairepage.enter_start_date_as_current_date()
        questionnairepage.enter_end_date_as_tomorrow_date()
        questionnairepage.click_emails_and_mobile_button()
        cy.wrap(user).each((data) => {
            questionnairepage.enter_active_user_mail_id(data)
        })
        questionnairepage.click_save_button_on_emails_and_mobile_window()
        questionnairepage.click_invite_button_on_questionnaire_invitation_page()
        homepage.log_out_admin()

    }

    invite_questionnaire_to_the_contact_user(basedatas, questionnaire, contact, randno, mail_sub) {
        admindashboardpage.click_admin_questionnaire_menu()
        questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
        questionnairepage.open_created_questionnaire(questionnaire)
        questionnairepage.click_invitation_tab_on_open_questionnaire()
        questionnairepage.enter_mail_subject(mail_sub, randno)
        questionnairepage.enter_mail_message(mail_sub)
        questionnairepage.enter_start_date_as_current_date()
        questionnairepage.enter_end_date_as_tomorrow_date()
        questionnairepage.click_emails_and_mobile_button()
        questionnairepage.enter_contact_user_mail_id(contact)
        questionnairepage.click_save_button_on_emails_and_mobile_window()
        questionnairepage.click_invite_button_on_questionnaire_invitation_page()
        homepage.log_out_admin()

    }

    invite_questionnaire_to_the_user_by_clicking_select_user_button(basedatas, questionnaire, user, randno, mail_sub) {
        admindashboardpage.click_admin_questionnaire_menu()
        questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
        questionnairepage.open_created_questionnaire(questionnaire)
        questionnairepage.click_invitation_tab_on_open_questionnaire()
        questionnairepage.enter_mail_subject(mail_sub, randno)
        questionnairepage.enter_mail_message(mail_sub)
        questionnairepage.enter_start_date_as_current_date()
        questionnairepage.enter_end_date_as_tomorrow_date()
        questionnairepage.click_select_user_button_on_questionnaire_invitation_page()
        questionnairepage.select_activated_user_on_questionnaire_invitation_page(user)
        questionnairepage.click_validate_buttton_on_questionnaire_invitation_page()
        questionnairepage.click_invite_button_on_questionnaire_invitation_page()
        homepage.log_out_admin()

    }

    verify_questionnaire_result_for_active_user(basedatas,questionnairedata, userdata){
        admindashboardpage.click_admin_questionnaire_menu()
        questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
        questionnairepage.open_created_questionnaire(questionnairedata)
        questionnairepage.click_result_tab_on_questionnaire_result_page()
        questionnairepage.click_per_evaluation_tab_on_questionnaire_result_page()
        questionnairepage.verify_questionnaire_result_for_active(questionnairedata, userdata)
    }

    verify_questionnaire_result_for_contact_user(basedatas,questionnairedata, contactuser){
        admindashboardpage.click_admin_questionnaire_menu()
        questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
        questionnairepage.open_created_questionnaire(questionnairedata)
        questionnairepage.click_result_tab_on_questionnaire_result_page()
        questionnairepage.click_per_evaluation_tab_on_questionnaire_result_page()
        questionnairepage.verify_questionnaire_result_for_contact(questionnairedata, contactuser)
    }

    verify_public_questionnaire_result_for_guest_user(basedatas,questionnairedata){
        admindashboardpage.click_admin_questionnaire_menu()
        questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
        questionnairepage.open_created_questionnaire(questionnairedata)
        questionnairepage.click_result_tab_on_questionnaire_result_page()
        questionnairepage.click_free_and_public_tests_tab_on_questionnaire_result_page()
        questionnairepage.verify_public_result_for_guest_user(questionnairedata)
    }

    verify_result_for_active_user_played_visibility_questionnaire(basedatas,questionnairedata, userdata){
        admindashboardpage.click_admin_questionnaire_menu()
        questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
        questionnairepage.open_created_questionnaire(questionnairedata)
        questionnairepage.click_result_tab_on_questionnaire_result_page()
        questionnairepage.click_free_and_public_tests_tab_on_questionnaire_result_page()
        questionnairepage.verify_visibility_result_for_active_user(questionnairedata, userdata)
    }

    create_public_questionnaire(basedatas, questionnairedata, public_ques, link){
        admindashboardpage.click_admin_questionnaire_menu()
        questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
        questionnairepage.open_created_questionnaire(questionnairedata)
        questionnairepage.click_the_visibility_tab_on_opened_questionnaire()
        questionnairepage.click_the_make_it_public_button()
        questionnairepage.enter_public_link(public_ques)
        questionnairepage.enter_return_link(link)
        questionnairepage.make_public_button_on_create_public_popup()

    }

    copy_the_public_link_on_quetionnaire_visibility_page(basedatas,questionnairedata){
        admindashboardpage.click_admin_questionnaire_menu()
        questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
        questionnairepage.open_created_questionnaire(questionnairedata)
        questionnairepage.click_the_visibility_tab_on_opened_questionnaire()
        questionnairepage.get_the_public_link_using_the_QR_code()
        homepage.log_out_admin()
    }

    create_visbility_questionnaire(basedatas, questionnairedata, group){
        admindashboardpage.click_admin_questionnaire_menu()
        questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
        questionnairepage.open_created_questionnaire(questionnairedata)
        questionnairepage.click_the_visibility_tab_on_opened_questionnaire()
        questionnairepage.created_group_checked(group)
        questionnairepage.click_save_button_on_questionnaire_visibility_page()
    }

    select_survey_form_on_created_questionnaire(basedatas, questionnairedatas, simple_surveydata){
        admindashboardpage.click_admin_questionnaire_menu()
        questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
        questionnairepage.open_created_questionnaire(questionnairedatas)
        questionnairepage.click_options_tab_on_opened_questionnaire()
        questionnairepage.select_form_on_questionnaire_option_page(simple_surveydata)
        questionnairepage.click_save_on_questionnaire_option_page()
    }

    trialplay_questionnaire_with_survey_form(basedatas, simple_surveydata, questionnaire, questiondatas){
        admindashboardpage.click_admin_questionnaire_menu()
        questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
        questionnairepage.click_trial_play_questionnaire_button(questionnaire)
        surveypage.fill_form(simple_surveydata)
        questionnairepage.verify_start_button_before_playing_static_questionnaire(questiondatas)
        questionnairepage.selectAnswersInPlayerForDynamicQuestionnaires(questiondatas, questionnaire)
        surveypage.fill_form(simple_surveydata)
        questionnairepage.click_back_to_questionnaire_button_on_test_score_page()
        userpage.log_out_user()

    }


    contact_user_play_questionnaire_with_survey_form(simple_surveydata, questionnaire, questiondatas){
        surveypage.fill_form(simple_surveydata)
        questionnairepage.verify_start_button_before_playing_static_questionnaire(questiondatas)
        questionnairepage.selectAnswersInPlayerForDynamicQuestionnaires(questiondatas, questionnaire)
        surveypage.fill_form(simple_surveydata)
        userpage.click_exit_button_on_questionnaire_testscore_page_while_contact_user_play_test()
    }

}
export default QuestionnaireAction;