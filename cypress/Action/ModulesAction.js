import ModulesPage from "../support/PageObject/ModulesPage";
import questionnaireAction from "./QuestionnaireAction";
import modulesPage from "../support/PageObject/ModulesPage";
import moment from "moment";
import HomePage from "../support/PageObject/HomePage";

const modulespage = new ModulesPage() ;
const homepage = new HomePage();



class ModulesAction
{
    //To create modules
    create_module(modulesdata){
        modulespage.click_modules_side_menu();
        modulespage.click_new_modules_button();
        cy.wrap(modulesdata).each((data)=>{
            modulespage.enter_module_name(data.module_name);
        })
        modulespage.click_save_button_at_module_creation_page();
    }

    //To add element into the created module
    add_element_into_created_module(basedatas, modulesdata){
        modulespage.click_modules_side_menu();
        cy.wrap(modulesdata).each((ele)=>{
            modulespage.open_created_module(ele.module_name);
            modulespage.click_elements_tab();
            let element = ele.elements
            cy.wrap(element).each((data)=> {
                cy.log(data.name)
                if (data.type === "questionnaire") {
                    modulespage.click_select_questionnaire_tab_on_module_elements_page()
                    modulespage.filter_created_base_on_element_page(basedatas);
                    modulespage.select_questionnaire_to_module(data);
                    modulespage.verify_success_message_after_added_element_into_module();
                }
                else {
                    modulespage.click_select_media_tab_on_module_elements_page()
                    modulespage.filter_created_base_on_element_page(basedatas);
                    modulespage.select_image_on_module_steps_page(data)
                    modulespage.verify_success_message_after_added_element_into_module();
                }
            })
        })
    }

    click_trial_play_button_on_created_module(moduledata){
        modulespage.click_modules_side_menu();
        cy.wrap(moduledata).each((ele)=>{
            modulespage.click_module_trial_play_button(ele)
        })
    }

    answer_on_module_player_page(module, question) {
        cy.wrap(module).each((ele)=> {
            let step = ele.elements
            let ele_count = Cypress.$(step).length
            cy.log(ele_count)
            cy.wrap(step).each((data, index) => {
                cy.log('my index value is:',index)
                modulespage.play_module(data, question)
                if (index < ele_count -1)
                {
                    modulespage.click_next_step_button_on_module_player_page()

                }
                else
                {
                    modulespage.close_module_test()
                    modulespage.click_close_button_on_confirmation_popup()
                }
            })
        })
    }

    set_questionnaire_usage(base, module){
        modulespage.click_modules_side_menu();
        cy.wrap(module).each((ele)=> {
            modulespage.open_created_module(ele.module_name);
            modulespage.click_steps_tab_on_module()
            modulespage.enable_practice_usage_on_module_steps_page()
            modulespage.enable_evaluation_usage_on_module_steps_page()
            modulespage.enable_certification_usage_on_module_steps_page()
            modulespage.click_save_button_on_module_steps_page()
        })
    }

    invite_module_to_active_user(module, user, randno, mail_sub){
        modulespage.click_modules_side_menu();
        cy.wrap(module).each((ele)=> {
            modulespage.open_created_module(ele.module_name);
            modulespage.click_invitation_tab_on_module()
            modulespage.enter_mail_subject(mail_sub, randno)
            modulespage.enter_mail_message(mail_sub)
            modulespage.enter_start_date_as_current_date()
            modulespage.enter_end_date_as_tomorrow_date()
            modulespage.click_select_user_button_on_module_invitation_page()
            modulespage.select_activated_user_on_module_invitation_page(user)
            modulespage.click_validate_buttton_on_module_invitation_page()
            modulespage.click_invite_button_on_module_invitation_page()
            homepage.log_out_admin()
        })
    }


}
export default ModulesAction