import MediaAction from "./MediaAction";
import BasePage from '../support/PageObject/BasePage';
import QuestionPage from '../support/PageObject/QuestionPage';
import QuestionnaireAction from '../Action/QuestionnaireAction';
import UserPage from "../support/PageObject/UserPage";
import AdminDashboardPage from '../support/PageObject/AdminDashboardPage';
import QuestionnairePage from '../support/PageObject/QuestionnairePage.js';
import ModulesPage from "../support/PageObject/ModulesPage";
import QuestionnaireAction from '../Action/QuestionnaireAction';
import MediaPage from '../support/PageObject/MediaPage';
import TrainingsPage from '../support/PageObject/TrainingsPage';

const basepage = new BasePage();
const questionpage = new QuestionPage();
const questionnaireaction = new QuestionnaireAction();
const mediaaction = new MediaAction();
const trainingspage = new TrainingsPage();
const userpage = new UserPage()
const mediapage = new MediaPage()
const questionnairepage = new QuestionnairePage;
const modulespage = new ModulesPage();
const adminDashboardPage =new AdminDashboardPage()

class BaseAction {

    create_base_with_single_domain_and_verify(base) {
        basepage.click_admin_base_menu();
        basepage.click_new_base_button();
        basepage.enter_base_name(base);
        basepage.enter_domain_name(base["domains"]);
        basepage.save_create_base_page();
        basepage.verify_created_base_exist(base)
    }


    create_base_without_domain_and_verify(base){
        basepage.click_admin_base_menu();
        basepage.click_new_base_button();
        basepage.enter_base_name(base);
        basepage.save_create_base_page();
        basepage.verify_created_base_exist(base)
    }

    create_question_on_created_base(base, question){
        basepage.click_admin_base_menu();
        basepage.open_created_base(base);
        basepage.click_questions_tab();
        basepage.click_new_question_button();
        const QuestionPage = require('../support/PageObject/QuestionPage')
        questionpage.create_question(base, question);
    }

    create_all_question_with_free_on_created_base(base, question){
        basepage.click_admin_base_menu();
        basepage.open_created_base(base);
        basepage.click_questions_tab();
        basepage.click_new_question_button();
        questionpage.create_question_with_free_form(base, question);
    }

    create_question_with_same_rank_on_created_base(base, question){
        basepage.click_admin_base_menu();
        basepage.open_created_base(base);
        basepage.click_questions_tab();
        basepage.click_new_question_button();
        questionpage.create_question_with_same_rank_and_verify_it_error_message(base, question);
    }

    upload_sample_question_file_through_base(base) {
        basepage.click_admin_base_menu();
        basepage.open_created_base(base);
        basepage.click_import_and_export_tab()
        basepage.click_to_download_the_sample_question_file()
        basepage.verify_file_is_downloaded()
        basepage.click_upload_file_button()
        basepage.click_import_and_export_tab()
        basepage.reload_import_page()
        basepage.CheckImportedFileStatus()
        basepage.verify_imported_question_is_in_base_of_question_page()
    }

    create_questionnaire_on_created_base_page(base, questionnaire){
        basepage.click_admin_base_menu();
        basepage.open_created_base(base);
        basepage.click_questionnaire_tab();
        basepage.click_new_questionnaire_button()
        questionnaireaction.create_static_type_questionnaire(questionnaire)
    }

    select_question_on_static_type_questionnaire(base, questionnaire){
        basepage.click_admin_base_menu();
        basepage.open_created_base(base);
        basepage.click_questionnaire_tab();
        questionnaireaction.select_all_question_on_created_static_type_questionnaire(questionnaire)
    }

    enter_question_count_on_dynamic_questionnaire(base, questionnaire){
        basepage.click_admin_base_menu();
        basepage.open_created_base(base);
        basepage.click_questionnaire_tab();
        questionnaireaction.select_all_question_on_created_static_type_questionnaire(questionnaire)
    }

    create_question_by_domain_selection(base){
        cy.wait(5000)
        // basepage.click_admin_base_menu();
        // basepage.open_created_base(base);
        // basepage.click_questions_tab();
        // basepage.click_new_question_button();
        questionpage.create_question_with_random_domain_selection(base["domains"]);
    }

    create_media_on_base(base, mediadata){
        basepage.click_admin_base_menu();
        basepage.open_created_base(base);
        basepage.click_media_tab_on_base()
        mediaaction.create_media_on_basepage(mediadata)
    }
    createBaseWithTag(basedatas, basedatas1){
        basepage.click_admin_base_menu();
        basepage.click_new_base_button();
        basepage.enter_base_name(basedatas)
        basepage.enterTag(basedatas)
        basepage.save_create_base_page()
        basepage.click_new_base_button();
        basepage.enter_base_name(basedatas1)
        basepage.save_create_base_page()
    }
    verifyToFilterwithTag(basedatas){
        basepage.entertagfield(basedatas);
        basepage.verifyTheBase(basedatas);
        basepage.selectTheBase(basedatas);
        basepage.clickArchiveBtn();
        basepage.chooseStatusFilter();
        basepage.selectAllStatus();
        basepage.verifyAllCountOfArchivedAndActiveBases();
    }
    verifyTheActiveFilterInBase(basedatas) {
        basepage.click_admin_base_menu();
        basepage.chooseStatusFilter();
        basepage.selectArchiveOption();
        basepage.verifyTheArchivedBase(basedatas);
        basepage.selectTheArchivedBase(basedatas);
        basepage.clickUnarchiveBtn();
        basepage.chooseStatusFilter();
        //basepage.verify_the_base_after_unarchive(basedatas);
        basepage.selectActiveOption();
        basepage.verifyTheBase(basedatas);
    }

    logout(){
        adminDashboardPage.log_out_admin()
    }
    verifyTheArchivedBaseInQuestionnaire(basedatas, questionnairedatas) {
        admindashboardpage.click_admin_questionnaire_menu();
        questionnairepage.click_topic_filter_field();
        basepage.verify_Created_base_on_topic_filter_field(basedatas);
        basepage.verifyArchivedQuestionnaire(basedatas, questionnairedatas)
    }
    verifyTheArchivedBaseInQuestions(basedatas, questiondatas) {
        admindashboardpage.click_admin_question_menu();
        questionpage.click_topic_filter_field_on_question_list_page();
        basepage.verify_Created_base_on_topic_filter_field(basedatas);
        basepage.verifyCreatedBaseQuestions(questiondatas)
    }
    verifyTheArchivedBaseInMedia(basedatas, mediadata) {
        mediapage.click_media_side_menu();
        basepage.clickEnterPriseFolderDD();
        basepage.verify_Created_base_on_topic_filter_field(basedatas)
        basepage.verifyCreatedBaseMedias(mediadata)
    }
    verifyTheArchivedBaseInTraining(basedatas, training) {
        trainingspage.click_trainings_side_menu()
        basepage.click_topic_filter_field_on_Training_page()
        basepage.verify_Created_base_on_topic_filter_field(basedatas)
        basepage.verifyCreatedBaseTraining(training)
    }
    verifyTheCreatedBaseDetails(basedatas, admindatas) {

        basepage.verify_created_base_exist(basedatas)
        basepage.clickBaseExpandIcon(basedatas);
        basepage.verifyTheAuthorName(admindatas)
        basepage.verifytheCreatedDomainName(basedatas)
        basepage.clickCollapseIcon();
    }
    editBaseDetails(basedatas, baseeditdatas, without_mail_user) {
        basepage.click_admin_base_menu();
        basepage.open_created_base(basedatas)
        basepage.clickEditBaseIcon();
        basepage.edit_base_name(baseeditdatas)
        basepage.clickAuthorField()
        basepage.chooseUser(without_mail_user)
        basepage.clearEnteredDomain()
        basepage.enter_domain_name(baseeditdatas["domains"])
        basepage.save_create_base_page();


    }
    verifyEditedBaseDetails(baseeditdatas, without_mail_user) {
        basepage.click_admin_base_menu();
        basepage.verify_created_base_exist(baseeditdatas)
        basepage.clickBaseExpandIcon(baseeditdatas);
        basepage.verifyTheEditedAuthorName(without_mail_user)
        basepage.verifyTHeDomainName(baseeditdatas["domains"])
        basepage.clickCollapseIcon();
    }

    create_module(modulesdata) {
        basepage.clickModuleTabOnBase();
        basepage.clickNewModuleButton();
        cy.wrap(modulesdata).each((data) => {
            modulespage.enter_module_name(data.module_name);
        })
        modulespage.click_save_button_at_module_creation_page();
    }
    add_element_into_created_module(basedatas, modulesdata) {
        cy.wrap(modulesdata).each((ele) => {
            modulespage.open_created_module(ele.module_name);
            modulespage.click_elements_tab();
            let element = ele.elements
            cy.wrap(element).each((data) => {
                cy.log(data.name)
                if (data.type === "questionnaire") {
                    modulespage.click_select_questionnaire_tab_on_module_elements_page()
                    modulespage.filter_created_base_on_element_page(basedatas);
                    modulespage.select_questionnaire_to_module(data);
                    modulespage.verify_success_message_after_added_element_into_module();
                }
                else {
                    modulespage.click_select_media_tab_on_module_elements_page()
                    modulespage.filter_created_base_on_element_page(basedatas);
                    modulespage.select_image_on_module_steps_page(data)
                    modulespage.verify_success_message_after_added_element_into_module();
                }
            })
        })

    }
    deleteCreatedBase(basedatas) {
        basepage.click_admin_base_menu();
        basepage.clickDeleteIcon(basedatas);
        basepage.clickDeleteButton();
    }
    verifyDeletedBaseInQuestionnaire(basedatas, questionnairedatas) {
        admindashboardpage.click_admin_questionnaire_menu();
        questionnairepage.click_topic_filter_field();
        basepage.verify_Created_base_on_topic_filter_field(basedatas);
        basepage.verifyArchivedQuestionnaire(basedatas, questionnairedatas)
    }
    verifyDeletedBaseInQuestions(basedatas, questiondatas) {
        admindashboardpage.click_admin_question_menu();
        questionpage.click_topic_filter_field_on_question_list_page();
        basepage.verify_Created_base_on_topic_filter_field(basedatas);
        basepage.verifyCreatedBaseQuestions(questiondatas)
    }
    verifyDeletedBaseInMedia(basedatas, mediadata) {
        mediapage.click_media_side_menu();
        basepage.clickEnterPriseFolderDD();
        basepage.verify_Created_base_on_topic_filter_field(basedatas)
        basepage.verifyCreatedBaseMedias(mediadata)
    }
    verifyDeletedBaseInTraining(basedatas, training) {
        trainingspage.click_trainings_side_menu()
        basepage.click_topic_filter_field_on_Training_page()
        basepage.verify_Created_base_on_topic_filter_field(basedatas)
        basepage.verifyCreatedBaseTraining(training)
    }
    verifyDeletedBaseInModules() {
        modulespage.click_modules_side_menu();
        basepage.verifyDeletedModuleName();

    }
    create_trainings_without_cycle(basedatas, training, admindatas) {
        basepage.click_admin_base_menu();
        basepage.open_created_base(basedatas);
        basepage.clickTrainingModule();
        trainingspage.click_new_training_button();
        trainingspage.enter_training_name(training)
        trainingspage.enter_start_date_as_current_date_in_trainings();
        trainingspage.enter_end_date_as_tomorrow_date_in_trainings();
        trainingspage.click_trainer_field()
        trainingspage.choose_trainer(admindatas)
        trainingspage.click_save_button_at_training_creation_page();
        trainingspage.verify_created_training(training, basedatas);

    }
    drag_questionnaire_on_practice_section_in_training(basedatas, trainingdata, training_ques) {
        trainingspage.open_created_training(basedatas, trainingdata);
        trainingspage.click_questionnaire_tab_on_opened_training()
        trainingspage.drag_questionnaire_to_practice_section_on_training(training_ques)
    }
    add_participants_into_created_training(usersdatas) {
        trainingspage.click_invitation_tab();
        trainingspage.click_select_participants_button();
        cy.wrap(usersdatas).each((data) => {
            trainingspage.choose_participants(data.first_name, data.last_name);
        })
        trainingspage.click_validate_button();
    }
    DragandDrop_present_user_to_reg_user_section_on_training_participant_page(user) {
        trainingspage.click_participants_tab()
        trainingspage.drag_and_drop_user_on_present_section(user)
    }
    invite_selected_participant_on_training(usersdatas) {
        trainingspage.click_invitation_tab()
        cy.wrap(usersdatas).each((data) => {
            trainingspage.selected_participant_is_on_not_send_status(data.first_name, data.last_name);
            trainingspage.click_send_invitation_button()
            trainingspage.verify_user_is_on_send_status_after_inviting_on_training(data.first_name, data.last_name)
        })
        userpage.log_out_user()

    }






}
export default BaseAction;


    
 
