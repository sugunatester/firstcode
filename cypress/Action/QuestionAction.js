import QuestionPage from "../support/PageObject/QuestionPage.js";
import AdminDashboardPage from "../support/PageObject/AdminDashboardPage.js";
import HomePage from "../support/PageObject/HomePage.js";


const questionpage = new QuestionPage()
const admindashboardpage = new AdminDashboardPage()
const homepage = new HomePage()
class QuestionAction {

    edit_create_question_on_question_list_page(base, question) {
        admindashboardpage.click_admin_question_menu()
        questionpage.click_topic_filter_field_on_question_list_page()
        questionpage.select_all_topic_option_on_question_list_page()
        questionpage.click_topic_filter_field_on_question_list_page()
        questionpage.select_base_option_on_question_list_page(base)
        questionpage.edit_created_question(base, question)
    }

    play_each_create_question_on_question_list_page(basedata, questiondatas) {
        admindashboardpage.click_admin_question_menu()
        questionpage.click_topic_filter_field_on_question_list_page()
        questionpage.select_all_topic_option_on_question_list_page()
        questionpage.click_topic_filter_field_on_question_list_page()
        questionpage.select_base_option_on_question_list_page(basedata)
        questionpage.select_single_type_question_answers_in_player_without_free_question(questiondatas)
    }

    play_each_create_question_on_question_list_page_with_free(basedata, questiondatas) {
        admindashboardpage.click_admin_question_menu()
        questionpage.click_topic_filter_field_on_question_list_page()
        questionpage.select_all_topic_option_on_question_list_page()
        questionpage.click_topic_filter_field_on_question_list_page()
        questionpage.select_base_option_on_question_list_page(basedata)
        questionpage.select_answers_in_player_with_free_question(questiondatas)
    }

    create_duplicate_on_each_create_question_on_question_list_page(basedata, questiondatas) {
        admindashboardpage.click_admin_question_menu()
        questionpage.click_topic_filter_field_on_question_list_page()
        questionpage.select_all_topic_option_on_question_list_page()
        questionpage.click_topic_filter_field_on_question_list_page()
        questionpage.select_base_option_on_question_list_page(basedata)
        questionpage.create_duplicate_on_each_created_question_and_verify(basedata, questiondatas)
        questionpage.click_checkbox_on_original_question_after_duplicated()
        questionpage.click_delete_question_button()
        questionpage.verify_original_question_is_existed()
    }

    export_particular_base_question_to_docx_file(basedata2) {
        admindashboardpage.click_admin_question_menu()
        questionpage.click_topic_filter_field_on_question_list_page()
        questionpage.select_all_topic_option_on_question_list_page()
        questionpage.click_topic_filter_field_on_question_list_page()
        questionpage.select_base_option_on_question_list_page(basedata2)
        questionpage.click_export_all_button_on_question_page()
        questionpage.click_export_button_on_confirmation_window()
        cy.wait(1000)
    }

    copy_created_question_and_move_to_another_base(basedata1, basedata2, questiondatas2) {
        admindashboardpage.click_admin_question_menu()
        questionpage.click_topic_filter_field_on_question_list_page()
        questionpage.select_all_topic_option_on_question_list_page()
        questionpage.click_topic_filter_field_on_question_list_page()
        questionpage.select_base_option_on_question_list_page(basedata1)
        questiondatas2.forEach(ques => {
            questionpage.select_checkbox_on_created_question(ques)
        })
        questionpage.click_copy_to_base_button()
        questionpage.select_base_on_copy_question_to_base_window(basedata2)
    }

    verify_copied_question_is_on_bases(basedata2, questiondatas2) {
        cy.reload()
        admindashboardpage.click_admin_question_menu()
        questionpage.click_topic_filter_field_on_question_list_page()
        questionpage.select_all_topic_option_on_question_list_page()
        questionpage.click_topic_filter_field_on_question_list_page()
        questionpage.select_base_option_on_question_list_page(basedata2)
        questionpage.verify_question_exist_in_copied_base(questiondatas2)
    }
}
export default QuestionAction;