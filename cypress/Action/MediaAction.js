// import MediaPage from "../support/PageObject/MediaPage.js";
// import SettingPage from "../support/PageObject/SettingPage.js";

const MediaPage = require('../support/PageObject/MediaPage')
const SettingPage = require('../support/PageObject/SettingPage')


    const mediapage = new MediaPage()
    const settingpage = new SettingPage()

    class MediaAction {

        create_media_on_mediapage(mediadata) {
            mediapage.click_media_side_menu()
            cy.log(mediadata)
            cy.wrap(mediadata).each((datas) => {
                cy.log('Print upto the items--',datas)
                cy.wrap(datas).then((ele)=>{
                    cy.log('print the ele with the items',ele)
                    let media_name = ele.name;
                    let media_type = ele.type;
                    cy.log(media_name)
                    if(media_type === "factsheet"){
                        mediapage.click_factsheet_button()
                        mediapage.enter_factsheet_title(media_name)
                        mediapage.enter_factsheet_contents(ele.factsheet_data)
                        mediapage.save_factsheet_sheet()
                    }
                    else if(media_type === "embed"){
                        mediapage.click_embed_button()
                        mediapage.enter_embed_title(media_name)
                        mediapage.enter_embed_code(ele.embed_code)
                        mediapage.save_embed_video()
                    }
                    else {
                        mediapage.click_create_media_button()
                        // mediapage.click_create_zip_button()
                        mediapage.click_upload_a_file_button(ele)
                    }
                    mediapage.verify_uploaded_media_is_exist(media_name)
                })
            })
        }

        create_media_on_basepage(mediadata) {
            cy.wrap(mediadata).each((datas) => {
                cy.log('Print upto the items--',datas)
                cy.wrap(datas).then((ele)=>{
                    cy.log('print the ele with the items',ele)
                    let media_name = ele.name;
                    let media_type = ele.type;
                    cy.log(media_name)
                    if(media_type === "factsheet"){
                        mediapage.click_factsheet_button()
                        mediapage.enter_factsheet_title(media_name)
                        mediapage.enter_factsheet_contents(ele.factsheet_data)
                        mediapage.save_factsheet_sheet()
                    }
                    else if(media_type === "embed"){
                        mediapage.click_embed_button()
                        mediapage.enter_embed_title(media_name)
                        mediapage.enter_embed_code(ele.embed_code)
                        mediapage.save_embed_video()
                    }
                    else {
                        mediapage.click_create_media_button()
                        // mediapage.click_create_zip_button()
                        mediapage.click_upload_a_file_button(ele)
                    }
                    mediapage.verify_uploaded_media_is_exist(media_name)
                })
            })
        }



    }
export default MediaAction;