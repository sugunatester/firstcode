import * as path from "path";

class BasePage {

    click_admin_base_menu() {
        cy.reload()
        cy.wait(5000)
        cy.xpath("//a[@href='/enterprise/topics/list']//span[text()='Bases']")
            .click()
    }

    click_new_base_button() {
        cy.get('#new-base-btn').click();
    }

    enter_base_name(base) {
        cy.get('#topic_text').type(base["base_name"]).log("Successfully entered the base name");

    }

    enter_domain_name(domains) {
        // cy.get('#domains-0-domain_text').within(() => {
        // let count = Cypress.$(domains).length;
        let domain_count = 0;
        cy.log(domain_count);

        cy.wrap(domains).each(domain => {
            cy.xpath("//input[@id='domains-" + domain_count + "-domain_text']").type(domain);
            cy.xpath("//a[contains(@class,'add-domain')]").first().click({force: true});
            domain_count = domain_count + 1;
        });
    }

    clearEnteredDomain(){

        cy.get('#domains > div.bloc-body > div:nth-child(2) > div.cell.domain-text > div').clear()
    }


    save_create_base_page() {
        cy.get('#standard-toolbar > .btn-primary').click();

    }

    verify_created_base_exist(base) {
        cy.xpath("//div[@class='flex-row']//a[contains(text(),'" + base["base_name"] + "')]")
            .should('be.visible')
    }

    open_created_base(base) {
        cy.xpath("(//div[@class='flex-row']//a[contains(text(),'" + base["base_name"] + "')])[1]").click({force: true});
    }

    click_questions_tab() {
        cy.xpath("//a[text()='Questions']").click();
    }

    click_new_question_button() {
        cy.xpath("//a[text()='New Question']").click();

    }

    click_questionnaire_tab() {
        cy.xpath("//a[text()='Questionnaires']").click()

    }

    click_new_questionnaire_button() {
        cy.wait(1000)
        cy.xpath("//*[@id='std-toolbar']/a").click({force: true})

    }

    click_create_button() {
        cy.get('#questionnaire_name-save-btn').click();

    }

    click_import_and_export_tab() {
        cy.xpath("//a[text()='Import & Export']")
            .click()
            .log("Successfully clicked the import and export tab button on opened base")

        cy.wait(1000)
    }

    click_upload_file_button() {
        // const downloadsFolder = Cypress.config("downloadsFolder");
        let filename = "../downloads/experquiz-howto-upload-v7-en.docx"
        cy.get('input[type="file"]', {timeout: 50000})
            .attachFile(filename)
        // .attachFile({ filePath: downloadsFolder, fileName: filename});

        cy.log("the questionnaire file is uploaded successfully")

        cy.reload()
    }

    click_to_download_the_sample_question_file() {
        cy.window().document().then(function (doc) {
            doc.addEventListener('click', () => {
                setTimeout(function () {
                    doc.location.reload()
                }, 5000)
            })
            cy.xpath("//a[@class='btn btn-primary download-btn ']")
                .click()
                .log("Successfully download the sample question file")
        })

    }

    verify_file_is_downloaded() {
        const downloadsFolder = Cypress.config("downloadsFolder");
        cy.readFile(path.join(downloadsFolder, "experquiz-howto-upload-v7-en.docx"))
            .should("exist");
    }

    reload_import_page() {
        cy.get('body')
            .then(($file) => {
                const FilePanelIsVisible = $file.find('[id=mcq-files-list]')
                    .is(':visible');
                if (FilePanelIsVisible) {
                    this.CheckImportedFileStatus()
                } else {
                    cy.reload()
                    this.reload_import_page()
                }

            })
    }

    CheckImportedFileStatus = () => {
        cy.get('body').then($mainContainer => {
            const isVisible = $mainContainer.find("div[class='flex-row file-div'] i[class='fa fa-check']")
                .is(':visible');
            if (isVisible) {
                cy.xpath("//div[@class='big-btn ']//span[text()='imported']/../strong")
                    .then(function ($count) {
                        let get_imported_count = $count.text()
                        cy.log('My imported question count is--', get_imported_count)
                        cy.wrap(get_imported_count).as('import_question_count')
                    })
            } else {
                cy.reload()
                this.CheckImportedFileStatus();
            }
        });
    }


    verify_imported_question_is_in_base_of_question_page() {
        this.click_questions_tab()
        cy.get('@import_question_count').then(count => {
            cy.xpath("//h2[@class='message' and contains(text(),'" + count + "')]")
        })
    }

    click_admin_user_menu() {
        cy.xpath("(//a[@href='/enterprise/users'])[1]").click({waitForAnimations: false});

    }

    click_dashaboard_menu() {
        cy.xpath("//i[@class='icon icon-dashboard']").click();

    }

    click_media_tab_on_base() {
        cy.xpath("//a[text()='Medias']").click()
    }

    enterTag(base){
        cy.get('#display-outer_tags_list>div>ul>li>div').type(base['tagname'])
    }
    entertagfield(base){
        cy.get('div.tag-editor-wrapper>ul').type(base['tagname'])
        .type('{enter}').should('be.visible')
    }
    verifyTheBase(base){
        cy.xpath("//a[@class='cell topic-name '][contains(text(),'"+base["base_name"]+"')]")
        .should('be.visible')
    }
    selectTheBase(base){
        cy.xpath("//a[contains(text(),'"+base["base_name"]+"')]/..//span[@class='fa fa-square-o unchecked  ']").click()
    }
    clickArchiveBtn(){
        cy.get('#topics-archive-btn').click()
    }
    verify_the_archived_base_not_displayed(base){
        cy.xpath("//a[@class='cell topic-name '][contains(text(),'"+base["base_name"]+"')]")
        .should('not.be.visible')
    }
    chooseStatusFilter(){
        cy.get('#status-filter-button').click()
    }
    selectAllStatus(){
        cy.xpath("//div[text()='All status']").click()
    }
    verifyAllCountOfArchivedAndActiveBases(){
        let  countbases
        let totalcount
        var list = []
        cy.get('div.topic-div').then(count=>{
            cy.log(count.length);
            countbases=count.length;
             totalcount =countbases.toString().slice('')

        })

        cy.get('span.tts-auto').invoke('text').then((text) => {
            var fullText = text;
    var pattern = /[0-9]+/g;
    cy.log(pattern);
    var number = fullText.match(pattern);
    cy.log(number[0]);
            expect(number[0]).to.equal(totalcount);


        })
    }
    selectArchiveOption(){
        cy.xpath("//div[text()='Archived']").click()
    }
    verifyTheArchivedBase(base){

        cy.xpath("//a[@class='cell topic-name archived']").then(($ele) => {

            const text = $ele.text()
            cy.log(text)

            expect(text).to.contains(base['base_name'])

        })
    }
    selectTheArchivedBase(base){
        cy.xpath("//a[contains(text(),'"+base["base_name"]+"')]/..//span[@class='fa fa-square-o unchecked  ']").click()
    }

    clickUnarchiveBtn(){
        cy.get('#checked-toolbar>#topics-unarchive-btn').click()
    }

    verify_the_base_after_unarchive(base){
        cy.xpath("//a[@class='cell topic-name '][contains(text(),'"+base["base_name"]+"')]")
        .should('not.be.visible')
    }

    selectActiveOption(){
        cy.xpath("//div[text()='Active']").click()
    }

    verifyArchivedQuestionnaire(base,questionnaire){
        cy.xpath("//div[@data-topic='" + base['base_name'] + "']/../a[normalize-space()=' " + questionnaire['questionnaire_name'] + "']")
        .should('not.exist')
    }

    verify_Created_base_on_topic_filter_field(base){
        cy.xpath("//ul[@id='topic-filter-menu']//div[text()='" + base["base_name"] + "']")
        .should('not.exist')
    }

    clickBaseExpandIcon(base){
        cy.get('.detail-toggle-btn > .fa')
        .should('be.visible')
        .click()
    }
    verifyTheAuthorName(admin){
        cy.xpath("//div[@class='data-value' and text()='" + admin["first_name"][0].toUpperCase() + admin["first_name"].slice(1) +" " + admin['last_name'][0].toUpperCase() + admin["last_name"].slice(1) + "']")
        .should('be.exist')
    }
    verifyTheEditedAuthorName(user){
        cy.wrap(user).each((data)=>{
            let firstname= data["first_name"][0].toUpperCase()
            let lastname = data["last_name"][0].toUpperCase()
            cy.xpath("//div[@class='data-value' and text()='" + firstname + "" + data['first_name'].slice(1) + " "+ lastname +"" + data['last_name'].slice(1) + "']")
            .should('be.exist')

     })

    }
    verifytheCreatedDomainName(base){
        cy.xpath("//div[@class='data-group  ']//div[text()='" + base['domains'] + "']")
        .should('be.visible')
    }
    verifyTHeDomainName(domains){


            cy.xpath("//label[text()='Domains']/following::div[1]").then((data) => {
                const datas=data.text();
                const textspace=datas.replace(/\s*,\s*/g, ",");


                cy.log('datas....',textspace)
                const sortedItems = domains.slice().sort();

                cy.log('my sorted name are......c.....',sortedItems)
                const items=sortedItems.toString();
                cy.log('sorted name are......c.....',items)

                expect(textspace).to.equal(items)

        })

    }
    clickCollapseIcon(){
        cy.xpath("//li//a[@data-toggle='collapse']").click()
    }
    clickEditBaseIcon(){
        cy.xpath("//div[@id='std-toolbar']//a[text()='Edit']").click()
    }
    clickAuthorField(){
        cy.get('#author').click();
    }

    chooseUser(user){
        let firstname= user[0]["first_name"][0].toUpperCase()
        let lastname = user[0]["last_name"].toUpperCase()
        cy.xpath("//div[@class='user-item']//span[text()='"+ lastname +" " + firstname + "" + user[0]['first_name'].slice(1) + "']")
        .click()
    }

    verifyTheSelectedUser(user){
        let firstname= user[0]["first_name"][0].toUpperCase()
        let lastname = user[0]["last_name"].toUpperCase()
        cy.xpath("//input[@id='author' and @value='"+ lastname +" " + firstname + "" + user[0]['first_name'].slice(1) + "']")
        .should('be.exist')
    }
    edit_base_name(base) {
        cy.get('#topic_text').clear().type(base["base_name"]);

    }


    verifyCreatedBaseQuestions(question){
        cy.wrap(question).each((question_title) => {
            cy.xpath("//div[@class='cell title'][contains(normalize-space(.),'" + question_title + "')]")
            .should('not.exist')
        })
    }
    clickEnterPriseFolderDD(){
        cy.get('#topic-filter-button').click()
    }
    verifyCreatedBaseMedias(media){
        cy.wrap(media).each((media_name) => {
            cy.xpath("//div[@id='media-folder']//div[contains(@class,'folder-item media tags-filtered')]//div//span[text()=' " + media_name + "']")
            .should('not.exist')
        })
    }
    click_topic_filter_field_on_Training_page() {

        cy.xpath("//span[@id='topic-filter-button']")
            .click()
            .log("successfully clicking the Topic filter button on question list page")

    }
    verifyCreatedBaseTraining(training){

        cy.xpath("//div//a[normalize-space()=' " + training['training_name'] + " ']")
            .should('not.exist')
    }
    clickAddIcon(){
        cy.get('#domains > div.bloc-body > div:nth-child(2) > div.cell.add-domain').click()
    }
    enterAbbreviation(editbase){
        cy.get('#topic_short').type(editbase['Abbreviation'])
    }
    enterDescription(editbase){
        cy.get('#description').type(editbase['description'])
    }

    //To delete Base
    clickDeleteIcon(base){
        cy.xpath("//div//a[normalize-space()='" + base["base_name"] + "']/../../div//div[6]//ul//li[5]").click();
    }
    clickDeleteButton(){
        cy.get('a.btn.btn-danger.btn-ok').click();
    }
    clickModuleTabOnBase(){
        cy.xpath("//li//a[text()='Modules']").click();
    }
    clickNewModuleButton(){
        cy.get('#new-module-btn').click();
    }
    verifyDeletedModuleName(){
        cy.xpath("//div[@class='flex-row body']//a[@class='cell module-name ellipsis ']")
        .should('not.be.visible');
    }


    clickTrainingModule(){
        cy.get('#topic-view-page>div>ul>li:nth-child(5)>a').click()
    }
    clickNewTrainingBtn(){
        cy.get('#new-training-btn').click()
    }
    enterTrainingName(base){
        cy.get('#training_name').type(base['training_name'])
    }
    chooseStartDate(){
        const dayjs = require('dayjs')

       //In test
       cy.log(dayjs().format('DD/MM/YYYY'))  //Prints todays date 30/09/2021
       cy.get('#start_date').type(dayjs().format('DD/MM/YYYY'))
    }
    chooseEndDate(){
        const dayjs = require('dayjs')

       //In test
       cy.log(dayjs().format('DD/MM/YYYY'))  //Prints todays date 30/09/2021
       cy.get('#end_date').type(dayjs().format('DD/MM/YYYY'))
    }
    enterTrainerName(){
        cy.get('#trainer_name').click()
        cy.get('div.users-div').click()
        cy.get('#trainer_name').should('be.visible')
    }
    clickSave(){
        cy.get('#standard-toolbar>button').click()
    }
    chooseQuestionnaire(){
        cy.xpath("//div[@class='big-btn  with-href ']//span[text()='questionnaire']").click()
    }
    dragQuestionaire(){
        cy.get('#items-set-1>div').drag('#items-set-2')
        .should('be.visible')
    }
    clickMediaTab(){
         cy.xpath("//li//a[text()='Medias']").click()
    }
}

export default BasePage;
