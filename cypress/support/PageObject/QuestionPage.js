class QuestionPage {

    click_topic_filter_field_on_question_list_page() {

        cy.xpath("//span[@id='topic-filter-button']")
            .click()
            .log("successfully clicking the Topic filter button on question list page")

    }

    select_all_topic_option_on_question_list_page() {

        cy.xpath("//div[text()='All topics']")
            .click()
            .log("successfully clicking the All Topic option on question list page")

    }

    select_base_option_on_question_list_page(base) {

        cy.xpath("//div[text()='" + base["base_name"] + "']")
            .click()
            .log("successfully filtered the created base on question list page")

    }

    click_new_question_button() {

        cy.xpath("//a[@id='new-question-btn']")
            .click()
            .log("successfully clicked the create new question button")

    }

    click_export_all_button_on_question_page() {

        cy.xpath("//a[@id='questions-export-all-btn']")
            .click()
            .log("successfully clicked the export all button on question page")

    }

    click_export_button_on_confirmation_window() {
        cy.window().document().then(function (doc) {
            doc.addEventListener('click', () => {
                setTimeout(function () {
                    doc.location.reload()
                }, 5000)
            })
            cy.xpath("(//div[@class='modal-content vcenter'])[3]/../../..//button[@id='export-save-btn']")
                .click()
                .log("Successfully download the export question docx file")
        })

    }

    click_domain_filter_field() {

        cy.get("#domain_id-button > .ui-selectmenu-text").click();

    }

    // create_question_with_domain(base, question) {
    //     let count = 1;
    //     cy.get('#domain_id-button').click();
    //     let bases = base["domains"];
    //     cy.wrap(bases).each(bases1 => {
    //         cy.xpath("//div[text()='" + bases1 + "']").first().click({force: true});
    //         count = count + 1;
    //         // create_question(question)
    //         return
    //
    //         create_question_with_domain(base, question) {
    //             // cy.wrap(question).each(data => {
    //             //     this.click_question_type();
    //             //     this.enter_question_title();
    //             let count = 1;
    //             cy.get('#domain_id-button').click();
    //             let bases = base["domains"];
    //             cy.wrap(bases).each(bases1 => {
    //                 cy.xpath("//div[text()='" + bases1 + "']").first().click({force: true});
    //                 count = count + 1;
    //             })
    //         }
    //     })
    // }

    click_question_type(question_type) {
        if (question_type === "single") {
            cy.xpath("//li[@data-value='mcq']/span[2]").click();
        } else if (question_type === "multi") {
            cy.get('[data-value="mchk"] > .answer-type-caption').click();
        } else if (question_type === "true_or_false") {
            cy.xpath("//li[@data-value='bool']/span[2]").click();
        } else if (question_type === "entered_answer") {
            cy.xpath("//li[@data-value='text']/span[2]").click();
        } else if (question_type === "sort_list") {
            cy.xpath("//li[@data-value='sort']/span[2]").click();
        } else if (question_type === "match_items") {
            cy.xpath("//li[@data-value='match']/span[2]").click();
        } else if (question_type === "fill") {
            cy.xpath("//li[@data-value='fill']/span[2]").click();
        } else if (question_type === "classify") {
            cy.xpath("//li[@data-value='classif']/span[2]").click();
        } else if (question_type === "softsingle") {
            cy.xpath("//li[@data-value='heart']/span[2]").click();
        } else if (question_type === "softmultiple") {
            cy.xpath("//li[@data-value='heart_multiple']/span[2]").click();
        } else if (question_type === "image_area") {
            cy.xpath("//li[@data-value='imga']/span[2]").click();
        } else if (question_type === "free_form_answer") {
            cy.xpath("//li[@data-value='free']/span[2]").click();
        } else if (question_type === "Select") {
            cy.xpath("//li//span[text()='Select']").click();
        }
    }

    select_domain_by_randomly(domains) {
        cy.wrap(domains).each((domain) => {
            var random_domain_selection = ""
            let my_domain = cy.log(domain.domain)
            for (var i = 0; i < 2; i++)
                random_domain_selection += my_domain;
            return random_domain_selection;
        })

    }

    enter_question_title(titles) {
        cy.get('#title')
            .clear()
            .type(titles)
            .log("successfully entered the question title")
    }

    enter_question(questions) {
        cy.xpath("(//div[contains(@class,'cke_textarea')])[1]")
            .clear()
            .type(questions)
            .log("successfully entered the question")
    }

    enter_question_rank(rank) {
        cy.xpath("//input[@id='rank']")
            .clear()
            .type(rank)
            .log("Successfully entered the question rank")
    }

    click_save_and_new_on_created_question_page() {
        cy.xpath("//i[@class='fa fa-retweet']").click()
            // cy.xpath("//a[@name='save-new']").click()
            .log("Successfully to save the create question page")
    }

    click_save_button_on_edit_question_page() {
        cy.xpath("//a[@name='save']")
            .click()
            .log("Successfully saved the edited questions")
    }

    create_question_with_random_domain_selection(base) {
        cy.log('my domain list is', base)
        let my_do = base
        cy.wrap(my_do).each((domainss) => {
            cy.log('my single do', domainss)
            // cy.xpath("//span[@id='domain_id-button']")
            //     .click()
            // cy.xpath("//div[text()='" + domainss + "']")
        })

    }


    create_question(base, question) {
        cy.wrap(question).each((ques) => {
            cy.log(ques.title);
            cy.log(ques.type);
            cy.log(ques.question_text);
            this.click_question_type(ques.type);
            this.enter_question_title(ques.title);
            this.enter_question(ques.question_text);
            let ans = ques.answers;
            cy.wrap(ans).each((answer, index) => {
                let type = ["single", "sort_list", "fill"];
                let type1 = ["match_items"];
                let type2 = ["Select"];
                let type3 = ["classify"];
                let type4 = ["softsingle", "softmultiple"];
                let type5 = ["entered_answer"];
                let type6 = ["multi"];
                if (type.includes(ques.type)) {
                    cy.get('#answers-' + index + '-ckeditor').type(answer.answer_text)
                    cy.wait(3000);
                } else if (type1.includes(ques.type)) {
                    if (index === 0) {
                        cy.get('#answers-' + index + '-ckeditor').type(answer.case)
                        index++
                        cy.get('#answers-' + index + '-ckeditor').type(answer.match_text)
                    } else if (index === 1) {
                        index++
                        cy.get('#answers-' + index + '-ckeditor').type(answer.case)
                        index++
                        cy.get('#answers-' + index + '-ckeditor').type(answer.match_text)
                    }
                    cy.wait(3000);
                } else if (type2.includes(ques.type)) {
                    cy.get('#answers-' + index + '-ckeditor').type(answer.syntax + answer.correct_answer + answer.second_option + answer.third_option)
                } else if (type3.includes(ques.type)) {
                    cy.get('#answers-' + index + '-ckeditor').type(answer.bucket + answer.syntax + answer.label)
                } else if (type4.includes(ques.type)) {
                    cy.wait(3000);
                    cy.get('#display-answer-' + index + ' .tag-editor div').click()
                    cy.get('.ui-autocomplete-input').type(answer.tag_answer)
                    cy.get('#answers-' + index + '-ckeditor').type(answer.answer_text)
                } else if (type5.includes(ques.type)) {
                    cy.get('#answers-' + index + '').type(answer.answer_text)
                } else if (type6.includes(ques.type)) {
                    cy.xpath("//div[@class='field-input xq-editable ']//div[@id='answers-" + index + "-ckeditor']")
                        .type(answer.answer_text)
                        cy.wait(5000)
                    if (index === 1) {
                        cy.get('#display-answer-1 > .field-area > .field-check > label > .glyph-answer-false').should('be.visible')
                            .click()
                    }
                    cy.wait(3000);
                }
            })
            this.click_save_and_new_on_created_question_page();
            cy.wait(3000);
        })

    }

    create_question_with_free_form(base, question) {
        cy.wrap(question).each((ques) => {
            cy.log(ques.title);
            cy.log(ques.type);
            cy.log(ques.question_text);
            this.click_question_type(ques.type);
            this.enter_question_title(ques.title);
            this.enter_question(ques.question_text);
            let ans = ques.answers;
            cy.wrap(ans).each((answer, index) => {
                let type = ["single", "sort_list", "fill"];
                let type1 = ["match_items"];
                let type2 = ["Select"];
                let type3 = ["classify"];
                let type4 = ["softsingle", "softmultiple"];
                let type5 = ["entered_answer"];
                let type6 = ["multi"];
                let type7 = ["free_form_answer"];
                if (type.includes(ques.type)) {
                    cy.get('#answers-' + index + '-ckeditor').type(answer.answer_text)
                    cy.wait(3000);
                } else if (type1.includes(ques.type)) {
                    if (index === 0) {
                        cy.get('#answers-' + index + '-ckeditor').type(answer.case)
                        index++
                        cy.get('#answers-' + index + '-ckeditor').type(answer.match_text)
                    } else if (index === 1) {
                        index++
                        cy.get('#answers-' + index + '-ckeditor').type(answer.case)
                        index++
                        cy.get('#answers-' + index + '-ckeditor').type(answer.match_text)
                    }
                    cy.wait(3000);
                } else if (type2.includes(ques.type)) {
                    cy.get('#answers-' + index + '-ckeditor').type(answer.syntax + answer.correct_answer + answer.second_option + answer.third_option)
                } else if (type3.includes(ques.type)) {
                    cy.get('#answers-' + index + '-ckeditor').type(answer.bucket + answer.syntax + answer.label)
                } else if (type4.includes(ques.type)) {
                    cy.wait(3000);
                    cy.get('#display-answer-' + index + ' .tag-editor div').click()
                    cy.get('.ui-autocomplete-input').type(answer.tag_answer)
                    cy.get('#answers-' + index + '-ckeditor').type(answer.answer_text)
                } else if (type5.includes(ques.type)) {
                    cy.get('#answers-' + index + '').type(answer.answer_text)
                } else if (type6.includes(ques.type)) {
                    cy.xpath("//div[@class='field-input xq-editable ']//div[@id='answers-" + index + "-ckeditor']")
                        .type(answer.answer_text)
                    if (index === 1) {
                        cy.get('#display-answer-1 > .field-area > .field-check > label > .glyph-answer-false')
                            .click()
                    }
                    cy.wait(3000);
                } else if (type7.includes(ques.type) && ques.title.includes("free with text")) {
                    cy.log("-----------")

                } else if (type7.includes(ques.type) && ques.title.includes("free with record")) {
                    this.click_more_setting_button_on_create_question_page()
                    this.enable_with_attachment()
                    this.enable_attachment_audio()
                    this.disable_text_area()

                } else if (type7.includes(ques.type) && ques.title.includes("free with attachment")) {
                    this.click_more_setting_button_on_create_question_page()
                    this.enable_with_attachment()
                    this.disable_text_area()
                }
            })
            this.click_save_and_new_on_created_question_page();
            cy.wait(3000);
        })

    }

    create_question_with_same_rank_and_verify_it_error_message(base, question) {
        cy.wrap(question).each((ques) => {
            cy.log(ques.title);
            cy.log(ques.type);
            cy.log(ques.question_text);
            this.click_question_type(ques.type);
            this.enter_question_title(ques.title);
            this.enter_question(ques.question_text);
            this.enter_question_rank(ques.rank)
            let ans = ques.answers;
            cy.wrap(ans).each((answer, index) => {
                let type = ["single", "sort_list", "fill"];
                let type1 = ["match_items"];
                let type2 = ["Select"];
                let type3 = ["classify"];
                let type4 = ["softsingle", "softmultiple"];
                let type5 = ["entered_answer"];
                let type6 = ["multi"];
                if (type.includes(ques.type)) {
                    cy.get('#answers-' + index + '-ckeditor').type(answer.answer_text)
                    cy.wait(3000);
                } else if (type1.includes(ques.type)) {
                    if (index === 0) {
                        cy.get('#answers-' + index + '-ckeditor').type(answer.case)
                        index++
                        cy.get('#answers-' + index + '-ckeditor').type(answer.match_text)
                    } else if (index === 1) {
                        index++
                        cy.get('#answers-' + index + '-ckeditor').type(answer.case)
                        index++
                        cy.get('#answers-' + index + '-ckeditor').type(answer.match_text)
                    }
                    cy.wait(3000);
                } else if (type2.includes(ques.type)) {
                    cy.get('#answers-' + index + '-ckeditor').type(answer.syntax + answer.correct_answer + answer.second_option + answer.third_option)
                } else if (type3.includes(ques.type)) {
                    cy.get('#answers-' + index + '-ckeditor').type(answer.bucket + answer.syntax + answer.label)
                } else if (type4.includes(ques.type)) {
                    cy.wait(3000);
                    cy.get('#display-answer-' + index + ' .tag-editor div').click()
                    cy.get('.ui-autocomplete-input').type(answer.tag_answer)
                    cy.get('#answers-' + index + '-ckeditor').type(answer.answer_text)
                } else if (type5.includes(ques.type)) {
                    cy.get('#answers-' + index + '').type(answer.answer_text)
                } else if (type6.includes(ques.type)) {
                    cy.xpath("//div[@class='field-input xq-editable ']//div[@id='answers-" + index + "-ckeditor']")
                        .type(answer.answer_text)
                    if (index === 1) {
                        cy.get('#display-answer-1 > .field-area > .field-check > label > .glyph-answer-false')
                            .click()
                    }
                    cy.wait(3000);
                }
            })
            this.click_save_and_new_on_created_question_page();
            cy.wait(3000);
        })
        this.verify_rank_error_message()
    }

    verify_rank_error_message() {
        cy.xpath("//p[@class='help-block' and text()='This rank is already assigned']")
            .should('be.exist').then(rank_error => {
            if (rank_error) {
                cy.log("The rank error is message displayed")
            } else {
                cy.log("the rank error message is not exist")
            }
        })
    }

    click_edit_button_on_created_question(question_title) {

        cy.xpath("//div[@class='cell title'][contains(normalize-space(.),'" + question_title + "')]//following::a[2]")
            .click()
            .log("successfully clicked the edit button on created question")

    }

    click_play_button_on_created_question(question_title) {

        cy.xpath("//div[@class='cell title'][contains(normalize-space(.),'" + question_title + "')]//following::a[3]", {timeout: 5000})
            .click()
            .log("successfully clicked the edit button on created question")

    }

    click_duplicate_button_on_created_question(question_title) {

        cy.xpath("//div[@class='cell title'][contains(normalize-space(.),'" + question_title + "')]//following::a[4]")
            .click()
            .log("successfully clicked the edit button on created question")

    }

    verify_duplicate_question(base, question_title) {
        cy.reload()
        // cy.xpath("//div[@class='cell title'][contains(normalize-space(.),'" + question_title + " (copy)')]", {timeout:500000})
        //     .should('be.visible')
        cy.xpath("//div[@title='" + base["base_name"] + "']//following::div[@class='cell title'][contains(text(),'" + question_title + " (copy)')]", {timeout: 50000})
            .should('be.visible')
    }

    verify_duplicate_question_is_in_draft_status(question_title) {
        cy.xpath("//div[@class='cell title'][contains(normalize-space(.),'" + question_title + " (copy)')]/../..//div[@class='cell status' and contains(text(),'draft')]")
            .should('be.visible')
        // .should('exist')
    }

    click_checkbox_on_original_question_after_duplicated() {
        cy.xpath("//div[@class='cell status' and contains(text(),'active')]/../..//span[@class='fa fa-square-o unchecked  ']")
            .click({multiple: true})
    }

    verify_original_question_is_existed() {
        cy.reload()
        cy.xpath("//div[@class='cell status' and contains(text(),'active')]")
            .should('not.be.exist')
    }

    click_delete_question_button(question_title) {
        cy.xpath("//a[@id='delete-questions-btn']")
            .click()
        this.click_confirm_delete_question_button()
        cy.reload()
    }

    click_confirm_delete_question_button(question_title) {
        cy.xpath("//a[@id='confirm-delete-btn']")
            .click()
    }

    select_checkbox_on_created_question(ques) {

        cy.xpath("//div[@class='cell title' and contains(text(),'" + ques["title"] + "')]/../..//span[@class='fa fa-square-o unchecked  ']")
            .click()
            .log("successfully checked the checkbox on created question")

    }

    click_copy_to_base_button() {

        cy.xpath("//a[@id='copy-to-base-btn']")
            .click()
            .log("successfully clicked the copy to base button")


    }

    click_base_filter_field_on_copy_to_base_window() {

        cy.xpath("//div[@class='modal-content assign-content vcenter']//span[@class='ui-selectmenu-text']")
            .click()
            .log("successfully clicked the select base filter field on copy to base window")

    }

    select_base_on_copy_to_base_window(base) {

        cy.xpath("//div[@class='ui-selectmenu-menu ui-front ui-selectmenu-open']//ul[@id='assign-topic-menu']//div[text()='" + base["base_name"] + "']")
            .click()
            .log("successfully selected the base on copy to base window")

    }

    click_save_button_on_copy_to_base_window() {

        cy.xpath("//div[@class='modal-content assign-content vcenter']//input[@id='popover-submit']")
            .click()
            .log("successfully clicked the save button on copy to base window")

    }

    verify_copied_question_is_existed_on_base(question_title) {
        cy.xpath("//div[@class='cell title'][contains(normalize-space(.),'" + question_title + "')]//following::a[4]")
            .should('exist')
    }

    verify_question_exist_in_copied_base(question) {
        question.forEach(ques => {
            this.verify_copied_question_is_existed_on_base(ques.title)

        })
    }

    select_base_on_copy_question_to_base_window(base) {
        // cy.xpath("//div[@class='modal-dialog']/../..//div[@class='modal-content assign-content vcenter']")
        //     .should('be.visible')
        // cy.xpath("//div[@class='modal-dialog']/../..//div[@class='modal-content assign-content vcenter']", {timeout:50000})
        //     .then($select_base_window=>{
        //         if($select_base_window.is(':visible')){
        this.click_base_filter_field_on_copy_to_base_window()
        this.select_base_on_copy_to_base_window(base)
        this.click_save_button_on_copy_to_base_window()
        // }
        // else{
        //     cy.log("copy to base window is not displayed")
        // }
        // })
    }


    create_duplicate_on_each_created_question_and_verify(base, question) {
        question.forEach(ques => {
            this.click_duplicate_button_on_created_question(ques.title)
            this.verify_duplicate_question(base, ques.title)
            this.verify_duplicate_question_is_in_draft_status(ques.title)
        })
    }


    edit_created_question(base, question) {
        cy.wrap(question).each((ques) => {
            this.click_edit_button_on_created_question(ques.title)
            this.click_question_type(ques.edit_type);
            this.enter_question_title(ques.edit_title);
            this.enter_question(ques.edit_question_text);
            let ans = ques.edit_answers;
            cy.wrap(ans).each((answer, index) => {
                let type = ["single", "sort_list", "fill"];
                let type1 = ["match_items"];
                let type2 = ["Select"];
                let type3 = ["classify"];
                let type4 = ["softsingle", "softmultiple"];
                let type5 = ["entered_answer"];
                let type6 = ["multi"];
                if (type.includes(ques.edit_type)) {
                    cy.get('#answers-' + index + '-ckeditor')
                        .clear()
                        .type(answer.answer_text)
                    cy.wait(3000);
                } else if (type1.includes(ques.edit_type)) {
                    if (index === 0) {
                        cy.get('#answers-' + index + '-ckeditor')
                            .clear()
                            .type(answer.case)
                        index++
                        cy.get('#answers-' + index + '-ckeditor')
                            .clear()
                            .type(answer.match_text)
                    } else if (index === 1) {
                        index++
                        cy.get('#answers-' + index + '-ckeditor')
                            .clear()
                            .type(answer.case)
                        index++
                        cy.get('#answers-' + index + '-ckeditor')
                            .clear()
                            .type(answer.match_text)
                    }
                    cy.wait(3000);
                } else if (type2.includes(ques.edit_type)) {
                    cy.get('#answers-' + index + '-ckeditor')
                        .clear()
                        .type(answer.syntax + answer.correct_answer + answer.second_option + answer.third_option)
                } else if (type3.includes(ques.edit_type)) {
                    cy.get('#answers-' + index + '-ckeditor')
                        .clear()
                        .type(answer.bucket + answer.syntax + answer.label)
                } else if (type4.includes(ques.edit_type)) {
                    cy.get('#answers-' + index + '-ckeditor')
                        .clear()
                        .type(answer.answer_text)
                    if (ques.edit_type === "softsingle") {
                        if (index === 0) {
                            this.edit_tag1_on_soft_skill_multiple_question(answer)
                        }
                        if (index === 1) {
                            this.edit_tag2_on_soft_skill_multiple_question(answer)
                        }
                        if (index === 2) {
                            this.edit_tag3_on_soft_skill_multiple_question(answer)
                        }
                    }
                    if (ques.edit_type === "softmultiple") {
                        if (index === 0) {
                            this.enter_tag1_on_select_question(answer)
                        }
                        if (index === 1) {
                            this.enter_tag2_on_select_question(answer)
                        }
                        if (index === 2) {
                            this.enter_tag3_on_select_question(answer)
                        }
                    }
                } else if (type5.includes(ques.edit_type)) {
                    cy.get('#answers-' + index + '')
                        .clear()
                        .type(answer.answer_text)
                } else if (type6.includes(ques.edit_type)) {
                    cy.get('#answers-' + index + '-ckeditor > :nth-child(1) > .form-group > .cke_textarea_inline')
                    cy.xpath("//div[@class='field-input xq-editable ']//div[@id='answers-" + index + "-ckeditor']")
                        .clear()
                        .type(answer.answer_text)
                    cy.wait(3000);
                }
            })
            this.click_save_button_on_edit_question_page()
            cy.wait(3000);
        })

    }


    edit_tag1_on_soft_skill_multiple_question(answer) {
        cy.xpath("//div[text()='" + answer.tag_answer_1 + "']")
            .should('be.visible')
        cy.xpath("//div[@id='display-answer-0']//div[@class='tag-editor-delete']")
            .click()
        cy.wait(1000)
        cy.get('#display-answer-0 .tag-editor div')
            .click()
        cy.get('.ui-autocomplete-input')
            .type(answer.edit_tag_answer)
    }

    edit_tag2_on_soft_skill_multiple_question(answer) {
        cy.xpath("//div[text()='" + answer.tag_answer_1 + "']")
            .should('be.visible')
        cy.xpath("//div[text()='" + answer.tag_answer_2 + "']")
            .should('be.visible')
        cy.xpath("//div[@id='display-answer-1']//div[@class='tag-editor-delete']")
            .click({multiple: true})
        cy.wait(1000)
        cy.get('#display-answer-1 .tag-editor div')
            .click()
        cy.get('.ui-autocomplete-input')
            .type(answer.edit_tag_answer)
    }

    edit_tag3_on_soft_skill_multiple_question(answer) {
        cy.xpath("//div[text()='" + answer.tag_answer_1 + "']")
            .should('be.visible')
        cy.xpath("//div[text()='" + answer.tag_answer_2 + "']")
            .should('be.visible')
        cy.xpath("//div[@id='display-answer-2']//div[@class='tag-editor-delete']")
            .click({multiple: true})
        cy.wait(1000)
        cy.get('#display-answer-2 .tag-editor div')
            .click()
        cy.get('.ui-autocomplete-input')
            .type(answer.edit_tag_answer)
    }

    enter_tag1_on_select_question(answer) {
        cy.wait(1000)
        cy.get('#display-answer-0 .tag-editor div')
            .click()
        cy.get('.ui-autocomplete-input')
            .type(answer.tag_answer)
    }

    enter_tag2_on_select_question(answer) {
        cy.wait(1000)
        cy.get('#display-answer-1 .tag-editor div')
            .click()
        cy.get('.ui-autocomplete-input')
            .type(answer.tag_answer)
    }

    enter_tag3_on_select_question(answer) {
        cy.wait(1000)
        cy.get('#display-answer-2 .tag-editor div')
            .click()
        cy.get('.ui-autocomplete-input')
            .type(answer.tag_answer)
    }

    click_validate_and_move_next_question_in_questionnaire_player() {
        cy.xpath("//div//a//i[@class='fa fa-check']").click();
        // cy.xpath("//div//a[@id='next-btn']").click({force: true, multiple: true});
        cy.get('#exit-btn > .fa').click()
    }

    select_all_type_question_answers_in_player_without_free_question(questiondatas) {
        questiondatas.forEach(QuestionDatas => {
            cy.log(QuestionDatas)
            this.click_play_button_on_created_question(QuestionDatas.title)
            cy.xpath("//div[@class='debrief' and(contains(@style,'none'))]/../div[@class='question ' and @data-answer-type]")
                .invoke('attr', 'data-answer-type')
                .then(attributeValue => {
                    cy.wait(3000);
                    this.AnswerAllTypeQuestionWithoutFree(questiondatas, attributeValue)
                });
        });
    }

    select_single_type_question_answers_in_player_without_free_question(questiondatas) {
        questiondatas.forEach(QuestionDatas => {
            cy.log(QuestionDatas)
            this.click_play_button_on_created_question(QuestionDatas.title)
            cy.xpath("//div[@class='debrief' and(contains(@style,'none'))]/../div[@class='question ' and @data-answer-type]")
                .invoke('attr', 'data-answer-type')
                .then(attributeValue => {
                    cy.wait(3000);
                    this.AnswerSingleTypeQuestionWithoutFree(QuestionDatas, attributeValue)
                });
        });
    }

    select_answers_in_player_with_free_question(questiondatas) {
        questiondatas.forEach(QuestionDatas => {
            cy.log(QuestionDatas)
            this.click_play_button_on_created_question(QuestionDatas.title)
            cy.xpath("//div[@class='debrief' and(contains(@style,'none'))]/../div[@class='question ' and @data-answer-type]")
                .invoke('attr', 'data-answer-type')
                .then(attributeValue => {
                    cy.wait(3000);
                    this.AnswerAllTypeQuestionWithFree(questiondatas, attributeValue)
                });
        });
    }

    AnswerAllTypeQuestionWithFree(questiondatas, attributeValue) {
        cy.log("Entered into main loop")
        // questiondatas.forEach(data => {
        //     cy.log("data", data)
        if (questiondatas.type === "single" && attributeValue === "mcq") {
            cy.log("It entered into single Type")
            cy.log("1", questiondatas.type)
            let answer = questiondatas.answers;
            answer.forEach(ans => {
                if (ans.correct === true) {
                    cy.wait(3000);
                    cy.xpath("//div//p[text()='" + ans.answer_text + "']").click({multiple: true});
                }
            })
            // this.click_validate_and_move_next_question_in_questionnaire_player();
        } else if (questiondatas.type === "multi" && attributeValue === "mchk") {
            cy.log("It entered into multi Type")
            cy.log("2", questiondatas.type)
            let answer = questiondatas.answers;
            answer.forEach(ans => {
                if (ans.correct === true) {
                    cy.wait(3000);
                    cy.xpath("//div//p[text()='" + ans.answer_text + "']")
                        .click({force: true, multiple: true});
                }
            })
            // this.click_validate_and_move_next_question_in_questionnaire_player();
        } else if (questiondatas.type === "true_or_false" && attributeValue === "bool") {
            cy.log("It entered into true_or_false")
            cy.log("3", questiondatas.type)
            let answer = questiondatas.answers;
            answer.forEach(ans => {
                if (ans.correct === true) {
                    cy.wait(3000);
                    cy.xpath("//div//p[text()='" + ans.answer_text + "']").click();
                }
            })
            // this.click_validate_and_move_next_question_in_questionnaire_player();
        } else if (questiondatas.type === "match_items" && attributeValue === "match") {
            cy.log("It entered into match_the_following")
            cy.log("4", questiondatas.type)
            let answer = questiondatas.answers;
            cy.wait(3000);

            let questionArray = [];
            let answerArray = [];
            cy.xpath("//div[@class='match-left']//p").each(val => {
                questionArray.push(val.text())
                cy.log("q..........", questionArray);
                cy.log("Check question[0]", questionArray[0])
                cy.log("Check question[1]", questionArray[1])

                cy.xpath("//div[@class='sort-content']//p").each(val1 => {
                    answerArray.push(val1.text())
                    cy.log("a.............", answerArray);
                    cy.log("Check answer[0]", answerArray[0])
                    cy.log("Check answer[1]", answerArray[1])

                    if (questionArray[0] === answer[0].case) {
                        cy.log("it enter into 1st if condition")
                        if (answerArray[0] === answer[0].match_text) {
                            cy.log("it enter into 2nd if condition")
                            cy.log("It's already in correct order")
                        } else {
                            cy.log("it enter into else")
                            cy.xpath("//div[@class='sort-content']//p[text()='" + answer[0].match_text + "']")
                                .trigger("mousedown", {which: 1})
                            cy.xpath("//div[@class='sort-content']//p[text()='" + answer[1].match_text + "']")
                                .trigger("mousemove")
                                .trigger("mouseup", {force: true})
                        }
                    }
                })
            })
            // this.click_validate_and_move_next_question_in_questionnaire_player();
        } else if (questiondatas.type === "sort_list" && attributeValue === "sort") {
            cy.log("it entered into sort_list")
            cy.log("5", questiondatas.type)
            let answer = questiondatas.answers;
            cy.wait(3000);

            cy.xpath("//div[@class='sortable ui-sortable']").then(val => {
                let value = val.text();
                cy.log("..........", value)
                cy.log(answer[0].answer_text + " " + answer[1].answer_text + " " + answer[2].answer_text)

                //here the wrap only for looping not for fetch data
                cy.wrap(answer).each(data => {
                    cy.log("for loop", data)
                    if (value === answer[0].answer_text + " " + answer[1].answer_text + " " + answer[2].answer_text) {
                        cy.log("enters into if")
                    } else {
                        cy.log("enters into else")
                        cy.xpath("//div[@class='sort-content']//p[text()='" + answer[0].answer_text + "']")
                            .trigger("mousedown", {which: 1})
                        cy.get('.answer:nth-child(1)').first().trigger("mousemove", {force: true})
                            .trigger("mouseup", {force: true})

                        cy.xpath("//div[@class='sort-content']//p[text()='" + answer[1].answer_text + "']")
                            .trigger("mousedown", {which: 1})
                        cy.get('.answer:nth-child(2)').first().trigger("mousemove", {force: true})
                            .trigger("mouseup", {force: true})

                        cy.xpath("//div[@class='sort-content']//p[text()='" + answer[2].answer_text + "']")
                            .trigger("mousedown", {which: 1})
                        cy.get('.answer:nth-child(3)').first().trigger("mousemove", {force: true})
                            .trigger("mouseup", {force: true})
                    }
                })
            })
            // this.click_validate_and_move_next_question_in_questionnaire_player();
        } else if (questiondatas.type === "fill" && attributeValue === "fill") {
            cy.log("it entered into fill")
            cy.log("6", questiondatas.type)
            let Answer = questiondatas.answers;
            Answer.forEach(ans => {
                if (ans.correct === true) {
                    cy.wait(3000);
                    cy.xpath("//div//p//input[@class='fill-blank']").type(ans.answer);
                }
            })
            // this.click_validate_and_move_next_question_in_questionnaire_player();
        } else if (questiondatas.type === "entered_answer" && attributeValue === "text") {
            cy.log("it entered into enter_answer")
            cy.log("7", questiondatas.type)
            let Answer = questiondatas.answers;
            let answer = questiondatas.answers;
            answer.forEach(ans => {
                cy.wait(3000);
                cy.xpath("//div//input[@class='answer-given active']").type(ans.answer_text);
            })
            // this.click_validate_and_move_next_question_in_questionnaire_player();
        } else if (questiondatas.type === "classify" && attributeValue === "classif") {
            cy.log("it entered into classification")
            cy.log("8", questiondatas.type)
            cy.wait(3000);
            cy.get('[data-name=_default] > div').then(questions => {
                let lists = questions;
                let i = 0;
                cy.wrap(lists).each($elem => {
                    let value = $elem.text();
                    cy.wait(2000)
                    cy.xpath("//div[contains(@data-name,'answer_')]/div[contains(text(),'" + value + "')]/..").trigger("mousedown", {which: 1})
                    cy.get('[data-title=' + value + ']')
                        .trigger("mousemove")
                        .trigger("mouseup", {force: true})
                    cy.wait(2000)
                    i++
                })
            })

            // this.click_validate_and_move_next_question_in_questionnaire_player();

        } else if (questiondatas.type === "Select" && attributeValue === "fsel") {
            cy.log("it entered into select_type")
            cy.log("9", questiondatas.type)
            let answer = questiondatas.answers;
            answer.forEach(ans => {
                cy.wait(3000);
                cy.xpath("(//span[@class='ui-selectmenu-text'])[1]").click();
                cy.xpath("//li[@class='ui-menu-item']//div[text() = '" + ans.correct_answer + "']").click();
            })
            // this.click_validate_and_move_next_question_in_questionnaire_player();
        } else if (questiondatas.type === "softsingle" && attributeValue === "heart") {
            cy.log("10", questiondatas.type)
            cy.log("it entered into soft_skill_single")
            cy.wait(3000);
            cy.xpath("(//div[@class='answer answer-choice active'])[1]").click();
            // this.click_validate_and_move_next_question_in_questionnaire_player();
        } else if (questiondatas.type === "softmultiple" && attributeValue === "heart_multiple") {
            // cy.wait(9000);
            cy.log("11", questiondatas.type)
            cy.log("it entered into soft_skill_multiple")
            let answer = questiondatas.answers;
            answer.forEach(ans => {
                let index = 1
                // cy.wait(9000);
                if (ans.correct === true) {
                    cy.xpath("(//div[@class='answer answer-multi-choice active'])[" + index + "]").click();
                }
            })
            // this.click_validate_and_move_next_question_in_questionnaire_player();
        } else if (questiondatas.type === "free_form_answer" && attributeValue === "free" && questiondatas.title.includes("free with text")) {
            cy.log("11", questiondatas.type)
            cy.log("it entered into free")
            let answer = questiondatas.answers;
            answer.forEach(ans => {
                cy.xpath("//textarea[@class='answer-given free active']").type(ans.answer_text);
            })
        } else if (questiondatas.type === "free_form_answer" && attributeValue === "free" && questiondatas.title.includes("free with attachment")) {
            const filepath = "media/certificate.pdf"
            cy.log(filepath)
            const fileName = ["certificate.pdf"]
            cy.fixture(filepath).then((fileContent) => {
                cy.get("input[name='media-upload']")
                    .wait(3000)
                    .attachFile({
                            fileContent, fileName, mimeType: 'docx/pdf/png/xlsx',
                        }
                    )
            })
        } else if (questiondatas.type === "free_form_answer" && attributeValue === "free" && questiondatas.title.includes("free with record")) {
            cy.xpath("//a[contains(@class,'record-start')]").click({force: true, multiple: true})
            cy.wait(5000)
            cy.xpath("//a[contains(@class,'record-stop')]").click({force: true, multiple: true})
        }
        // });
        this.click_validate_and_move_next_question_in_questionnaire_player();
    }

    AnswerAllTypeQuestionWithoutFree(questiondatas, attributeValue) {
        cy.log("Entered into main loop")
        questiondatas.forEach(data => {
            cy.log("data", data)
            if (data.type === "single" && attributeValue === "mcq") {
                cy.log("It entered into single Type")
                cy.log("1", data.type)
                let answer = data.answers;
                answer.forEach(ans => {
                    if (ans.correct === true) {
                        cy.wait(3000);
                        cy.xpath("//div//p[text()='" + ans.answer_text + "']").click({multiple: true});
                    }
                })
            } else if (data.type === "multi" && attributeValue === "mchk") {
                cy.log("It entered into multi Type")
                cy.log("2", data.type)
                let answer = data.answers;
                answer.forEach(ans => {
                    if (ans.correct === true) {
                        cy.wait(3000);
                        cy.xpath("//div//p[text()='" + ans.answer_text + "']")
                            .click({force: true, multiple: true});
                    }
                })
            } else if (data.type === "true_or_false" && attributeValue === "bool") {
                cy.log("It entered into true_or_false")
                cy.log("3", data.type)
                let answer = data.answers;
                answer.forEach(ans => {
                    if (ans.correct === true) {
                        cy.wait(3000);
                        cy.xpath("//div//p[text()='" + ans.answer_text + "']").click();
                    }
                })
            } else if (data.type === "match_items" && attributeValue === "match") {
                cy.log("It entered into match_the_following")
                cy.log("4", data.type)
                let answer = data.answers;
                cy.wait(3000);

                let questionArray = [];
                let answerArray = [];
                cy.xpath("//div[@class='match-left']//p").each(val => {
                    questionArray.push(val.text())
                    cy.log("q..........", questionArray);
                    cy.log("Check question[0]", questionArray[0])
                    cy.log("Check question[1]", questionArray[1])

                    cy.xpath("//div[@class='sort-content']//p").each(val1 => {
                        answerArray.push(val1.text())
                        cy.log("a.............", answerArray);
                        cy.log("Check answer[0]", answerArray[0])
                        cy.log("Check answer[1]", answerArray[1])

                        if (questionArray[0] === answer[0].case) {
                            cy.log("it enter into 1st if condition")
                            if (answerArray[0] === answer[0].match_text) {
                                cy.log("it enter into 2nd if condition")
                                cy.log("It's already in correct order")
                            } else {
                                cy.log("it enter into else")
                                cy.xpath("//div[@class='sort-content']//p[text()='" + answer[0].match_text + "']")
                                    .trigger("mousedown", {which: 1})
                                cy.xpath("//div[@class='sort-content']//p[text()='" + answer[1].match_text + "']")
                                    .trigger("mousemove")
                                    .trigger("mouseup", {force: true})
                            }
                        }
                    })
                })
            } else if (data.type === "sort_list" && attributeValue === "sort") {
                cy.log("it entered into sort_list")
                cy.log("5", data.type)
                let answer = data.answers;
                cy.wait(3000);

                cy.xpath("//div[@class='sortable ui-sortable']").then(val => {
                    let value = val.text();
                    cy.log("..........", value)
                    cy.log(answer[0].answer_text + " " + answer[1].answer_text + " " + answer[2].answer_text)

                    //here the wrap only for looping not for fetch data
                    cy.wrap(answer).each(data => {
                        cy.log("for loop", data)
                        if (value === answer[0].answer_text + " " + answer[1].answer_text + " " + answer[2].answer_text) {
                            cy.log("enters into if")
                        } else {
                            cy.log("enters into else")
                            cy.xpath("//div[@class='sort-content']//p[text()='" + answer[0].answer_text + "']")
                                .trigger("mousedown", {which: 1})
                            cy.get('.answer:nth-child(1)').first().trigger("mousemove", {force: true})
                                .trigger("mouseup", {force: true})

                            cy.xpath("//div[@class='sort-content']//p[text()='" + answer[1].answer_text + "']")
                                .trigger("mousedown", {which: 1})
                            cy.get('.answer:nth-child(2)').first().trigger("mousemove", {force: true})
                                .trigger("mouseup", {force: true})

                            cy.xpath("//div[@class='sort-content']//p[text()='" + answer[2].answer_text + "']")
                                .trigger("mousedown", {which: 1})
                            cy.get('.answer:nth-child(3)').first().trigger("mousemove", {force: true})
                                .trigger("mouseup", {force: true})
                        }
                    })
                })
            } else if (data.type === "fill" && attributeValue === "fill") {
                cy.log("it entered into fill")
                cy.log("6", data.type)
                let Answer = data.answers;
                Answer.forEach(ans => {
                    if (ans.correct === true) {
                        cy.wait(3000);
                        cy.xpath("//div//p//input[@class='fill-blank']").type(ans.answer);
                    }
                })
            } else if (data.type === "entered_answer" && attributeValue === "text") {
                cy.log("it entered into enter_answer")
                cy.log("7", data.type)
                let Answer = data.answers;
                let answer = data.answers;
                answer.forEach(ans => {
                    cy.wait(3000);
                    cy.xpath("//div//input[@class='answer-given active']").type(ans.answer_text);
                })
            } else if (data.type === "classify" && attributeValue === "classif") {
                cy.log("it entered into classification")
                cy.log("8", data.type)
                cy.wait(3000);
                cy.get('[data-name=_default] > div').then(questions => {
                    let lists = questions;
                    let i = 0;
                    cy.wrap(lists).each($elem => {
                        let value = $elem.text();
                        cy.wait(2000)
                        cy.xpath("//div[contains(@data-name,'answer_')]/div[contains(text(),'" + value + "')]/..").trigger("mousedown", {which: 1})
                        cy.get('[data-title=' + value + ']')
                            .trigger("mousemove")
                            .trigger("mouseup", {force: true})
                        cy.wait(2000)
                        i++
                    })
                })

            } else if (data.type === "Select" && attributeValue === "fsel") {
                cy.log("it entered into select_type")
                cy.log("9", data.type)
                let answer = data.answers;
                answer.forEach(ans => {
                    cy.wait(3000);
                    cy.xpath("(//span[@class='ui-selectmenu-text'])[1]").click();
                    cy.xpath("//li[@class='ui-menu-item']//div[text() = '" + ans.correct_answer + "']").click();
                })
            } else if (data.type === "softsingle" && attributeValue === "heart") {
                cy.log("10", data.type)
                cy.log("it entered into soft_skill_single")
                cy.wait(3000);
                cy.xpath("(//div[@class='answer answer-choice active'])[1]").click();
            } else if (data.type === "softmultiple" && attributeValue === "heart_multiple") {
                // cy.wait(9000);
                cy.log("11", data.type)
                cy.log("it entered into soft_skill_multiple")
                let answer = data.answers;
                answer.forEach(ans => {
                    let index = 1
                    // cy.wait(9000);
                    if (ans.correct === true) {
                        cy.xpath("(//div[@class='answer answer-multi-choice active'])[" + index + "]").click();
                    }
                })
            }
        });
        this.click_validate_and_move_next_question_in_questionnaire_player();
    }


    AnswerSingleTypeQuestionWithoutFree(questiondatas, attributeValue) {
        cy.log("Entered into main loop")
        questiondatas.forEach(data => {
            cy.log("data", data)
            if (data.type === "single" && attributeValue === "mcq") {
                cy.log("It entered into single Type")
                cy.log("1", data.type)
                let answer = data.answers;
                answer.forEach(ans => {
                    if (ans.correct === true) {
                        cy.wait(3000);
                        cy.xpath("//div//p[text()='" + ans.answer_text + "']").click({multiple: true});
                    }
                })
            } else if (data.type === "multi" && attributeValue === "mchk") {
                cy.log("It entered into multi Type")
                cy.log("2", data.type)
                let answer = data.answers;
                answer.forEach(ans => {
                    if (ans.correct === true) {
                        cy.wait(3000);
                        cy.xpath("//div//p[text()='" + ans.answer_text + "']")
                            .click({force: true, multiple: true});
                    }
                })
            } else if (data.type === "true_or_false" && attributeValue === "bool") {
                cy.log("It entered into true_or_false")
                cy.log("3", data.type)
                let answer = data.answers;
                answer.forEach(ans => {
                    if (ans.correct === true) {
                        cy.wait(3000);
                        cy.xpath("//div//p[text()='" + ans.answer_text + "']").click();
                    }
                })
            } else if (data.type === "match_items" && attributeValue === "match") {
                cy.log("It entered into match_the_following")
                cy.log("4", data.type)
                let answer = data.answers;
                cy.wait(3000);

                let questionArray = [];
                let answerArray = [];
                cy.xpath("//div[@class='match-left']//p").each(val => {
                    questionArray.push(val.text())
                    cy.log("q..........", questionArray);
                    cy.log("Check question[0]", questionArray[0])
                    cy.log("Check question[1]", questionArray[1])

                    cy.xpath("//div[@class='sort-content']//p").each(val1 => {
                        answerArray.push(val1.text())
                        cy.log("a.............", answerArray);
                        cy.log("Check answer[0]", answerArray[0])
                        cy.log("Check answer[1]", answerArray[1])

                        if (questionArray[0] === answer[0].case) {
                            cy.log("it enter into 1st if condition")
                            if (answerArray[0] === answer[0].match_text) {
                                cy.log("it enter into 2nd if condition")
                                cy.log("It's already in correct order")
                            } else {
                                cy.log("it enter into else")
                                cy.xpath("//div[@class='sort-content']//p[text()='" + answer[0].match_text + "']")
                                    .trigger("mousedown", {which: 1})
                                cy.xpath("//div[@class='sort-content']//p[text()='" + answer[1].match_text + "']")
                                    .trigger("mousemove")
                                    .trigger("mouseup", {force: true})
                            }
                        }
                    })
                })
            } else if (data.type === "sort_list" && attributeValue === "sort") {
                cy.log("it entered into sort_list")
                cy.log("5", data.type)
                let answer = data.answers;
                cy.wait(3000);

                cy.xpath("//div[@class='sortable ui-sortable']").then(val => {
                    let value = val.text();
                    cy.log("..........", value)
                    cy.log(answer[0].answer_text + " " + answer[1].answer_text + " " + answer[2].answer_text)

                    //here the wrap only for looping not for fetch data
                    cy.wrap(answer).each(data => {
                        cy.log("for loop", data)
                        if (value === answer[0].answer_text + " " + answer[1].answer_text + " " + answer[2].answer_text) {
                            cy.log("enters into if")
                        } else {
                            cy.log("enters into else")
                            cy.xpath("//div[@class='sort-content']//p[text()='" + answer[0].answer_text + "']")
                                .trigger("mousedown", {which: 1})
                            cy.get('.answer:nth-child(1)').first().trigger("mousemove", {force: true})
                                .trigger("mouseup", {force: true})

                            cy.xpath("//div[@class='sort-content']//p[text()='" + answer[1].answer_text + "']")
                                .trigger("mousedown", {which: 1})
                            cy.get('.answer:nth-child(2)').first().trigger("mousemove", {force: true})
                                .trigger("mouseup", {force: true})

                            cy.xpath("//div[@class='sort-content']//p[text()='" + answer[2].answer_text + "']")
                                .trigger("mousedown", {which: 1})
                            cy.get('.answer:nth-child(3)').first().trigger("mousemove", {force: true})
                                .trigger("mouseup", {force: true})
                        }
                    })
                })
            } else if (data.type === "fill" && attributeValue === "fill") {
                cy.log("it entered into fill")
                cy.log("6", data.type)
                let Answer = data.answers;
                Answer.forEach(ans => {
                    if (ans.correct === true) {
                        cy.wait(3000);
                        cy.xpath("//div//p//input[@class='fill-blank']").type(ans.answer);
                    }
                })
            } else if (data.type === "entered_answer" && attributeValue === "text") {
                cy.log("it entered into enter_answer")
                cy.log("7", data.type)
                let Answer = data.answers;
                let answer = data.answers;
                answer.forEach(ans => {
                    cy.wait(3000);
                    cy.xpath("//div//input[@class='answer-given active']").type(ans.answer_text);
                })
            } else if (data.type === "classify" && attributeValue === "classif") {
                cy.log("it entered into classification")
                cy.log("8", data.type)
                cy.wait(3000);
                cy.get('[data-name=_default] > div').then(questions => {
                    let lists = questions;
                    let i = 0;
                    cy.wrap(lists).each($elem => {
                        let value = $elem.text();
                        cy.wait(2000)
                        cy.xpath("//div[contains(@data-name,'answer_')]/div[contains(text(),'" + value + "')]/..").trigger("mousedown", {which: 1})
                        cy.get('[data-title=' + value + ']')
                            .trigger("mousemove")
                            .trigger("mouseup", {force: true})
                        cy.wait(2000)
                        i++
                    })
                })

            } else if (data.type === "Select" && attributeValue === "fsel") {
                cy.log("it entered into select_type")
                cy.log("9", data.type)
                let answer = data.answers;
                answer.forEach(ans => {
                    cy.wait(3000);
                    cy.xpath("(//span[@class='ui-selectmenu-text'])[1]").click();
                    cy.xpath("//li[@class='ui-menu-item']//div[text() = '" + ans.correct_answer + "']").click();
                })
            } else if (data.type === "softsingle" && attributeValue === "heart") {
                cy.log("10", data.type)
                cy.log("it entered into soft_skill_single")
                cy.wait(3000);
                cy.xpath("(//div[@class='answer answer-choice active'])[1]").click();
            } else if (data.type === "softmultiple" && attributeValue === "heart_multiple") {
                // cy.wait(9000);
                cy.log("11", data.type)
                cy.log("it entered into soft_skill_multiple")
                let answer = data.answers;
                answer.forEach(ans => {
                    let index = 1
                    // cy.wait(9000);
                    if (ans.correct === true) {
                        cy.xpath("(//div[@class='answer answer-multi-choice active'])[" + index + "]").click();
                    }
                })
            }
        });
        this.click_validate_and_move_next_question_in_questionnaire_player();
    }

    click_more_setting_button_on_create_question_page() {
        cy.xpath("//a[@id='more-settings-btn']/span[@class='closed']")
            .click()
            .log("successfully clicked the more setting button on question creation page")
    }

    enable_with_attachment() {
        cy.xpath("//label[@for='answer_with_attachment']//span[text()='Yes']").click()
    }

    enable_attachment_audio() {
        cy.xpath("//label[@for='audio_attachment']//span[text()='Yes']").click()
    }

    disable_text_area() {
        cy.xpath("//label[@for='hide_textarea']//span[text()='Yes']").click()
    }


}

export default QuestionPage;