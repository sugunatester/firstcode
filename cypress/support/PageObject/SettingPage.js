class SettingPage {

    click_setting_side_menu() {
        cy.xpath("//a[@href='/user/enterprise']/..//span[text()='Settings']", {timeout: 50000})
            .click()
            .log("successfully clicked the settings side menu")

    }

    click_setting_tab() {
        cy.xpath("//a[@href='/enterprise/options' and text()='Settings ']", {timeout: 50000})
            .click()
            .log("successfully clicked the settings tab")
    }

    enable_only_administration_side_menu() {
        this.click_setting_side_menu()
        this.click_setting_tab()
        this.enable_questionnaire_side_menu()
        this.enable_question_side_menu()
        this.enable_media_side_menu()
        this.enable_result_side_menu()
        this.enable_show_cycle_checkbox()
        this.enable_cycle_side_menu()
        this.enable_show_skill_checkbox()
        this.enable_skill_side_menu()
        this.enable_training_side_menu()
        this.enable_module_side_menu()
        this.enable_path_side_menu()
        this.enable_show_survey_checkbox()
        this.enable_survey_side_menu()
        this.enable_certificate_side_menu()
        this.enable_chat_side_menu()
        this.enable_show_business_intelligence_checkbox()
        this.enable_business_intelligence_side_menu()
        this.disable_more_side_menu()
        this.click_to_save_the_setting_page()
    }

    verify_all_administration_side_menu() {
        this.verify_questionnaire_side_menu_is_exist()
        this.verify_question_side_menu_is_exist()
        this.verify_medias_side_menu_is_exist()
        this.verify_result_side_menu_is_exist()
        this.verify_cycle_side_menu_is_exist()
        this.verify_skill_side_menu_is_exist()
        this.verify_training_side_menu_is_exist()
        this.verify_modules_side_menu_is_exist()
        this.verify_path_side_menu_is_exist()
        this.verify_survey_side_menu_is_exist()
        this.verify_certificates_side_menu_is_exist()
        this.verify_chats_side_menu_is_exist()
        this.verify_bi_side_menu_is_exist()
    }

    enable_only_administrator_dashboard() {
        this.click_setting_side_menu()
        this.click_setting_tab()
        this.enable_calendar_on_dashboard_option()
        this.enable_dashboard_rank_option()
        this.click_to_save_the_setting_page()
    }

    enable_only_enterprise(admindata, settingdata, commondata) {
        this.click_setting_side_menu()
        this.click_setting_tab()
        this.enable_show_enterprise_logo_option()
        this.enable_use_advanced_permission_option()
        this.enable_use_hierarchy_for_permissions_option()
        this.enable_use_tag_option()
        this.enable_use_title_option()
        this.enable_GDPR_message_option()
        this.enter_GDPR_message(settingdata)
        this.enable_GDPR_for_guest_and_contact()
        this.enable_contact_message_to_administrator_option()
        this.enter_contact_mail(admindata)
        this.enter_sender_for_email(admindata)
        this.enter_redirect_url_after_activity(commondata)
        this.click_to_save_the_setting_page()
    }

    enable_only_specific_function() {
        this.click_setting_side_menu()
        this.click_setting_tab()
        this.enable_use_mailing_option()
        this.enable_use_certificates_generator_option()
        this.enable_define_a_glossary_option()
        this.enable_use_topic_transfer_option()
        this.click_to_save_the_setting_page()
    }

    enable_only_player() {
        cy.wait(1000)
        this.click_setting_side_menu()
        this.click_setting_tab()
        this.enable_player_setting_option()
        this.enable_color_setting_option()
        this.enable_player_message_option()
        this.click_to_save_the_setting_page()
    }

    enable_only_bases() {
        this.click_setting_side_menu()
        this.click_setting_tab()
        this.enable_show_base_color_option()
        this.enable_base_archive_button_option()
        this.enable_question_quality_panel_option()
        this.click_to_save_the_setting_page()
    }

    enable_only_medias() {
        this.click_setting_side_menu()
        this.click_setting_tab()
        this.enable_use_media_subfolders_option()
        this.enable_use_more_images_option()
        this.click_to_save_the_setting_page()
    }

    enable_only_questionnaires() {
        this.click_setting_side_menu()
        this.click_setting_tab()
        this.enable_number_of_question_by_domain_option()
        this.enable_display_evaluation_consequence_option()
        this.enable_renewal_evaluation_period_option()
        this.enable_save_questionnaire_email_option()
        this.enable_copy_DOCX_questionnaire_option()
        this.enable_correction_document_for_questionnaire_option()
        this.enable_correction_document_for_evaluation_option()
        this.enable_display_evaluation_rank_option()
        this.enable_display_grade_20_option()
        this.enable_filter_by_sender_in_evaluation_list_option()
        this.enable_define_group_permission_on_questionnaire_option()
        this.click_to_save_the_setting_page()
    }

    enable_only_skills() {
        this.click_setting_side_menu()
        this.click_setting_tab()
        this.enable_use_qualification()
        this.enable_use_job()
        this.enable_self_registration_for_user_option()
        this.enable_display_only_validated_and_registered_block_and_qualification_option()
        this.click_to_save_the_setting_page()
    }

    enable_only_trainings() {
        this.click_setting_side_menu()
        this.click_setting_tab()
        this.enable_training_blackboard_option()
        this.enable_training_satisfaction_cycle_option()
        this.enable_training_attendance_sheet_option()
        this.click_to_save_the_setting_page()
    }

    enable_only_exports(settingdata) {
        this.click_setting_side_menu()
        this.click_setting_tab()
        this.enable_user_export_option()
        this.enable_question_export_flat_option()
        this.enable_question_export_option()
        this.enable_question_export_json_option()
        this.enable_question_export_docx_option()
        this.enter_excel_footer_message(settingdata)
        this.click_to_save_the_setting_page()
    }

    enable_only_user_data() {
        this.click_setting_side_menu()
        this.click_setting_tab()
        this.enable_user_modify_their_own_profile_option()
        this.enable_direction_option()
        this.enable_pole_option()
        this.enable_team_option()
        this.click_to_save_the_setting_page()
    }

    enable_only_access_control() {
        cy.wait(1000)
        this.click_setting_side_menu()
        this.click_setting_tab()
        this.enable_block_account_after_3_failed_connection_option()
        this.enable_require_upper_and_lowercase_characters_inside_passwords_option()
        this.enable_require_non_alphanumeric_characters_inside_passwords_option()
        this.click_to_save_the_setting_page()
    }

//ENABLE ALL ADMINISTRATOR SIDE MENU ON SETTING TAB:

    enable_questionnaire_side_menu() {
        cy.xpath("//input[@name='main_menu_show_questionnaires']//..//span[@class='fa fa-square-o unchecked ']")
            .click()
            .log("successfully enabled the questionnaire side menu")

    }

    enable_question_side_menu() {
        cy.xpath("//input[@name='main_menu_show_questions']//..//span[@class='fa fa-square-o unchecked ']")
            .click()
            .log("successfully enabled the question side menu")

    }

    enable_media_side_menu() {
        cy.xpath("//input[@name='main_menu_show_media']//..//span[@class='fa fa-square-o unchecked ']")
            .click()
            .log("successfully enabled the media side menu")

    }

    enable_result_side_menu() {
        cy.xpath("//input[@name='main_menu_show_evaluations']//..//span[@class='fa fa-square-o unchecked ']")
            .click()
            .log("successfully enabled the result side menu")

    }

    enable_cycle_side_menu() {
        cy.xpath("//input[@name='main_menu_show_cycles']//..//span[@class='fa fa-square-o unchecked ']")
            .click()
            .log("successfully enabled the cycle side menu")

    }

    enable_skill_side_menu() {
        cy.xpath("//input[@name='main_menu_show_skills']//..//span[@class='fa  fa-check-square-o checked ']")
            .click({force: true})
            .log("successfully enabled the skill side menu")

    }

    enable_training_side_menu() {
        cy.xpath("//input[@name='main_menu_show_trainings']//..//span[@class='fa fa-square-o unchecked ']")
            .click()
            .log("successfully enabled the training side menu")

    }

    enable_module_side_menu() {
        cy.xpath("//input[@name='main_menu_show_modules']//..//span[@class='fa fa-square-o unchecked ']")
            .click()
            .log("successfully enabled the module side menu")

    }

    enable_path_side_menu() {
        cy.xpath("//input[@name='main_menu_show_paths']//..//span[@class='fa fa-square-o unchecked ']")
            .click()
            .log("successfully enabled the path side menu")

    }

    enable_survey_side_menu() {
        cy.xpath("//input[@name='main_menu_show_forms']//..//span[@class='fa fa-square-o unchecked ']")
            .click()
            .log("successfully enabled the survey side menu")

    }

    enable_certificate_side_menu() {
        cy.xpath("//input[@name='main_menu_show_certificates']//..//span[@class='fa fa-square-o unchecked ']")
            .click()
            .log("successfully enabled the certificate side menu")

    }

    enable_chat_side_menu() {
        cy.xpath("//input[@name='main_menu_show_chats']//..//span[@class='fa fa-square-o unchecked ']")
            .click()
            .log("successfully enabled the chat side menu")

    }

    enable_business_intelligence_side_menu() {
        cy.xpath("//input[@name='main_menu_show_business_intelligence']//..//span[@class='fa fa-square-o unchecked ']")
            .click()
            .log("successfully enabled the business_intelligence side menu")

    }

    disable_more_side_menu() {
        cy.xpath("//input[@id='main_menu_show_extras']/..//span[@class='fa  fa-check-square-o checked ']")
            .then($button => {
                if ($button.is(':visible')) {
                    cy.xpath("//input[@id='main_menu_show_extras']/..//span[@class='fa  fa-check-square-o checked ']")
                        .click()
                } else {
                    cy.xpath("//input[@id='main_menu_show_extras']/..//span[@class='fa fa-square-o unchecked ']")
                        .should('be.visible')
                    cy.log('the more side menu is disabled')
                }
            })
    }

    // ENABLE ALL ADMINISTRATOR DASHBOARD OPTION ON SETTING TAB:

    enable_calendar_on_dashboard_option() {
        cy.xpath("//input[@name='show_calendar']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the calendar on dashboard option")

    }

    enable_dashboard_rank_option() {
        cy.xpath("//input[@name='show_dashboard_rank']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the dashboard rank option")

    }


//ENABLE ALL ENTERPRISE OPTION ON SETTING TAB:

    enable_show_enterprise_logo_option() {
        cy.xpath("//input[@name='show_enterprise_logo']//..//span[@class='fa fa-toggle-off unchecked ']", {timeout: 50000})
            .click()
            .log("successfully enabled the show enterprise logo option")
    }

    enable_use_advanced_permission_option() {
        cy.xpath("//input[@name='show_advanced_permissions']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the Use advanced permissions option")

    }

    enable_use_hierarchy_for_permissions_option() {
        cy.xpath("//input[@name='show_hierarchy']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the Use hierarchy for permissions option")

    }

    enable_use_tag_option() {
        cy.xpath("//input[@name='show_tags']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the Use tag option")
    }

    enable_use_title_option() {
        cy.xpath("//input[@name='show_title']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the Use title option")

    }

    enable_GDPR_message_option() {
        cy.xpath("//input[@name='show_gdpr_message']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the GDPR message option")

    }

    enter_GDPR_message(data) {
        let text = cy.xpath("(.//div[@role='textbox']/p[1])").clear()
        // let text2 = cy.xpath("(.//div[@role='textbox']/p[2])").clear()
        // let text3 = cy.xpath("(.//div[@role='textbox']/p[3])").clear()
        // return cy.xpath("//*[@class='cke_textarea_inline cke_editable cke_editable_inline cke_contents_ltr cke_show_borders']")
        cy.get('.cke_textarea_inline')
            .type(data["gdpr_message"])
            .log("successfully enter the GDPR message successfully")

    }

    enable_GDPR_for_guest_and_contact() {
        cy.xpath("//input[@name='show_gdpr_for_guests']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the GDPR for contact and guest user option")
    }

    enable_contact_message_to_administrator_option() {
        cy.xpath("//input[@name='show_user_contact_panel']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the Contact message to the administrator option")

    }

    enter_contact_mail(admin_email_address) {

        let email_id;
        email_id = admin_email_address["user_name"] + admin_email_address["domain"];
        cy.xpath("//input[@id='contact_email']")
            .type((email_id))
            .log("successfully entered the contact mail address")

    }

    enable_captcha_for_unidentified_user_before_stating_a_test_option() {
        cy.xpath("//input[@name='show_captcha_for_guests']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the captcha for unidentified user before stating test option")

    }

    enter_sender_for_email(admin_email_address) {

        let email_id;
        email_id = admin_email_address["user_name"] + admin_email_address["domain"];
        cy.xpath("//input[@id='custom_sender_email']")
            .type(email_id)
            .log("successfully entered the sender mail address")

    }

    enter_redirect_url_after_activity(data1) {
        cy.xpath("//input[@id='url_after']")
            .type(data1["redirect_url"])
            .log("successfully entered the redirect url after activity")

    }


//ENABLE ALL SPECIFIC FUNCTION OPTION ON SETTING TAB:

    enable_use_mailing_option() {
        cy.xpath("//input[@name='show_mailing']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the use mailing option")

    }

    enable_show_cycle_checkbox() {
        cy.xpath("//input[@name='show_cycles']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the cycle checkbox option")

    }

    enable_show_survey_checkbox() {
        cy.xpath("//input[@name='show_survey']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the survey checkbox option")

    }

    enable_use_certificates_generator_option() {
        cy.xpath("//input[@name='show_certificate']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the use certificates generator option")

    }

    enable_define_a_glossary_option() {
        cy.xpath("//input[@name='use_glossary']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the define a glossary option")

    }

    enable_use_topic_transfer_option() {
        cy.xpath("//input[@name='use_topic_transfer']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the use topic transfer option")

    }

    enable_show_business_intelligence_checkbox() {
        cy.xpath("//input[@name='with_business_intelligence']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the survey checkbox option")

    }

//ENABLE ALL PLAYER OPTION ON SETTING TAB:

    enable_player_setting_option() {
        cy.xpath("//input[@name='show_player_settings']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the player setting option")

    }

    enable_color_setting_option() {
        cy.xpath("//input[@name='show_color_settings']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the color setting option")

    }

    enable_player_message_option() {
        cy.xpath("//input[@name='show_player_messages']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the player message option")

    }

//ENABLE ALL BASES OPTION ON SETTING TAB:

    enable_show_base_color_option() {
        cy.xpath("//input[@name='show_topic_color_field']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the show base color option")

    }

    enable_base_archive_button_option() {
        cy.xpath("//input[@name='show_archive_topics']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the Bases archive button option")

    }

    enable_question_quality_panel_option() {
        cy.xpath("//input[@name='show_question_quality_panel']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the Question quality panel option")
    }

//ENABLE ALL MEDIA OPTION ON SETTING TAB:

    enable_use_media_subfolders_option() {
        cy.xpath("//input[@name='use_media_subfolders']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the Use medias subfolders option")
    }

    enable_use_more_images_option() {
        cy.xpath("//input[@name='use_more_images']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the Use more images option")

    }

//ENABLE ALL QUESTIONNAIRE OPTION ON SETTING TAB:

    enable_number_of_question_by_domain_option() {
        cy.xpath("//input[@name='show_questionnaire_dyn_specif_questions']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the Number of questions by domains in questionnaires option")

    }

    enable_question_feedback_in_player_option() {
        cy.xpath("//input[@name='show_question_feedback']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the question feedback in player option")

    }

    enable_display_evaluation_consequence_option() {
        cy.xpath("//input[@name='show_evaluation_consequence']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the evaluation consequence option")

    }

    enable_renewal_evaluation_period_option() {
        cy.xpath("//input[@name='show_renewal_warning']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the renewal evaluation period option")

    }

    enable_save_questionnaire_email_option() {
        cy.xpath("//input[@name='save_questionnaire_email']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the save questionnaire email option")

    }

    enable_copy_DOCX_questionnaire_option() {
        cy.xpath("//input[@name='show_questionnaire_copy_docx']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the copy DOCX questionnaire option")
    }

    enable_correction_document_for_questionnaire_option() {
        cy.xpath("//input[@name='show_questionnaire_correction']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the correction document questionnaire option")
    }

    enable_correction_document_for_evaluation_option() {
        cy.xpath("//input[@name='show_evaluation_correction']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the correction document for evaluation option")
    }

    enable_display_evaluation_rank_option() {
        cy.xpath("//input[@name='show_evaluation_rank']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the evaluation rank option")
    }

    enable_display_grade_20_option() {
        cy.xpath("//input[@name='show_grade_20']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the display grade of 20 option")

    }

    enable_filter_by_sender_in_evaluation_list_option() {
        cy.xpath("//input[@name='show_leader_filter']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the filter by sender in evaluation list option")

    }

    enable_define_group_permission_on_questionnaire_option() {
        cy.xpath("//input[@name='show_group_questionnaire_permission']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the group permission on questionnaire option")

    }


//ENABLE ALL SKILL OPTION ON SETTING TAB:

    enable_show_skill_checkbox() {
        cy.xpath("//input[@name='show_skills_blocks']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the skill checkbox option")

    }

    enable_use_qualification() {
        cy.xpath("//input[@name='show_qualifications']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the qualification option")

    }

    enable_use_job() {
        cy.xpath("//input[@name='show_jobs']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the job option")

    }

    enable_self_registration_for_user_option() {
        cy.xpath("//input[@name='show_self_registration']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the self registration for user option")
    }

    enable_display_only_validated_and_registered_block_and_qualification_option() {
        cy.xpath("//input[@name='show_user_only_validated_skills']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled to display only validated and registered skills and qualification option")
    }

//ENABLE ALL TRAINING OPTION ON SETTING TAB:

    enable_training_blackboard_option() {
        cy.xpath("//input[@name='show_training_blackboard']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the training blackboard option")
    }

    enable_training_satisfaction_cycle_option() {
        cy.xpath("//input[@name='show_training_cycles']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the training satisfaction cycle option")
    }

    enable_training_attendance_sheet_option() {
        cy.xpath("//input[@name='show_training_attendance']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the training attendance sheet option")
    }

    enable_training_satisfaction_feedback_option() {
        cy.xpath("//input[@name='show_training_satisfaction']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the training satisfaction feedback option")
    }

//ENABLE ALL EXPORT OPTION ON SETTING TAB:

    enable_user_export_option() {
        cy.xpath("//input[@name='show_users_export_xlsx']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the user export option")

    }

    enable_question_export_flat_option() {
        cy.xpath("//input[@name='show_question_exports_flat']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the question export flat option")

    }

    enable_question_export_option() {
        cy.xpath("//input[@name='show_question_exports']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the question export option")

    }

    enable_question_export_json_option() {
        cy.xpath("//input[@name='show_question_exports_json']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the question export json option")

    }

    enable_question_export_docx_option() {
        cy.xpath("//input[@name='show_question_exports_docx']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the question export docx option")

    }

    enter_excel_footer_message(settingdata) {
        cy.xpath("//*[@id='excel_footer']")
            .type(settingdata["excel_footer"])
            .log("successfully entered the excel footer message")

    }

//ENABLE ALL USER OPTION ON SETTING TAB:

    enable_user_modify_their_own_profile_option() {
        cy.xpath("//input[@name='users_may_change_profile']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the user modify own profile data option")

    }

    enable_direction_option() {
        cy.xpath("//input[@name='direction_show']//..//span[@class='fa fa-square-o unchecked ']")
            .click()
            .log("successfully enabled the direction option")

    }

    enable_pole_option() {
        cy.xpath("//input[@name='pole_show']//..//span[@class='fa fa-square-o unchecked ']")
            .click()
            .log("successfully enabled the pole option")

    }

    enable_department_option() {
        cy.xpath("//input[@name='department_show']//..//span[@class='fa fa-square-o unchecked ']")
            .click()
            .log("successfully enabled the department option")

    }

    enable_team_option() {
        cy.xpath("//input[@name='team_show']//..//span[@class='fa fa-square-o unchecked ']")
            .click()
            .log("successfully enabled the team option")

    }

    enable_employee_id_option() {
        cy.xpath("//input[@name='employee_id_show']//..//span[@class='fa fa-square-o unchecked ']")
            .click()
            .log("successfully enabled the employee id option")

    }

    enable_job_option() {
        cy.xpath("//input[@name='job_title_show']//..//span[@class='fa fa-square-o unchecked ']")
            .click()
            .log("successfully enabled the job option")

    }

    enable_birth_date_option() {
        cy.xpath("//input[@name='birth_date_show']//..//span[@class='fa fa-square-o unchecked ']")
            .click()
            .log("successfully enabled the birth date option")

    }

    enable_hiring_date_option() {
        cy.xpath("//input[@name='hiring_date_show']//..//span[@class='fa fa-square-o unchecked ']")
            .click()
            .log("successfully enabled the hiring date option")

    }

    enable_fix_phone_option() {
        cy.xpath("//input[@name='phone_fix_show']//..//span[@class='fa fa-square-o unchecked ']")
            .click()
            .log("successfully enabled the fix phone option")

    }

    enable_enterprise_option() {
        cy.xpath("//input[@name='enterprise_show']//..//span[@class='fa fa-square-o unchecked ']")
            .click()
            .log("successfully enabled the enterprise option")

    }

//ENABLE ALL USER OPTION ON SETTING TAB:

    enable_block_account_after_3_failed_connection_option() {
        cy.xpath("//input[@name='password_3_attempts']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the block account after 3 failed connection option")

    }

    enable_require_upper_and_lowercase_characters_inside_passwords_option() {
        cy.xpath("//input[@name='password_upper_lowercase']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the Require upper and lowercase characters inside passwords option")

    }

    enable_require_non_alphanumeric_characters_inside_passwords_option() {
        cy.xpath("//input[@name='password_non_alpha']//..//span[@class='fa fa-toggle-off unchecked ']")
            .click()
            .log("successfully enabled the Require non-alphanumeric characters inside passwords option")

    }

//CLICK THE SAVE BUTTON ON SETTING TAB:

    click_to_save_the_setting_page() {
        cy.xpath("(//*[@class='btn btn-primary'])[1]", {timeout: 500000})
            .click()
            .log("successfully saved the settings page")

    }

//VERIFY ALL ADMIN SIDE MENU IS EXIST AFTER ENABLING ALL ADMIN SIDE MENU:

    verify_questionnaire_side_menu_is_exist() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/enterprise/questionnaires'] span[class='text']").length > 0) {
                cy.get("a[href='/enterprise/questionnaires'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("Questionnaires")) {
                            cy.log('the questionnaire menu is exist')
                        } else {
                            assert.notExists(null, 'questionnaire menu is not exist')

                        }
                    });
            } else {
                assert.notExists(null, 'questionnaire menu is not exist')

            }
        });
    }

    verify_question_side_menu_is_exist() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/enterprise/questions/all'] span[class='text']").length > 0) {
                cy.get("a[href='/enterprise/questions/all'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("Questions")) {
                            cy.log('the question menu is exist')
                        } else {
                            assert.notExists(null, 'question menu is not exist')

                        }
                    });
            } else {
                assert.notExists(null, 'question menu is not exist')

            }
        });
    }

    verify_medias_side_menu_is_exist() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/enterprise/media'] span[class='text']").length > 0) {
                cy.get("a[href='/enterprise/media'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("Medias")) {
                            cy.log('the Medias menu is exist')
                        } else {
                            assert.notExists(null, 'Medias menu is not exist')

                        }
                    });
            } else {
                assert.notExists(null, 'Medias menu is not exist')

            }
        });
    }

    verify_result_side_menu_is_exist() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/enterprise/evaluations'] span[class='text']").length > 0) {
                cy.get("a[href='/enterprise/evaluations'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("Results")) {
                            cy.log('the results menu is exist')
                        } else {
                            assert.notExists(null, 'results menu is not exist')

                        }
                    });
            } else {
                assert.notExists(null, 'results menu is not exist')

            }
        });
    }

    verify_cycle_side_menu_is_exist() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/cycles/list'] span[class='text']").length > 0) {
                cy.get("a[href='/cycles/list'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("Cycles")) {
                            cy.log('the cycle menu is exist')
                        } else {
                            assert.notExists(null, 'cycle menu is not exist')

                        }
                    });
            } else {
                assert.notExists(null, 'cycle menu is not exist')

            }
        });
    }

    verify_skill_side_menu_is_exist() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/enterprise/skills/list'] span[class='text']").length > 0) {
                cy.get("a[href='/enterprise/skills/list'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("Skills")) {
                            cy.log('the skill menu is exist')
                        } else {
                            assert.notExists(null, 'skill menu is not exist')

                        }
                    });
            } else {
                assert.notExists(null, 'skill menu is not exist')

            }
        });
    }

    verify_training_side_menu_is_exist() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/enterprise/trainings'] span[class='text']").length > 0) {
                cy.get("a[href='/enterprise/trainings'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("Trainings")) {
                            cy.log('the trainings menu is exist')
                        } else {
                            assert.notExists(null, 'trainings menu is not exist')

                        }
                    });
            } else {
                assert.notExists(null, 'trainings menu is not exist')

            }
        });
    }

    verify_modules_side_menu_is_exist() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/enterprise/modules/list'] span[class='text']").length > 0) {
                cy.get("a[href='/enterprise/modules/list'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("Modules")) {
                            cy.log('the module menu is exist')
                        } else {
                            assert.notExists(null, 'module menu is not exist')
                        }
                    });
            } else {
                assert.notExists(null, 'module menu is not exist')

            }
        });
    }

    verify_path_side_menu_is_exist() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/enterprise/paths/list'] span[class='text']").length > 0) {
                cy.get("a[href='/enterprise/paths/list'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("Paths")) {
                            cy.log('the path menu is exist')
                        } else {
                            assert.notExists(null, 'path menu is not exist')
                        }
                    });
            } else {
                assert.notExists(null, 'path menu is not exist')

            }
        });
    }

    verify_survey_side_menu_is_exist() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/surveys'] span[class='text']").length > 0) {
                cy.get("a[href='/surveys'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("Surveys")) {
                            cy.log('the survey menu is exist')
                        } else {
                            assert.notExists(null, 'survey menu is not exist')
                        }
                    });
            } else {
                assert.notExists(null, 'survey menu is not exist')

            }
        });
    }

    verify_certificates_side_menu_is_exist() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/enterprise/certificates/view'] span[class='text']").length > 0) {
                cy.get("a[href='/enterprise/certificates/view'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("Certificates")) {
                            cy.log('the certificates menu is exist')
                        } else {
                            assert.notExists(null, 'certificates menu is not exist')
                        }
                    });
            } else {
                assert.notExists(null, 'certificates menu is not exist')

            }
        });
    }

    verify_chats_side_menu_is_exist() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/enterprise/chats/list'] span[class='text']").length > 0) {
                cy.get("a[href='/enterprise/chats/list'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("Chats")) {
                            cy.log('the chats menu is exist')
                        } else {
                            assert.notExists(null, 'chats menu is not exist')
                        }
                    });
            } else {
                assert.notExists(null, 'chats menu is not exist')

            }
        });
    }

    verify_bi_side_menu_is_exist() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/enterprise/bi/view/dashboards/list'] span[class='text']").length > 0) {
                cy.get("a[href='/enterprise/bi/view/dashboards/list'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("Business Intelligence")) {
                            cy.log('the bi menu is exist')
                        } else {
                            assert.notExists(null, 'bi menu is not exist')
                        }
                    });
            } else {
                assert.notExists(null, 'bi menu is not exist')

            }
        });
    }


//ON CUSTOMIZATION TAB:

    click_customization_tab() {
        cy.xpath("//a[@href='/enterprise/user_menu' and text()='Customization']")
            .click()
            .log("successfully clicked the settings tab")

    }

    click_user_menu_tab_on_customization_page() {
        cy.xpath("//a[@href='/enterprise/user_menu' and text()='User Menu']")
            .click()
            .log("successfully clicked the settings tab")

    }

    verify_and_enable_employee_checkbox_on_user_type_section() {
        cy.xpath("//label[contains(text(),'Employee')]/..//span[@class='fa  fa-check-square-o checked ']")
            .then($button => {
                if ($button.is(':visible')) {
                    cy.log('the employee checkbox is enabled')
                } else {
                    cy.xpath("//label[contains(text(),'Employee')]/..//span[@class='fa fa-square-o unchecked ']")
                        .click()
                }
            })
    }

    verify_and_enable_client_checkbox_on_user_type_section() {
        cy.xpath("//label[contains(text(),'Client')]/..//span[@class='fa  fa-check-square-o checked ']")
            .then($button => {
                if ($button.is(':visible')) {
                    cy.log('the Client checkbox is enabled')
                } else {
                    cy.xpath("//label[contains(text(),'Client')]/..//span[@class='fa fa-square-o unchecked ']")
                        .click()
                }
            })
    }

    verify_and_enable_trainee_checkbox_on_user_type_section() {
        cy.xpath("//label[contains(text(),'Trainee')]/..//span[@class='fa  fa-check-square-o checked ']")
            .then($button => {
                if ($button.is(':visible')) {
                    cy.log('the Trainee checkbox is enabled')
                } else {
                    cy.xpath("//label[contains(text(),'Trainee')]/..//span[@class='fa fa-square-o unchecked ']")
                        .click()
                }
            })
    }

    verify_and_enable_contractor_checkbox_on_user_type_section() {
        cy.xpath("//label[contains(text(),'Contractor')]/..//span[@class='fa  fa-check-square-o checked ']")
            .then($button => {
                if ($button.is(':visible')) {
                    cy.log('the Contractor checkbox is enabled')
                } else {
                    cy.xpath("//label[contains(text(),'Contractor')]/..//span[@class='fa fa-square-o unchecked ']")
                        .click()
                }
            })
    }

    verify_and_enable_other_checkbox_on_user_type_section() {
        cy.xpath("//label[contains(text(),'Other')]/..//span[@class='fa  fa-check-square-o checked ']")
            .then($button => {
                if ($button.is(':visible')) {
                    cy.log('the Other checkbox is enabled')
                } else {
                    cy.xpath("//label[contains(text(),'Other')]/..//span[@class='fa fa-square-o unchecked ']")
                        .click()
                }
            })
    }

    verify_and_enable_all_employee_side_menu_on_employee_section() {
        cy.xpath("//input[@id='employee_user_dashboard_show_all']/..//span[@class='fa  fa-check-square-o checked ']")
            .then($button => {
                if ($button.is(':visible')) {
                    cy.log('the all employee side menu is enabled')
                } else {
                    cy.xpath("//input[@id='employee_user_dashboard_show_all']/..//span[@class='fa fa-square-o unchecked ']")
                        .click()
                }
            })
    }

    verify_and_enable_all_client_side_menu_on_employee_section() {
        cy.xpath("//input[@id='client_user_dashboard_show_all']/..//span[@class='fa  fa-check-square-o checked ']")
            .then($button => {
                if ($button.is(':visible')) {
                    cy.log('the all client side menu is enabled')
                } else {
                    cy.xpath("//input[@id='client_user_dashboard_show_all']/..//span[@class='fa fa-square-o unchecked ']")
                        .click()
                }
            })
    }

    verify_and_enable_all_trainee_side_menu_on_employee_section() {
        cy.xpath("//input[@id='trainee_user_dashboard_show_all']/..//span[@class='fa  fa-check-square-o checked ']")
            .then($button => {
                if ($button.is(':visible')) {
                    cy.log('the all trainee side menu is enabled')
                } else {
                    cy.xpath("//input[@id='trainee_user_dashboard_show_all']/..//span[@class='fa fa-square-o unchecked ']")
                        .click()
                }
            })
    }

    verify_and_enable_all_contractor_side_menu_on_employee_section() {
        cy.xpath("//input[@id='contractor_user_dashboard_show_all']/..//span[@class='fa  fa-check-square-o checked ']")
            .then($button => {
                if ($button.is(':visible')) {
                    cy.log('the all contractor side menu is enabled')
                } else {
                    cy.xpath("//input[@id='contractor_user_dashboard_show_all']/..//span[@class='fa fa-square-o unchecked ']")
                        .click()
                }
            })
    }

    verify_and_enable_all_other_side_menu_on_employee_section() {
        cy.xpath("//input[@id='other_user_dashboard_show_all']/..//span[@class='fa  fa-check-square-o checked ']")
            .then($button => {
                if ($button.is(':visible')) {
                    cy.log('the all other side menu is enabled')
                } else {
                    cy.xpath("//input[@id='other_user_dashboard_show_all']/..//span[@class='fa fa-square-o unchecked ']")
                        .click()
                }
            })
    }


    click_save_button_on_customization_page() {
        cy.xpath("//button[@class='btn btn-primary' and text()='Save']")
            .click()

    }

    enable_all_user_type() {
        this.click_customization_tab()
        this.click_user_menu_tab_on_customization_page()
        this.verify_and_enable_employee_checkbox_on_user_type_section()
        this.verify_and_enable_client_checkbox_on_user_type_section()
        this.verify_and_enable_trainee_checkbox_on_user_type_section()
        this.verify_and_enable_contractor_checkbox_on_user_type_section()
        this.verify_and_enable_other_checkbox_on_user_type_section()
        this.click_save_button_on_customization_page()
    }

    enable_all_user_type_side_menu() {
        this.click_customization_tab()
        this.click_user_menu_tab_on_customization_page()
        this.verify_and_enable_all_employee_side_menu_on_employee_section()
        this.verify_and_enable_all_client_side_menu_on_employee_section()
        this.verify_and_enable_all_trainee_side_menu_on_employee_section()
        this.verify_and_enable_all_contractor_side_menu_on_employee_section()
        this.verify_and_enable_all_other_side_menu_on_employee_section()
        this.click_save_button_on_customization_page()
    }

//ON ADMINISTRATION TAB:

    click_administration_tab() {
        cy.xpath("//a[@href='/enterprise/auditlog/view' and text()='Administration']")
            .click()
            .log("successfully clicked the administration tab")

    }

    click_emails_and_logs_tab_on_administration_page() {
        cy.xpath("//a[@href='/email_sms_logs/view' and text()='Email & Sms log']")
            .click()
            .log("successfully clicked the emails and logs tab")
        cy.reload()
        cy.xpath("//a[@href='/email_sms_logs/view' and text()='Email & Sms log']")
            .click()
    }

    click_preview_message_button_on_emails_and_logs_tab(to, sub) {

        let user_mail_id = to["user_name"] + to["domain"]
        cy.xpath("//div[contains(text(),'" + user_mail_id + "')]/..//div[contains(text(),'" + sub["www_activate_user_subject"] + "')]/..//a[@class='message-preview-btn']", {timeout:500000})
            .click()
            .log("successfully clicked the preview message button")

    }

    click_preview_message_button_for_registration_link_on_emails_and_logs_tab(to, sub, randno) {
        let user_mail_id = to["user_name"] + to["domain"]
        cy.xpath("//div[contains(text(),'" + user_mail_id + "')]/..//div[contains(text(),'" + sub["mail_subject"] + randno["rand_no"] + "')]/..//a[@class='message-preview-btn']", {timeout: 50000})
            .should('be.visible')
            .click()
            .log("successfully clicked the preview message button")
    }

    click_preview_message_button_for_survey_test_for_contactuser_on_emails_and_logs_tab(to, sub, randno) {
        let contactuser_mail_id = to["contact_user_id"]
        cy.log('my contact user is', contactuser_mail_id)
        cy.xpath("//div[contains(text(),'" + contactuser_mail_id + "')]/..//div[contains(text(),'" + sub["mail_subject"] + randno["rand_no"] + "')]/..//a[@class='message-preview-btn']", {timeout: 50000})
            .should('be.visible')
            .click()
            .log("successfully clicked the preview message button")
    }

    click_preview_message_button_for_questionnaire_test_for_contactuser_on_emails_and_logs_tab(to, sub, randno) {
        let contactuser_mail_id = to["contact_user_id"]
        cy.log('my contact user is', contactuser_mail_id)
        cy.xpath("//div[contains(text(),'" + contactuser_mail_id + "')]/..//div[contains(text(),'" + sub["mail_subject"] + randno["rand_no"] + "')]/..//a[@class='message-preview-btn']", {timeout: 50000})
            .should('be.visible')
            .click()
            .log("successfully clicked the preview message button")
    }


    get_password_from_activation_mail_sub() {
        cy.xpath("//td[@class='container-padding content']//p[text()=' Password :  ']/strong")
            .then(function ($div) {
                let cc = $div.length
                cy.log('my whole lenght', cc)
                let password = $div.text()
                let remove_space_on_password = password.replace(/ /g, "")
                let count = remove_space_on_password.length
                cy.log('password length is--', count)
                // let remove_space_on_password = password.replace('kr', '')
                cy.log('we got the password is--', remove_space_on_password)
                Cypress.env('my_passwords', remove_space_on_password)
                cy.log(Cypress.env('my_passwords'))
            });
    }

    get_password_from_email_Sms_log() {
        cy.xpath("//td[@class='container-padding content']//p[text()=' Password :  ']/strong", {timeout:5000})
            .then(function ($div) {
                let pwd = $div.text()
                let remove_space_on_password = pwd.replace(/ /g, "")
                cy.log('my activated user password is:', remove_space_on_password)
                cy.readFile("cypress/fixtures/user_reg_link.json", (err, password) => {
                    if (err) {
                        return cy.log("error");
                    }
                }).then((pwd) => {
                    pwd.activated_user_password = remove_space_on_password
                    cy.writeFile("cypress/fixtures/user_reg_link.json", JSON.stringify(pwd))
                })
            })
    }

    get_registration_link_from_invited_user_mail_sub() {
        cy.xpath("//td//p//a[@href]")
            .invoke('attr', 'href')
            .then(attributeValue => {
                cy.wait(3000);
                cy.log('my register link is', attributeValue)
                // Cypress.env('my_user_registration_link', a)
                cy.readFile("cypress/fixtures/user_reg_link.json", (err, link) => {
                    if (err) {
                        return cy.log("error");
                    }
                }).then((link) => {
                    link.user_reg_link = attributeValue
                    cy.writeFile("cypress/fixtures/user_reg_link.json", JSON.stringify(link))
                })

            });
    }


    click_preview_message_button_for_cycle_for_contactuser_on_emails_and_logs_tab(to, sub, randno) {
        let contactuser_mail_id = to["contact_user_id"]
        cy.log('my contact user is', contactuser_mail_id)
        cy.xpath("//div[contains(text(),'" + contactuser_mail_id + "')]/..//div[contains(text(),'" + sub["mail_subject"] + randno["rand_no"] + "')]/..//a[@class='message-preview-btn']", {timeout: 50000})
            .should('be.visible')
            .click()
            .log("successfully clicked the preview message button")
    }

    reload_email_and_sms_page_until_cycle_event_log(to) {
        cy.get('body')
            .then(($file) => {
                const FilePanelIsVisible = $file.find(to["contact_user_id"])
                    .is(':visible');
                if (FilePanelIsVisible) {
                    this.click_preview_message_button_for_cycle_events_of_questionnaire_test_for_contactuser_on_emails_and_logs_tab()
                } else {
                    cy.reload()
                    this.reload_email_and_sms_page_until_cycle_event_log()
                }

            })
    }

    click_preview_message_button_for_cycle_events_of_questionnaire_test_for_contactuser_on_emails_and_logs_tab(to, sub) {

        let contactuser_mail_id = to["contact_user_id"]
        cy.log('my contact user is', contactuser_mail_id)
        cy.xpath("//div[contains(text(),'" + contactuser_mail_id + "" + to["domain"] + "')]/..//div[contains(text(),'" + sub["cycle_event_of_questionnaire"] + "')]/..//a[@class='message-preview-btn']", {timeout: 50000})
            .should('be.visible')
            .click()
            .log("successfully clicked the preview message button")
    }

    click_preview_message_button_for_cycle_events_of_survey_test_for_contactuser_on_emails_and_logs_tab(to, sub) {
        let contactuser_mail_id = to["contact_user_id"]
        cy.log('my contact user is', contactuser_mail_id)
        cy.xpath("//div[contains(text(),'" + contactuser_mail_id + "" + to["domain"] + "')]/..//div[contains(text(),'" + sub["cycle_event_of_survey"] + "')]/..//a[@class='message-preview-btn']", {timeout: 50000})
            .should('be.visible')
            .click()
            .log("successfully clicked the preview message button")
    }

    get_cycle_event_of_questionnaire_link_from_invited_user_mail_sub() {
        cy.xpath("//p//a[@href]")
            .invoke('attr', 'href')
            .then(attributeValue => {
                cy.wait(3000);
                cy.log('my register link is', attributeValue)
                // Cypress.env('my_user_registration_link', a)
                cy.readFile("cypress/fixtures/user_reg_link.json", (err, link) => {
                    if (err) {
                        return cy.log("error");
                    }
                }).then((link) => {
                    link.cycle_questionnaire = attributeValue
                    cy.writeFile("cypress/fixtures/user_reg_link.json", JSON.stringify(link))
                })

            });
        cy.xpath("//a[text()='Quit']").click()
    }

    get_cycle_event_of_survey_link_from_invited_user_mail_sub() {
        cy.xpath("//p//a[@href]")
            .invoke('attr', 'href')
            .then(attributeValue => {
                cy.wait(3000);
                cy.log('my register link is', attributeValue)
                // Cypress.env('my_user_registration_link', a)
                cy.readFile("cypress/fixtures/user_reg_link.json", (err, link) => {
                    if (err) {
                        return cy.log("error");
                    }
                }).then((link) => {
                    link.cycle_survey = attributeValue
                    cy.writeFile("cypress/fixtures/user_reg_link.json", JSON.stringify(link))
                })

            });
        cy.xpath("//a[text()='Quit']").click()

    }

    verify_cycle_message_on_emails_and_logs_tab(mail_sub) {
        let cycle_meg = mail_sub["contact_user_id"]
        cy.log('my contact user is', cycle_meg)
        // cy.xpath("//td//p")
        cy.get('body').then((body) => {
            if (body.find("div[class='body-text']").length > 0) {
                cy.get("div[class='body-text'] p")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text === mail_sub["contact-user_id"]) {
                            cy.log('the cycle message is matched')
                        } else {
                            assert.notExists(null, 'the cycle message is not matched')
                        }
                    });
            } else {
                assert.notExists(null, 'the cycle message is not matched')

            }
        });
    }


    log_out_admin() {
        // cy.xpath("(//a//i[@class='fa fa-sign-out'])[1]").click({force: true});
        cy.xpath("//div[@class='login-slide']")
            .should('be.hidden')
            .invoke('show')
        cy.xpath("//div[@class='login-slide']//li//a[text()='Log Out']")
            .click({force: true})
    }

    ClickDefferedJobs(){
        cy.xpath("//a[text()='Deferred jobs']").click()
    }

}


export default SettingPage;