import * as path from "path";

class BaseModulePage {
    enterQuestinnaireName(questionnaire_name) {
        cy.get('#questionnaire_name')
            .type(questionnaire_name)
            .log("successfully entered the questionnaire name")
    }


    enterQuestionCount(count) {
        cy.xpath("//input[@id='nb_questions']").type(count)
            .log("successfully entered the Question count")
    }

    saveButton() {
        cy.xpath("//button[@name='submit']").click()
    }

    clickVisibility(questionnaireName) {
        cy.xpath("//a[contains(text(),'" + questionnaireName + "')]/..//a[@title='Define visibility']").click()
    }

    MakeItPublic(questionnaireName) {
        cy.xpath("//div[div/span[contains(text(),'Public access')]]/../div/h2/a[contains(text(),'Make it public')]").click()
        cy.xpath("//input[@name='public_link']").type(questionnaireName)
        cy.xpath("//button[@id='remove-public']/../button[contains(text(),'Make Public')]").click()
    }

    MakeItVisible() {
        cy.xpath("//label[@for='can_view_all']/span[contains(@class,'fa fa-square-o')]").click()
        this.saveButton()
    }

    clickTestSettings(questionnaireName) {
        cy.xpath("//a[contains(text(),'" + questionnaireName + "')]/..//a[@title='Define the test settings']").click()
    }

    MakeRenewal() {
        cy.get("#renewal_period").type("10").log("Renewal Period entered")
        cy.xpath("(//button[@type='submit'])[1]").click()
    }

    MakeForm(name) {
        cy.get("#survey_form_select-button").click()
        cy.xpath("//ul[@aria-hidden='false']/li[@class='ui-menu-item']/div[text()='"+name+"']").click({ force: true })
        cy.xpath("(//button[@type='submit'])[1]").click()
    }

    MakeConsequence() {
        cy.get("#messages_consequence_translated").scrollIntoView().type('if global_score >= 60 then message("Congratulations [first_name] [last_name]")')
        cy.xpath("(//button[@type='submit'])[1]").click()
    }

    enter_survey_name(surveyName) {
        cy.xpath("//div[@class='bloc-body']//div//div//label[normalize-space(text()='Name of the form')]//..//input[@id='form_name']")
            .type(surveyName).log("Successfully entered the survey name at survey creation page");
    }

    clickDuplicate(base) {
        cy.xpath("//a[contains(text(),'" + base["base_name"] + "')]/..//li[contains(@class,'duplicate')]/a").click()
        cy.xpath("//span[text()='Yes']").click()
        cy.get("#clone-validate-btn").click()
    }

    clickCreatedDuplicate(base) {
        const d = new Date().toISOString().split('T')[0]
        cy.xpath("//a[@class='cell topic-name '][contains(text(),'" + base["base_name"] + " (copy) " + d + "')]").click()

    }

    clickCreatedTransferBase(base) {
        const d = new Date().toISOString().split('T')[0]
        cy.xpath("//a[@class='cell topic-name '][contains(text(),'" + base["base_name"] + " (transfer) " + d + "')]").click()

    }

    verifyDuplicatedQuestionnaireModes() {
        cy.wait(10000)
        cy.reload()

        if (cy.xpath("//a[contains(text(),'Questionnaire public')]/..//span[@title='Public']").should("not.exist")) {
            cy.log("Public mode is removed from questionnaire in duplicated Base")
        } else {
            cy.log("Public mode is not removed from questionnaire in duplicated Base")
        }

        if (cy.xpath("//a[contains(text(),'Questionnaire visibility')]/..//span[@title='Visible']").should("not.exist")) {
            cy.log("Visible mode is removed from questionnaire in duplicated Base")
        } else {
            cy.log("Visible mode is not removed from questionnaire in duplicated Base")
        }

        if (cy.xpath("//a[contains(text(),'Questionnaire form')]/..//span[@title='Form/Survey']").should("be.visible")) {
            cy.log("Form mode is not removed from questionnaire in duplicated Base")
        } else {
            cy.log("Form mode is removed from questionnaire in duplicated Base")
        }

        if (cy.xpath("//a[contains(text(),'Questionnaire consequence')]/..//span[@title='consequences']").should("be.visible")) {
            cy.log("consequences mode is not removed from questionnaire in duplicated Base")
        } else {
            cy.log("consequences mode is removed from questionnaire in duplicated Base")
        }

        if (cy.xpath("//a[contains(text(),'Questionnaire renewal')]/..//span[contains(@title,'Renewal period')]").should("be.visible")) {
            cy.log("Renewal mode is not removed from questionnaire in duplicated Base")
        } else {
            cy.log("Renewal mode is removed from questionnaire in duplicated Base")
        }

    }

    openCreatedSurvey(surveydata) {
        cy.xpath("//span[text()='" + surveydata + "']").click()
            .log("Successfully clicked the created survey")
    }

    clickOverviewTab() {
        cy.xpath("//ul[contains(@class,'compact')]/li/a[text()='Overview']").click()
    }

    getQuestionCount() {
        let txt
        cy.wait(3000)
        cy.xpath("//label[text()='Questions']/../div").then($el => {
            txt = $el[0].innerText
            cy.writeFile('cypress/fixtures/questionData.json', '{"questionCount":' + txt + '}')
        })

    }

    getQuestionnaireCount() {
        let txt
        cy.wait(3000)
        cy.xpath("//label[text()='Questionnaires']/../div").then($el => {
            // cy.log($el[0].innerText)
            txt = $el[0].innerText
            cy.writeFile('cypress/fixtures/questionnaireData.json', '{"questionnaireCount":' + txt + '}')
        })
    }

    clickEditIconOfQuestion(count) {
        cy.xpath("(//a[@class='fa fa-pencil'])[" + count + "]").click()
    }

    //choose a status in Edit question page
    chooseStatus(status) {
        cy.get("#workflow_status-button").click()
        cy.xpath("//div[text()='" + status + "']").click()
    }

    //choose a status in Base - question tab
    changeStatusAndVerifyCopied(status) {
        cy.get("#status-filter-button").click()
        cy.xpath("//div[text()='" + status + "']").click()

        if (status === "Draft" || status === "Proposed" || status === "Reviewed" || status === "Inactive") {
            cy.get("h2").contains("There are no questions matching your filter settings.").then((ele) => {
                if (cy.get("h2").contains("There are no questions matching your filter settings.").should("be.visible")) {
                    cy.log("PASS:" + status + " type questions are not copied")
                } else {
                    cy.log("FAIL:" + status + " type questions are copied")
                }
            })
        }
        if (status === "Active") {
            let countOfElements = 0;
            cy.get(".flex-row.body").then($elements => {
                let countOfElements = $elements.length;
                if (countOfElements === 7) {
                    cy.log("PASS:" + status + " type questions are copied")
                } else {
                    cy.log("Fail:" + status + " type questions are not copied")
                }
            });
        }
    }

    changeStatusAndVerifyCopiedInTransfer(status){
        cy.get("#status-filter-button").click()
        cy.xpath("//div[text()='" + status + "']").click()

        if (status === "Draft" || status === "Proposed" || status === "Reviewed" || status === "Inactive") {
            cy.get('.question-div > .flex-row > .status').should("contain",status.toLowerCase())
        }
        if (status === "Active") {
            let countOfElements = 0;
            cy.get(".flex-row.body").then($elements => {
                let countOfElements = $elements.length;
                if (countOfElements === 7) {
                    cy.log("PASS:" + status + " type questions are copied")
                } else {
                    cy.log("Fail:" + status + " type questions are not copied")
                }
            });
        }
    }

    clickSaveAndQuit() {
        cy.xpath("//a[@name='save']").click()
    }

    ClickTransferButton() {
        cy.get("#topic-gcs-copy-btn").click()
    }

    EnterExportFilName(randomNumber) {
        cy.get("#file_name").type("Transfer" + randomNumber)
    }

    ClickBuildTransferButton() {
        cy.get("#topic-gcs-copy-modal .ajax-form>div.btn-toolbar>button.btn.btn-primary").click()
    }

    getTransferLink() {
        cy.get('.copy-url').then(($url) => {
            const link = $url.attr('data-url')
            cy.task('setCopiedLink', link)
        });
    }

    visitLinkToTransfer() {
        cy.task('getCopiedLink').then(($url) => {
            cy.visit($url)
        })
    }

    verifyBaseTransfered(){
        cy.xpath("//a[contains(text(),'development (transfer)')]").should("be.visible")
    }

    verifyTransferedQuestionnaireModes() {
        cy.wait(10000)
        cy.reload()

        if (cy.xpath("//a[contains(text(),'Questionnaire public')]/..//span[@title='Public']").should("not.exist")) {
            cy.log("Public mode is removed from questionnaire in Transfered Base")
        } else {
            cy.log("Public mode is not removed from questionnaire in Transfered Base")
        }

        if (cy.xpath("//a[contains(text(),'Questionnaire visibility')]/..//span[@title='Visible']").should("not.exist")) {
            cy.log("Visible mode is removed from questionnaire in Transfered Base")
        } else {
            cy.log("Visible mode is not removed from questionnaire in Transfered Base")
        }

        if (cy.xpath("//a[contains(text(),'Questionnaire form')]/..//span[@title='Form/Survey']").should("not.exist")) {
            cy.log("Form mode is  removed from questionnaire in Transfered Base")
        } else {
            cy.log("Form mode is not removed from questionnaire in Transfered Base")
        }

        if (cy.xpath("//a[contains(text(),'Questionnaire consequence')]/..//span[@title='consequences']").should("be.visible")) {
            cy.log("consequences mode is not removed from questionnaire in Transfered Base")
        } else {
            cy.log("consequences mode is removed from questionnaire in Transfered Base")
        }

        if (cy.xpath("//a[contains(text(),'Questionnaire renewal')]/..//span[contains(@title,'Renewal period')]").should("be.visible")) {
            cy.log("Renewal mode is not removed from questionnaire in Transfered Base")
        } else {
            cy.log("Renewal mode is removed from questionnaire in Transfered Base")
        }
    }

}

export default BaseModulePage;
