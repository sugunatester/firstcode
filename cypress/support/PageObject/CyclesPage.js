import moment from "moment";

class CyclesPage {
    //To click the cycles menu from the side menu
    click_cycles_side_menu() {
        try {
            cy.xpath("//a[@href='/cycles/list']//span[text()='Cycles']").click({force: true})
                .log("Successfully clicked the cycles menu from the side menu");
        } catch (error) {
            cy.log("Failed to click the cycles menu from the side menu");
        }
    }

    //To click the new cycle button
    click_new_cycle_button() {
        try {
            cy.xpath("//div//a[@id='new-cycle-btn']")
                .click().log("Successfully clicked the new cycle button");
        } catch (error) {
            cy.log("Failed to click the new cycle button");
        }
    }

    //To enter the cycle name
    enter_cycle_name(cycledata) {
        cy.xpath("//div[@class='bloc-body']//div//div//label[contains(text(),'Name')]//..//input[@id='cycle_name']")
            .type(cycledata["cycle_name"]).log("Successfully cycles name is entered");
    }

    //To enter the cycle description
    enter_description(cycledata) {
        let cycle_description = cycledata.cycle_1;
        try {
            cy.xpath("//div[@class='bloc-body']//div[@class='flex-row']//label[contains(text(),'Description')]//..//textarea[@id='description']")
                .type(cycle_description["description"])
                .log("Successfully cycles description is entered");
        } catch (error) {
            cy.log("Failed to enter the cycles description");
        }
    }

    //To click the save button at the cycle creation page
    click_cycle_save_button() {
        try {
            cy.xpath("//button[@type='submit'][@form='cycle-form']").click()
                .log("Successfully clicked the save button at cycle creation page");
        } catch (error) {
            cy.log("Failed to click the save button at cycle creation page");
        }
    }

    //To verify created cycle is displayed at cycle list page
    verify_created_cycle(cycledata) {
        cy.xpath("//div[@class='flex-row body']//a[@class='cell cycle-name ellipsis'][contains(text(),'" + cycledata["cycle_name"] + "')]")
            .should('be.visible').log("Successfully verified the cycle creation");
    }

    //To open created cycles
    open_created_cycles(cycledata) {
        cy.xpath("//div[@class='flex-row body']//a[@class='cell cycle-name ellipsis'][contains(text(),'" + cycledata["cycle_name"] + "')]")
            .click().log("Created cycles is opened successfully");
    }

    //To click steps tab at cycles
    click_steps_tab() {

        cy.xpath("//div[@class='tabs-wrapper']//ul//li//a[text()='Steps']")
            .click().log("Steps tab at cycles is successfully clicked");

    }

    //To filter the created base at steps page of cycles
    filter_created_base_on_steps_page(basedatas) {
        this.click_topic_filter_field()
        this.select_all_topic_option()
        this.click_topic_filter_field()
        this.select_base_on_topic_filter_field(basedatas)
    }

    filter_all_on_steps_page() {
        this.click_topic_filter_field()
        this.select_all_topic_option()
    }

    //To click the topic filter
    click_topic_filter_field() {
        cy.xpath("//*[@id='topic-filter-button']").click();

    }

    //To select the base on the topic filter
    select_base_on_topic_filter_field(basedatas) {
        cy.xpath("//ul[@id='topic-filter-menu']//div[text()='" + basedatas["base_name"] + "']").click();

    }

    //To select all options
    select_all_topic_option() {
        cy.xpath("//div[contains(@id,'ui-id')][contains(text(),'All topics')]").click();

    }


    //To add the questionnaire to the cycles
    select_questionnaire_to_cycle(steps) {
        cy.xpath("//span[@class='caption'][contains(text(),'" + steps["questionnaire_name"] + "')]//..//..//..//a[@class='item-select-btn fa fa-arrow-circle-o-right']")
            .click().log("Successfully clicked the 'add this questionnaire to cycle' button at the step page of cycles")
    }


    select_form_on_cycle(form) {
        cy.xpath("//div[@id='available-items']//span[contains(text(),'" + form["form_name"] + "')]/../../..//a[contains(@title,'Add this survey to the cycle')]")
            .click().log("Successfully clicked the 'add this form to cycle' button at the step page of cycles")
    }

    //To click the save button at the step page of the cycles
    click_save_button_cycles_step_page() {
        cy.xpath("//a[@id='save-btn'][contains(text(),'Save')]")
            .click().log("Successfully clicked the save button at the steps page of the cycles");
        this.reload();
    }

    //To refersh page
    reload() {
        cy.reload();
    }

    set_delay_as_persent_on_questionnaire(base, questionnaire) {
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//div/input[@name='delta_days']")
            .clear()
            .type('0')
    }

    set_delay_as_persent_on_form(form) {
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + form["form_name"] + "')]/../../..//div/input[@name='delta_days']")
            .clear()
            .type('0')
    }


    uncheck_all_days(base, questionnaire) {
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//div//ul/li[@title='Monday'][@class='checked']")
            .should('be.visible')
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//div//ul/li[@title='Monday'][@class='checked']")
            .click()
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//div//ul/li[@title='Tuesday'][@class='checked']")
            .should('be.visible')
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//div//ul/li[@title='Tuesday'][@class='checked']")
            .click()
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//div//ul/li[@title='Wednesday'][@class='checked']")
            .should('be.visible')
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//div//ul/li[@title='Wednesday'][@class='checked']")
            .click()
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//div//ul/li[@title='Thursday'][@class='checked']")
            .should('be.visible')
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//div//ul/li[@title='Thursday'][@class='checked']")
            .click()
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//div//ul/li[@title='Friday'][@class='checked']")
            .should('be.visible')
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//div//ul/li[@title='Friday'][@class='checked']")
            .click()
    }

    set_day_as_persent(base, questionnaire) {
        const weekday = ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"];
        const today_date = new Date()
        const day = weekday[today_date.getDay()]
        cy.log(day)
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//ul/li[text()='" + day + "']")
            .click()
    }

    click_invitation_tab_on_cycle_invitation_page() {
        cy.xpath("//a[text()='Invitations']")
            .click()
    }

    enable_yes_on_cycle_invitation_page() {
        cy.xpath("//span[text()='Yes']")
            .click()
    }

    enter_subject_on_cycle_invitation_page(mail_sub, rand) {
        cy.xpath("//input[@id='email_subject']")
            .clear()
            .type(mail_sub["mail_subject"] + rand["rand_no"])
            .log("Successfully entered the mail subject + static random number")
    }

    enter_message_on_cycle_invitation_page(mail_sub) {
        cy.xpath("//textarea[@id='email_message']")
            .clear()
            .type(mail_sub["mail_message"])
            .log("Successfully entered the mail message")
    }

    click_emails_and_mobile_button() {
        cy.get('#select-contacts-btn').click();
    }

    enter_contact_user_mail_id(emails) {
        cy.get('#contacts-participants')
            .type(emails["contact_user_id"] + emails["domain"], {waitForAnimations: false})
            .log("successfully entered the contact user mail address")
    }

    click_save_button_on_emails_and_mobile_window() {
        cy.get('#save-contacts').click();

    }

    click_invite_button_on_cycle_invitation_page() {
        cy.xpath("//button[@id='invite']").click();
    }

    click_users_tab_on_cycle() {
        cy.xpath("//a[text()='Users']").click()
    }

    click_status_filter_field() {
        cy.xpath("//span[@id='status-filter-button']").click()
    }

    filter_ongoing_status_on_cycle_user_page() {
        this.click_status_filter_field()
        cy.xpath("//ul[@id='status-filter-menu']//div[contains(text(),'Ongoing')]").click()
    }

    filter_all_status_on_cycle_user_page() {
        this.click_status_filter_field()
        cy.xpath("//ul[@id='status-filter-menu']//div[contains(text(),'All')]").click()
    }

    filter_canceled_status_on_cycle_user_page() {
        cy.wait(5000)
        this.click_status_filter_field()
        cy.xpath("//ul[@id='status-filter-menu']//div[contains(text(),'Cancelled')]").click()
    }

    verify_contact_user_played_cycle_test(contact) {
        cy.xpath("//div[text()='[" + contact["contact_user_id"] + "]']/..//div[text()='completed']")
            .should('be.exist')
    }

    verify_active_user_played_cycle_result(user) {
        cy.wrap(user).each(userdata => {
            cy.xpath("//div[text()='" + userdata["last_name"] + " " + userdata["first_name"] + "']/..//div[text()='completed']")
                .should('be.exist')
        })
    }

    click_select_user_button_on_cycle_invitation_page() {
        cy.xpath("//*[@id='select-employees-btn']").click()
    }

    select_active_user(userdata) {
        cy.wrap(userdata).each(user => {
            cy.xpath("//span[@class='username'][contains(text(),'" + user["last_name"].toUpperCase() + " " + user["first_name"][0].toUpperCase() + user["first_name"].slice(1) + "')]/../../div/a[@class='to-right-btn']")
                .click()
        })
    }

    click_validate_button_on_select_user_popup() {
        cy.xpath("//*[@id='select-multi-users-modal']//a[text()=' Validate']").click()
    }


    click_checkbox_on_ongoing_cycle() {
        cy.xpath("//div[@class='cell check']//span[@class='fa fa-square-o unchecked  ']").click()
    }

    click_delete_button_on_cycle_user_page() {
        cy.xpath("//a[@title='Delete all checked']").click()
    }

    click_delete_button_on_confirmation_cycle_user_page() {
        cy.xpath("//a[@id='confirm-delete-btn']").click()
    }

    verify_deleted_ongoing_cycle_is_exist_on_canceled_status(user) {
        cy.wrap(user).each(userdata => {
            cy.xpath("//div[text()='" + userdata["first_name"][0].toUpperCase() + userdata["first_name"].slice(1) + " " + userdata["last_name"][0].toUpperCase() + userdata["last_name"].slice(1) + "']//following::div[text()='cancelled']")
                .should('be.exist')
        })
    }
}


export default CyclesPage