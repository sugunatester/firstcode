class SurveysPage {
//To click the surveys menu from side menu
    click_surveys_side_menu() {
        cy.xpath("//a[@href='/surveys']//span[text()='Surveys']", {timeout: 50000})
            .click()
            .log("Successfully clicked the surveys menu from side menu");

    }

    //To click the new form button at survey page
    click_new_form_button() {

        cy.xpath("//a[@id='new-survey-btn'][normalize-space(text()='New Form')]")
            .click().log("Successfully clicked the 'new form' button at surveys page")

    }

    //To enter survey name at survey creation page
    enter_survey_name(surveydata) {
        let form_name = surveydata.form_name;

        cy.xpath("//div[@class='bloc-body']//div//div//label[normalize-space(text()='Name of the form')]//..//input[@id='form_name']")
            .type(form_name).log("Successfully entered the survey name at survey creation page");

    }

    //To click the save button at the survey creation page
    click_save_button_at_survey_creation_page() {
        cy.xpath("//button[@type='submit'][@name='submit']").click()
            .log("Successfully clicked the save button at survey creation page");
        cy.reload()
    }

    //To open the created survey
    open_created_survey(surveydata) {

        cy.xpath("//span[text()='" + surveydata["form_name"] + "']")
            .click()
            .log("Successfully clicked the created survey");

    }

    //To click the edit button at the surveys overview page
    click_edit_button() {

        cy.xpath("//div[@id='std-toolbar']//a[@title='Edit survey form']")
            .click()
            .log("Successfully clicked the edit button at survey overview page");

    }

    create_all_fields_on_created_surveys(surveydata) {
        cy.wrap(surveydata["fields"]).each(field => {
            cy.log('my each field', field.field_type)
            if (field["field_type"] === "text" || field["field_type"] === "para" || field["field_type"] === "date" || field["field_type"] === "number" || field["field_type"] === "selector" || field["field_type"] === "bool" || field["field_type"] === "multiple_select" || field["field_type"] === "drop_down" || field["field_type"] === "label" || field["field_type"] === "section_separator") {
                this.move_field(field["field_type"])
                this.edit_field(field["field_type"])
            }
            if (field["field_type"] === "text" || field["field_type"] === "para" || field["field_type"] === "date" || field["field_type"] === "number" || field["field_type"] === "selector" || field["field_type"] === "bool" || field["field_type"] === "multiple_select" || field["field_type"] === "drop_down") {
                this.common_fields(field)
            }
            if (field["field_type"] === "label") {
                this.label_field(field)
            }
            if (field["field_type"] === "section_separator") {
                this.section_separator_fields(field)
            }
            if (field["field_type"] === "media") {
                this.move_media_fields(field["field_type"])
                this.upload_media(field)
            }
            if (field["field_type"] === "text" || field["field_type"] === "para") {
                this.enter_minimum_length(field)
                this.enter_maximum_length(field)
            }
            if (field["field_type"] === "selector" || field["field_type"] === "bool" || field["field_type"] === "multiple_select" || field["field_type"] === "drop_down") {
                this.enter_choices(field)
            }
            if (field["field_type"] !== "media") {
                this.click_save_field()
            }
        })

    }

    move_field(field) {
        if (field === 'text') {
            this.move_text_field()
        }
        if (field === 'para') {
            this.move_text_area()
        }
        if (field === 'number') {
            this.move_number_field()
        }
        if (field === 'date') {
            this.move_date_field()
        }
        if (field === 'selector') {
            this.move_selector_field()
        }
        if (field === 'bool') {
            this.move_bool_field()
        }
        if (field === 'drop_down') {
            this.move_drop_down_field()
        }
        if (field === 'multiple_select') {
            this.move_multiple_select_field()
        }
        if (field === 'label') {
            this.move_label_field()
        }
        if (field === 'section_separator') {
            this.move_section_separator_field()
        }
    }

    edit_field(field) {
        if (field === 'text') {
            this.edit_text_field()
        }
        if (field === 'para') {
            this.edit_text_area()
        }
        if (field === 'number') {
            this.edit_number_field()
        }
        if (field === 'date') {
            this.edit_date_field()
        }
        if (field === 'selector') {
            this.edit_selector_field()
        }
        if (field === 'bool') {
            this.edit_bool_field()
        }
        if (field === 'drop_down') {
            this.edit_drop_down_field()
        }
        if (field === 'multiple_select') {
            this.edit_multiple_select_field()
        }
        if (field === 'label') {
            this.edit_label_field()
        }
        if (field === 'section_separator') {
            this.edit_section_separator_field()
        }
    }

    move_text_field() {
        cy.xpath("(//div[contains(@class,'field-type-text')]//a[contains(@title,'form')])[1]")
            .click()
            .log("successfully moved the text field")
    }

    move_text_area() {
        cy.xpath("(//div[contains(@class,'field-type-textarea')]//a[contains(@title,'form')])[1]")
            .click()
            .log("successfully moved the text area field")
    }

    move_date_field() {
        cy.xpath("(//div[contains(@class,'field-type-date')]//a[contains(@title,'form')])[1]")
            .click()
            .log("successfully moved the date field")
    }

    move_number_field() {
        cy.xpath("(//div[contains(@class,'field-type-number')]//a[contains(@title,'form')])[1]")
            .click()
            .log("successfully moved the number field")
    }

    move_selector_field() {
        cy.xpath("(//div[contains(@class,'field-type-select')]//a[contains(@title,'form')])[1]")
            .click()
            .log("successfully moved the selector field")
    }

    move_bool_field() {
        cy.xpath("(//div[contains(@class,'field-type-bool')]//a[contains(@title,'form')])[1]")
            .click()
            .log("successfully moved the bool field")
    }

    move_drop_down_field() {
        cy.xpath("(//div[contains(@class,'field-type-dropdown')]//a[contains(@title,'form')])[1]")
            .click()
            .log("successfully moved the drop-down field")
    }

    move_multiple_select_field() {
        cy.xpath("(//div[contains(@class,'field-type-multiselect')]//a[contains(@title,'form')])[1]")
            .click()
            .log("successfully moved the multiple select field")
    }

    move_section_separator_field() {
        cy.xpath("(//div[contains(@class,'field-type-section')]//a[contains(@title,'form')])[1]")
            .click()
            .log("successfully moved the section separator field")
    }

    move_label_field() {
        cy.xpath("(//div[contains(@class,'field-type-label')]//a[contains(@title,'form')])[1]")
            .click()
            .log("successfully moved the label field")
    }

    edit_text_field() {
        let element = "(//div[contains(@class,'field-type-text')]//a[contains(@title,'Edit settings')])"
        cy.xpath("(//div[contains(@class,'field-type-text')]//a[contains(@title,'Edit settings')])")
            .each((el, index, array) => {
                let xpath_index_count = array.length
                cy.log(array.length)
                cy.log(index)
                cy.wrap(xpath_index_count).as('MyIndexCount')

            })
        cy.get('@MyIndexCount').then(count => {
            cy.xpath(element + "[" + count + "]")
                .click()
                .log("successfully edited the text field")
        })


    }

    edit_text_area() {
        let element = "(//div[contains(@class,'field-type-textarea')]//a[contains(@title,'Edit settings')])"
        cy.xpath("(//div[contains(@class,'field-type-textarea')]//a[contains(@title,'Edit settings')])")
            .each((el, index, array) => {
                let xpath_index_count = array.length
                cy.log(array.length)
                cy.log(index)
                cy.wrap(xpath_index_count).as('MyIndexCount')
            })
        cy.get('@MyIndexCount').then(count => {
            cy.xpath(element + "[" + count + "]")
                .click()
                .log("successfully edited the text field")
        })

    }

    edit_date_field() {
        let element = "(//div[contains(@class,'field-type-date')]//a[contains(@title,'Edit settings')])"
        cy.xpath("(//div[contains(@class,'field-type-date')]//a[contains(@title,'Edit settings')])")
            .each((el, index, array) => {
                let xpath_index_count = array.length
                cy.log(array.length)
                cy.log(index)
                cy.wrap(xpath_index_count).as('MyIndexCount')

            })
        cy.get('@MyIndexCount').then(count => {
            cy.xpath(element + "[" + count + "]")
                .click()
                .log("successfully edited the text field")
        })

    }

    edit_number_field() {
        let element = "(//div[contains(@class,'field-type-number')]//a[contains(@title,'Edit settings')])"
        cy.xpath("(//div[contains(@class,'field-type-number')]//a[contains(@title,'Edit settings')])")
            .each((el, index, array) => {
                let xpath_index_count = array.length
                cy.log(array.length)
                cy.wrap(xpath_index_count).as('MyIndexCount')

            })
        cy.get('@MyIndexCount').then(count => {
            cy.xpath(element + "[" + count + "]")
                .click()
                .log("successfully edited the text field")
        })

    }

    edit_selector_field() {
        let element = "(//div[contains(@class,'field-type-select')]//a[contains(@title,'Edit settings')])"
        cy.xpath("(//div[contains(@class,'field-type-select')]//a[contains(@title,'Edit settings')])")
            .each((el, index, array) => {
                let xpath_index_count = array.length
                cy.log(array.length)
                cy.log(index)
                cy.wrap(xpath_index_count).as('MyIndexCount')

            })
        cy.get('@MyIndexCount').then(count => {
            cy.xpath(element + "[" + count + "]")
                .click()
                .log("successfully edited the text field")
        })
    }

    edit_bool_field() {
        let element = "(//div[contains(@class,'field-type-bool')]//a[contains(@title,'Edit settings')])"
        cy.xpath("(//div[contains(@class,'field-type-bool')]//a[contains(@title,'Edit settings')])")
            .each((el, index, array) => {
                let xpath_index_count = array.length
                cy.log(array.length)
                cy.log(index)
                cy.wrap(xpath_index_count).as('MyIndexCount')

            })
        cy.get('@MyIndexCount').then(count => {
            cy.xpath(element + "[" + count + "]")
                .click()
                .log("successfully edited the text field")
        })
    }

    edit_drop_down_field() {
        let element = "(//div[contains(@class,'field-type-dropdown')]//a[contains(@title,'Edit settings')])"
        cy.xpath("(//div[contains(@class,'field-type-dropdown')]//a[contains(@title,'Edit settings')])")
            .each((el, index, array) => {
                let xpath_index_count = array.length
                cy.log(array.length)
                cy.log(index)
                cy.wrap(xpath_index_count).as('MyIndexCount')

            })
        cy.get('@MyIndexCount').then(count => {
            cy.xpath(element + "[" + count + "]")
                .click()
                .log("successfully edited the text field")
        })
    }

    edit_multiple_select_field() {
        let element = "(//div[contains(@class,'field-type-multiselect')]//a[contains(@title,'Edit settings')])"
        cy.xpath("(//div[contains(@class,'field-type-multiselect')]//a[contains(@title,'Edit settings')])")
            .each((el, index, array) => {
                let xpath_index_count = array.length
                cy.log(array.length)
                cy.log(index)
                cy.wrap(xpath_index_count).as('MyIndexCount')

            })
        cy.get('@MyIndexCount').then(count => {
            cy.xpath(element + "[" + count + "]")
                .click()
                .log("successfully edited the text field")
        })
    }

    edit_section_separator_field() {
        let element = "(//div[contains(@class,'field-type-section')]//a[contains(@title,'Edit settings')])"
        cy.xpath("(//div[contains(@class,'field-type-section')]//a[contains(@title,'Edit settings')])")
            .each((el, index, array) => {
                let xpath_index_count = array.length
                cy.log(array.length)
                cy.log(index)
                cy.wrap(xpath_index_count).as('MyIndexCount')

            })
        cy.get('@MyIndexCount').then(count => {
            cy.xpath(element + "[" + count + "]")
                .click()
                .log("successfully edited the text field")
        })
    }

    edit_label_field() {
        let element = "(//div[contains(@class,'field-type-label')]//a[contains(@title,'Edit settings')])"
        cy.xpath("(//div[contains(@class,'field-type-label')]//a[contains(@title,'Edit settings')])")
            .each((el, index, array) => {
                let xpath_index_count = array.length
                cy.log(array.length)
                cy.log(index)
                cy.wrap(xpath_index_count).as('MyIndexCount')

            })
        cy.get('@MyIndexCount').then(count => {
            cy.xpath(element + "[" + count + "]")
                .click()
                .log("successfully edited the text field")
        })

    }

    common_fields(field) {
        this.enter_field_names(field)
        this.enter_label_name(field)
        this.select_input_required(field)
        this.select_user_data(field)
    }

    enter_field_names(field) {
        cy.on('window:confirm', () => true);
        cy.xpath("//input[@id='field_name']", {timeout: 500000})
            .should('be.visible')
            .clear()
            .type(field["field_name"])
            .log("successfully entered the fields name")
    }

    enter_label_name(field) {
        cy.xpath("//input[@id='label']")
            .type(field["label_name"])
            .log("successfully entered the label name")
    }

    select_input_required(field) {
        cy.xpath("//input[@id='required']/../label/span[@class='checked']")
            .click()
            .log("successfully select_input_required")
    }

    select_user_data(field) {
        cy.xpath("//input[@id='user_data']/../label/span[@class='checked']")
            .click()
            .log("successfully select_user_data")
    }

    label_field(field) {
        this.enter_label_name(field)
    }

    section_separator_fields(field) {
        this.enter_field_names(field)
        this.enter_label_name(field)
    }

    move_media_fields(field) {
        if (field === 'media') {
            this.move_media_field()
        }
    }

    move_media_field() {
        cy.xpath("(//div[contains(@class,'field-type-media')]//a[contains(@title,'form')])[1]")
            .click()
            .log("successfully moved the media field")
    }

    upload_media(field) {
        this.upload_media_field(field)
    }

    upload_media_field(file_name) {
        cy.xpath("(//div[contains(@class,'field-type-media')]//div[@class='picto-wrapper'])[2]")
            .click()
        cy.xpath("//a[text()='Enterprise']", {timeout: 50000})
            .click()
        cy.xpath("//div[@class='filename']//span[text()='" + file_name["name"] + "']", {timeout: 50000})
            .click({force: true})
    }

    enter_minimum_length(length) {
        cy.xpath("//*[@id='min_length']")
            .type(length["minimum_length"])
    }

    enter_maximum_length(length) {
        cy.xpath("//*[@id='max_length']")
            .type(length["maximum_length"])
    }

    click_save_field() {
        cy.xpath("//*[@id='field-settings-done-btn']")
            .click()
    }

    enter_choices(field) {
        cy.wrap(field["choices"]).then(choices => {
            cy.log(choices)
            cy.xpath("//*[@id='choices']")
                .clear()
            cy.xpath("//*[@id='choices']")
                .type(choices.toString())
        })
    }

    //To verify the success message after saved surveys
    verify_success_message_after_survey_saved(surveydata) {
        let form_name = surveydata.form_name;
        cy.xpath("//div[@class='xq-flashes xq-m collapse']//ul//li[normalize-space(text()='  Survey form " + form_name + " was successfully created. You may now define the fields.')]")
            .should('be.visible');
    }

    click_trial_play_on_survey_list_page(surveydata) {
        cy.xpath("//*[contains(text(),'" + surveydata["form_name"] + "')]/../..//a[@title='Try survey form']")
            .click()
            .log("successfully click the trial play button on survey list page")
    }

    fill_form(surveydata) {
        cy.wrap(surveydata["fields"]).each(field => {
            if (field["field_type"] === "selector" || field["field_type"] === "bool") {
                this.select_choice(field)
            }
            if (field["field_type"] === "drop_down") {
                this.select_option_from_drop_down_field(field)
            }
            if (field["field_type"] === "multiple_select") {
                this.select_multiple_1(field)
                this.select_multiple_2(field)
                this.select_multiple_3(field)
            } else {
                this.enter_text_field(field)
            }
        })
        this.submit_form()
    }

    fill_form_with_condition(conditiondata) {
        cy.wrap(conditiondata["fields"]).each(field => {
            if (field["field_type"] === "text" || field["field_type"] === "para" || field["field_type"] === "date" || field["field_type"] === "number") {
                this.enter_text_field(field)
            }
            if (field["field_type"] === "selector" || field["field_type"] === "bool") {
                this.select_choice(field)
            }
            if (field["field_type"] === "drop_down") {
                this.select_option_from_drop_down_field(field)
            }
            if (field["field_type"] === "multiple_select") {
                this.select_multiple_1(field)
                this.select_multiple_2(field)
                this.select_multiple_3(field)
            }
        })
        this.submit_form()
    }

    select_choice(field) {
        if (field["answer_data"] !== "") {
            cy.xpath("//input[@name='" + field["field_name"] + "']/..//label[text()='" + field["answer_data"] + "']", {timeout: 5000})
                .click()
        }
    }

    select_option_from_drop_down_field(field) {
        cy.xpath("//span[@id='drop_down_1-button']")
            .click()
        cy.xpath("//div[text()='" + field["answer_data"] + "']")
            .click()
    }

    select_multiple_1(field) {
        cy.xpath("//label[contains(text(),'" + field["answer_data_1"] + "')]")
            .click()
    }

    select_multiple_2(field) {
        cy.xpath("//label[contains(text(),'" + field["answer_data_2"] + "')]")
            .click()
    }

    select_multiple_3(field) {
        cy.xpath("//label[contains(text(),'" + field["answer_data_3"] + "')]")
            .click()
    }

    enter_text_field(field) {
        if (field["field_type"] === "text" || field["field_type"] === "para" || field["field_type"] === "date" || field["field_type"] === "number") {
            cy.xpath("//*[@name='" + field["field_name"] + "']")
                .type(field["answer_data"])
            // cy.xpath("//body")
            cy.xpath("//div[@class='left-column']")
                .click()
        }
    }

    submit_form() {
        cy.xpath("//*[@name='submit']")
            .click()
    }

    create_fields_with_condition(conditiondata) {
        cy.wrap(conditiondata["fields"]).each(field => {
            cy.log('my each field name', field.field_type)
            if (field["field_type"] === "text" || field["field_type"] === "para" || field["field_type"] === "date" || field["field_type"] === "number" || field["field_type"] === "selector" || field["field_type"] === "bool" || field["field_type"] === "multiple_select" || field["field_type"] === "drop_down" || field["field_type"] === "label" || field["field_type"] === "section_separator") {
                this.move_field(field["field_type"])
                this.edit_field(field["field_type"])
            }
            if (field["field_type"] === "text" || field["field_type"] === "para" || field["field_type"] === "date" || field["field_type"] === "number" || field["field_type"] === "selector" || field["field_type"] === "bool" || field["field_type"] === "multiple_select" || field["field_type"] === "drop_down") {
                this.common_fields(field)
            }
            if (field["field_type"] === "label") {
                this.label_field(field)
            }
            if (field["field_type"] === "section_separator") {
                this.section_separator_fields(field)
            }
            if (field["field_type"] === "media") {
                this.move_media_fields(field["field_type"])
                this.upload_media(field)
            }
            if (field["field_type"] === "text" || field["field_type"] === "para") {
                this.enter_minimum_length(field)
                this.enter_maximum_length(field)
            }
            if (field["field_type"] === "selector" || field["field_type"] === "bool" || field["field_type"] === "multiple_select" || field["field_type"] === "drop_down") {
                this.enter_choices(field)
            }
            if (field["field_type"] !== "media") {
                this.click_save_field()
            }
            this.enter_condition_for_each_field(field)
        })

    }

    enter_condition_for_each_field(field) {
        if (field["field_type"] === "para") {
            this.edit_text_area()
            this.enter_condition_for_para(field)
            cy.log(field["condition_para"])
        }
        if (field["field_type"] === "date") {
            this.edit_date_field()
            this.enter_condition_for_date(field)
        }
        if (field["field_type"] === "number") {
            this.edit_number_field()
            this.enter_condition_for_number(field)
        }
        if (field["field_type"] === "selector") {
            this.edit_selector_field()
            this.enter_condition_for_selector(field)
        }
        if (field["field_type"] === "bool") {
            this.edit_bool_field()
            this.enter_condition_for_bool(field)
        }
        if (field["field_type"] === "drop_down") {
            this.edit_drop_down_field()
            this.enter_condition_for_drop_down(field)
        }
        if (field["field_type"] === "multiple_select") {
            this.edit_multiple_select_field()
            this.enter_condition_for_multiple(field)
        }
        if (field["field_type"] === "multiple_select" || field["field_type"] === "drop_down" || field["field_type"] === "bool" || field["field_type"] === "selector" || field["field_type"] === "number" || field["field_type"] === "date" || field["field_type"] === "para")
            this.click_save_field()
    }

    enter_condition_for_para(field) {
        cy.xpath("//input[@id='condition']")
            .type(field["condition_para"])
    }

    enter_condition_for_date(field) {
        cy.xpath("//input[@id='condition']")
            .type(field["condition_date"])
    }

    enter_condition_for_number(field) {
        cy.xpath("//input[@id='condition']")
            .type(field["condition_number"])
    }

    enter_condition_for_selector(field) {
        cy.xpath("//input[@id='condition']")
            .type(field["condition_selector"])
    }

    enter_condition_for_bool(field) {
        cy.xpath("//input[@id='condition']")
            .type(field["condition_bool"])
    }

    enter_condition_for_drop_down(field) {
        cy.xpath("//input[@id='condition']")
            .type(field["condition_drop_down"])
    }

    enter_condition_for_multiple(field) {
        cy.xpath("//input[@id='condition']")
            .type(field["condition_multiple"])
    }

    click_invitation_tab_on_survey() {
        cy.xpath("//a[text()='Invitations']")
            .click()
    }

    click_select_user_button_on_survey_invitation_page() {
        cy.xpath("//a[@id='select-employees-btn']")
            .click()
    }

    click_email_and_mobile_button_on_survey_invitation_page() {
        cy.xpath("//a[@id='select-contacts-btn']")
            .click()
    }

    select_activated_user_on_survey_invitation_page(userdata) {
        cy.wrap(userdata).each(user => {
            cy.xpath("//span[contains(text(),'" + user["last_name"].toUpperCase() + " " + user["first_name"] + "')]/../..//div[@class='cell btns']")
                .click()
        })
    }

    enter_user_mailid_on_email_and_mobile_popup(contactuser) {
        cy.xpath("//div[@id='select-contacts']//textarea[@id='contacts-participants']")
            .type(contactuser["contact_user_id"])
    }

    click_validate_buttton_on_survey_invitation_page() {
        cy.xpath("//a[@class='btn btn-primary save-btn']")
            .click()
    }

    click_save_button_on_email_and_mobile_popup() {
        cy.xpath("//a[@id='save-contacts']")
            .click()
    }

    click_invite_buttton_on_survey_invitation_page() {
        cy.xpath("//button[@name='invite-btn']")
            .click()
    }

    click_result_tab_on_survey() {
        cy.xpath("//a[text()='Results']")
            .click()
    }

    verify_status_on_played_survey_on_survey_result_page(userdata) {
        cy.wrap(userdata).each(user => {
            cy.xpath("//div[contains(text(),'" + user["last_name"].toUpperCase() + " " + user["first_name"] + "')]/..//div[contains(text(),'completed')]")
                .should('be.exist')
        })
    }

    verify_status_on_played_contact_user_on_survey_result_page(contactuser) {
        cy.xpath("//div[contains(text(),'[" + contactuser["contact_user_id"] + "]')]/..//div[contains(text(),'completed')]")
            .should('be.exist')
    }

    click_detail_button_on_survey_result_page(userdata) {
        cy.wrap(userdata).each(user => {
            cy.xpath("//div[contains(text(),'" + user["last_name"].toUpperCase() + " " + user["first_name"] + "')]/..//a[@title='Detail']")
                .click()
        })
    }

    click_detail_button_on_contact_user_of_survey_result_page(contactuser) {
        cy.xpath("//div[contains(text(),'[" + contactuser["contact_user_id"] + "]')]/..//a[@title='Detail']")
            .click()
    }

    verify_result_data_on_survey(surveydata, userdata) {
        cy.wrap(userdata).each(user => {
            cy.wrap(surveydata["fields"]).each(field => {
                cy.log(field.field_name)
                cy.log(field.answer_data)
                if (field["field_type"] === "text" || field["field_type"] === "para" || field["field_type"] === "date" || field["field_type"] === "number" || field["field_type"] === "selector" || field["field_type"] === "bool" || field["field_type"] === "drop_down") {
                    cy.xpath("//div[contains(text(),'" + user["last_name"].toUpperCase() + " " + user["first_name"] + "')]/../..//div[text()='" + field["field_name"] + "']/..//div[text()='" + field["answer_data"] + "']")
                        .should('be.exist')
                }
                if (field["field_type"] === "multiple_select") {
                    cy.xpath("//div[contains(text(),'" + user["last_name"].toUpperCase() + " " + user["first_name"] + "')]/../..//div[text()='" + field["field_name"] + "']/..//div[text()='" + field["answer_data_2"] + ", " + field["answer_data_1"] + ", " + field["answer_data_3"] + "']", {timeout: 50000})
                        .should('be.exist')
                }
            })
        })
    }


    verify_result_data_for_contactuser_on_survey(surveydata, contactuser) {
        cy.wrap(surveydata["fields"]).each(field => {
            cy.log(field.field_name)
            cy.log(field.answer_data)
            if (field["field_type"] === "text" || field["field_type"] === "para" || field["field_type"] === "date" || field["field_type"] === "number" || field["field_type"] === "selector" || field["field_type"] === "bool" || field["field_type"] === "drop_down") {
                cy.xpath("//div[contains(text(),'[" + contactuser["contact_user_id"] + "]')]/../..//div[text()='" + field["field_name"] + "']/..//div[text()='" + field["answer_data"] + "']")
                    .should('be.exist')
            }
            if (field["field_type"] === "multiple_select") {
                cy.xpath("//div[contains(text(),'[" + contactuser["contact_user_id"] + "]')]/../..//div[text()='" + field["field_name"] + "']/..//div[text()='" + field["answer_data_2"] + ", " + field["answer_data_1"] + ", " + field["answer_data_3"] + "']", {timeout: 50000})
                    .should('be.exist')
            }
        })

    }

    verify_contact_user_is_on_the_return_link_page_after_completed_form(link) {
        cy.wait(50000)
        cy.url().should('eq', link["redirect_url"])
        cy.title().should('eq', 'Google')
        cy.wait(5000)
        cy.go('back')
    }

}

export default SurveysPage