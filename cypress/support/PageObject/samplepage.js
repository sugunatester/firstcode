export default class Samplepage {
  visit(url) {
    return cy.visit(url);
  }

  click(selector, field) {
    try {
      return cy.get(selector).click().log("Pass");
    } catch (error) {
      return cy.log("Fail");
    }
  }
  type(selector, text, field) {
    try {
      return cy.get(selector).type(text).log("Pass");
    } catch (error) {
      return cy.log("Fail");
    }
  }
  getAttribute(selector, attributename, field) {
    let attr;
    try {
      return cy.get(selector).then(function (el) {
        attr = el.prop(attributename);
      });
    } catch (error) {}
  }
  selectByText(selector, text, field) {
    try {
      return cy.get(selector).select(text).log("Pass");
    } catch (error) {
      return cy.log("Fail");
    }
  }
  getText(selector, field) {
    let text;
    try {
      return cy.get(selector).then(function (el) {
        text = el.text();
      });
    } catch (error) {}
  }
  windowsalert(text){
   return cy.on("window:alert", str => {
        expect(str).equal(text);
      });
  }
  windowsAlertConfirm(text){
    return cy.on("window:confirm", str => {
         expect(str).equal(text);
       });
   }
}
