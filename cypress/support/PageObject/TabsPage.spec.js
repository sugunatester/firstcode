class TabsPageSpec {

    //To verify the home button at the home page
    home_button() {
        cy.get('body').then((body) => {
            if (body.find('#main-nav > li:nth-child(1) >a').length > 0) {
                cy.get('#main-nav > li:nth-child(1) >a')
                    .then(function ($value) {
                        const text = $value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Home")) {
                            assert.exists('Home', 'Home button is displayed')
                        } else {
                            assert.notExists(null, 'The text, "Home" is not displayed')
                        }
                    })
            } else {
                assert.notExists(null, 'Home button is not displayed')
            }
        })
    }

    //To verify the solution button at the home page
    solution_button() {
        cy.get('body').then((body) => {
            if (body.find('#main-nav > li:nth-child(2) >a').length > 0) {
                cy.get('#main-nav > li:nth-child(2) >a')
                    .then(function ($value) {
                        const text = $value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Solution")) {
                            assert.exists('Solution', 'Solution button is displayed')
                        } else {
                            assert.notExists(null, 'The text, "Solution" is not displayed')
                        }
                    })
            } else {
                assert.notExists(null, 'Solution button is not displayed')
            }
        })
    }

    //To verify the testimonals button at home page
    testimonials_button() {
        cy.get('body').then((body) => {
            if (body.find('#main-nav > li:nth-child(3) >a').length > 0) {
                cy.get('#main-nav > li:nth-child(3) >a')
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Testimonials")) {
                            assert.exists('Testimonials', 'Testimonials button is displayed')
                        } else {
                            assert.notExists(null, 'The text, "Testimonials" is not displayed')
                        }
                    })
            } else {
                assert.notExists(null, 'Testimonials button is not displayed')
            }
        })
    }

    //To verify the pricing button at home page
    pricing_button() {
        cy.get('body').then((body) => {
            if (body.find('#main-nav > li:nth-child(4) >a').length > 0) {
                cy.get('#main-nav > li:nth-child(4) >a')
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Pricing")){
                            assert.exists('Pricing', 'Pricing button is displayed')
                        } else {
                            assert.notExists(null, 'The text, "Pricing" is not displayed')
                        }
                    })
            } else {
                assert.notExists(null, 'Pricing button is not displayed')
            }
        })
    }

    //To verify contact button at home page
    contact_button() {
        cy.get('body').then((body) => {
            if (body.find('#main-nav > li:nth-child(5) >a').length > 0) {
                cy.get('#main-nav > li:nth-child(5) >a')
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Contact")) {
                            assert.exists('Contact', 'Contact button is displayed')
                        } else {
                            assert.notExists(null, 'The text, "Contact" is not displayed')
                        }
                    })
            } else {
                assert.notExists(null, 'Contact button is not displayed')
            }
        })
    }

    //To verify free trail button at home page
    free_trail_button() {
        cy.get('body').then((body) => {
            if (body.find('#main-nav > li:nth-child(6) >a').length > 0) {
                cy.get('#main-nav > li:nth-child(6) >a')
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Free trial")) {
                            assert.exists('Free trial', 'Free trial button is displayed')
                        } else {
                            assert.notExists(null, 'The text, "Free trial" is not displayed')
                        }
                    })
            } else {
                assert.notExists(null, 'Free trial button is not displayed')
            }
        })
    }

    //To verify Connection button at home page
    connection_button() {
        cy.get('body').then((body) => {
            if (body.find('#main-nav > li:nth-child(7) >a').length > 0) {
                cy.get('#main-nav > li:nth-child(7) >a')
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Connection")) {
                            assert.exists('Connection', 'Connection button is displayed')
                        } else {
                            assert.notExists(null, 'The text, "Connection" is not displayed')
                        }
                    })
            } else {
                assert.notExists(null, 'Connection button is not displayed')
            }
        })
    }

    //To verify Language button is displayed and hover is working at home page
    language_button() {
        cy.get('body').then((body) => {
            if (body.find('#main-nav > li:nth-child(8) >a').length > 0) {
                cy.get('#main-nav > li:nth-child(8) >a')
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("English")) {
                            assert.exists('English', 'Language button is displayed')
                            this.hover_language_button();
                        } else {
                            assert.notExists(null, 'The text, "Language" is not displayed')
                        }
                    })
            } else {
                assert.notExists(null, 'Language button is not displayed')
            }
        })
    }

    //To verify the items into the hovered list of language button
    hover_language_button() {
        cy.get('body').then((body) => {
            if (body.find('#main-nav > li:nth-child(8) >a:nth-child(2)').length > 0) {
                cy.get('#main-nav > li:nth-child(8) >a:nth-child(2)')
                    .should('be.hidden')
                    .invoke('show')
                cy.get('#main-nav > li:nth-child(8) >a:nth-child(2)')
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Select")) {
                            assert.exists('Select', 'Select text is displayed')
                            cy.get('#main-nav > li:nth-child(8) > ul >li').should((lis) => {
                                expect(lis, '3 items').to.have.length(3)
                                expect(lis.eq(0), 'first item').to.contain('English')
                                expect(lis.eq(1), 'second item').to.contain('Français')
                                expect(lis.eq(2), 'third item').to.contain('Español')
                            })
                        } else {
                            assert.exists(null, 'Select text is not displayed')
                        }
                    })
            } else {
                assert.notExists(null, 'Select text at language button is not displayed')
            }
        })
    }

    //To verify Free trial button at the bottom of the page is displayed
    // free_trail_button_at_bottom() {
    //     cy.get('body').then((body) => {
    //         if (body.find('#home .btn-holder >a[href="#register-tab"]').length > 0) {
    //             cy.get('#home .btn-holder >a[href="#register-tab"]')
    //                 .then(value => {
    //                     const text = value.text();
    //                     cy.log("Text inside the xpath ", text)
    //                     if (text.includes("Free trial")) {
    //                         assert.exists('Free trial', 'Free trial button at bottom of the page is displayed')
    //                     } else {
    //                         assert.notExists(null, 'The text, "Free trial" at bottom of the page is not displayed')
    //                     }
    //                 })
    //         } else {
    //             assert.notExists(null, 'Free trial button at bottom of the page is not displayed')
    //         }
    //     })
    // }

    //To click the dashboard menu
    click_dashboard_menu() {
        cy.xpath("//nav[@id='nav-main']//ul[@class='menu list-unstyled']//li//a//span[text()='Dashboard']")
            .click()
            .log("Dashboard option from side menu is clicked");
    }

    //To verify base count tab at dashboard page
    base_count_tab() {
        cy.get('body').then((body) => {
            if (body.find('#dashboard-page .briefs >a:nth-child(1)').length > 0) {
                cy.xpath("//div[@id='dashboard-page']//section[@class='briefs']//a[1]")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("base")) {
                            assert.exists('base', 'base tab at dashboard page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"base" is not displayed at base tab of dashboard page')
                        }
                    })
            } else {
                assert.notExists(null, 'base tab at dashboard page is not displayed')
            }
        })
    }

    //To verify questionnaire count tab at dashboard page
    questionnaire_count_tab() {
        cy.get('body').then((body) => {
            if (body.find('#dashboard-page .briefs >a:nth-child(2)').length > 0) {
                cy.xpath("//div[@id='dashboard-page']//section[@class='briefs']//a[2]")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("questionnaire")) {
                            assert.exists('questionnaire', 'questionnaire tab at dashboard page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"questionnaire" is not displayed at questionnaire tab of dashboard page')
                        }
                    })
            } else {
                assert.notExists(null, 'questionnaire tab at dashboard page is not displayed')
            }
        })
    }

    //To verify question count tab at dashboard page
    question_count_tab() {
        cy.get('body').then((body) => {
            if (body.find('#dashboard-page .briefs >a:nth-child(3)').length > 0) {
                cy.xpath("//div[@id='dashboard-page']//section[@class='briefs']//a[3]")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("question")) {
                            assert.exists('question', 'question tab at dashboard page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"question" is not displayed at question tab of dashboard page')
                        }
                    })
            } else {
                assert.notExists(null, 'question tab at dashboard page is not displayed')
            }
        })
    }

    //To verify user count tab at dashboard page
    user_count_tab() {
        cy.get('body').then((body) => {
            if (body.find('#dashboard-page .briefs >a:nth-child(5)').length > 0) {
                cy.xpath("//div[@id='dashboard-page']//section[@class='briefs']//a[4]")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("user")) {
                            assert.exists('user', 'user tab at dashboard page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"user" is not displayed at user tab of dashboard page')
                        }
                    })
            } else {
                assert.notExists(null, 'user tab at dashboard page is not displayed')
            }
        })
    }

    //To verify special user count tab at dashboard page
    special_user_count_tab() {
        cy.get('body').then((body) => {
            if (body.find('#dashboard-page .briefs >a:nth-child(6)').length > 0) {
                cy.xpath("//div[@id='dashboard-page']//section[@class='briefs']//a[5]")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("special user")) {
                            assert.exists('special user', 'special user tab at dashboard page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"special user" is not displayed at special user tab of dashboard page')
                        }
                    })
            } else {
                assert.notExists(null, 'special user tab at dashboard page is not displayed')
            }
        })
    }

    //To verify group count tab at dashboard page
    group_count_tab() {
        cy.get('body').then((body) => {
            if (body.find('#dashboard-page .briefs >a:nth-child(7)').length > 0) {
                cy.xpath("//div[@id='dashboard-page']//section[@class='briefs']//a[6]")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("group")) {
                            assert.exists('group', 'group tab at dashboard page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"group" is not displayed at group tab of dashboard page')
                        }
                    })
            } else {
                assert.notExists(null, 'group tab at dashboard page is not displayed')
            }
        })
    }

    //To verify the overview tab at base page
    base_overview_tab() {
        cy.get('body').then((body) => {
            if (body.find('#topic-view-page > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@id='topic-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Overview']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Overview")) {
                            assert.exists('Overview', 'Overview tab at base page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Overview" is not displayed at Overview tab of base page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at base page is not displayed')
            }
        })
    }

    //To verify the Questionnaires tab at base page
    base_questionnaires_tab() {
        cy.get('body').then((body) => {
            if (body.find('#topic-view-page > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@id='topic-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Questionnaires']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Questionnaires")) {
                            assert.exists('Questionnaires', 'Questionnaires tab at base page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Questionnaires" is not displayed at Questionnaires tab of base page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at base page is not displayed')
            }
        })
    }

    //To verify the Questions tab at base page
    base_questions_tab() {
        cy.get('body').then((body) => {
            if (body.find('#topic-view-page > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@id='topic-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Questions']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Questions")) {
                            assert.exists('Questions', 'Questions tab at base page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Questions" is not displayed at Questions tab of base page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at base page is not displayed')
            }
        })
    }

    //To verify the Results tab at base page
    base_results_tab() {
        cy.get('body').then((body) => {
            if (body.find('#topic-view-page > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@id='topic-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Results']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Results")) {
                            assert.exists('Results', 'Results tab at base page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Results" is not displayed at Results tab of base page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at base page is not displayed')
            }
        })
    }

    //To verify the Trainings tab at base page
    base_trainings_tab() {
        cy.get('body').then((body) => {
            if (body.find('#topic-view-page > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@id='topic-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Trainings']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Trainings")) {
                            assert.exists('Trainings', 'Trainings tab at base page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Trainings" is not displayed at Trainings tab of base page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at base page is not displayed')
            }
        })
    }

    //To verify the Modules tab at base page
    base_modules_tab() {
        cy.get('body').then((body) => {
            if (body.find('#topic-view-page > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@id='topic-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Modules']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Modules")) {
                            assert.exists('Modules', 'Modules tab at base page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Modules" is not displayed at Modules tab of base page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at base page is not displayed')
            }
        })
    }

    //To verify the Medias tab at base page
    base_medias_tab() {
        cy.get('body').then((body) => {
            if (body.find('#topic-view-page > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@id='topic-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Medias']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Medias")) {
                            assert.exists('Medias', 'Medias tab at base page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Medias" is not displayed at Medias tab of base page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at base page is not displayed')
            }
        })
    }

    //To verify the Import & Export tab at base page
    base_import_export_tab() {
        cy.get('body').then((body) => {
            if (body.find('#topic-view-page > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@id='topic-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Import & Export']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Import & Export")) {
                            assert.exists('Import & Export', 'Import & Export tab at base page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Import & Export" is not displayed at Import & Export tab of base page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at base page is not displayed')
            }
        })
    }

    //To verify the Quality tab at base page
    base_quality_tab() {
        cy.get('body').then((body) => {
            if (body.find('#topic-view-page > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@id='topic-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Quality']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Quality")) {
                            assert.exists('Quality', 'Quality tab at base page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Quality" is not displayed at Quality tab of base page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at base page is not displayed')
            }
        })
    }

    //To click the results tab at the base page
    click_base_result_tab() {
        cy.xpath("//div[@id='topic-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Results']")
            .click().log("Successfully clicked the results tab at the base");
    }

    //To verify the per evaluation tab at the base page
    base_per_evaluation_tab() {
        cy.get('body').then((body) => {
            if (body.find('div.holder > div.tabs-wrapper >ul>li>a').length > 0) {
                cy.xpath("//div[@class='holder filters-and-buttons']//div[@class='tabs-wrapper']//ul//li//a[text()='Per evaluation']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Per evaluation")) {
                            assert.exists('Per evaluation', 'Per evaluation tab of results tab at the base page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Per evaluation" is not displayed at Per evaluation tab of results tab at base page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at results tab of base page is not displayed')
            }
        })
    }

    //To verify the per user tab at the base page
    base_per_user_tab() {
        cy.get('body').then((body) => {
            if (body.find('div.holder > div.tabs-wrapper >ul>li>a').length > 0) {
                cy.xpath("//div[@class='holder filters-and-buttons']//div[@class='tabs-wrapper']//ul//li//a[text()='Per user']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Per user")) {
                            assert.exists('Per user', 'Per user tab of results tab at the base page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Per user" is not displayed at Per user tab of results tab at base page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at results tab of base page is not displayed')
            }
        })
    }

    //To verify the free and public tests tab at the base page
    base_free_and_public_tests_tab() {
        cy.get('body').then((body) => {
            if (body.find('div.holder > div.tabs-wrapper >ul>li>a').length > 0) {
                cy.xpath("//div[@class='holder filters-and-buttons']//div[@class='tabs-wrapper']//ul//li//a[text()='Free and public tests']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Free and public tests")) {
                            assert.exists('Free and public tests', 'Free and public tests tab of results tab at the base page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Free and public tests" is not displayed at Free and public tests tab of results tab at base page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at results tab of base page is not displayed')
            }
        })
    }

    //To verify the overview tab at questionnaire
    questionnaire_overview_tab() {
        cy.get('body').then((body) => {
            if (body.find('#questionnaire-view-page > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@id='questionnaire-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Overview']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Overview")) {
                            assert.exists('Overview', 'Overview tab at questionnaire page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Overview" is not displayed at Overview tab of questionnaire page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at questionnaire page is not displayed')
            }
        })
    }

    //To verify the questions tab at questionnaire
    questionnaire_questions_tab() {
        cy.get('body').then((body) => {
            if (body.find('#questionnaire-view-page > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@id='questionnaire-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Questions']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Questions")) {
                            assert.exists('Questions', 'Questions tab at questionnaire page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Questions" is not displayed at Questions tab of questionnaire page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at questionnaire page is not displayed')
            }
        })
    }

    //To verify the selection tab at questionnaire
    questionnaire_selection_tab() {
        cy.get('body').then((body) => {
            if (body.find('#questionnaire-view-page > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@id='questionnaire-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Selection']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Selection")) {
                            assert.exists('Selection', 'Selection tab at questionnaire page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Selection" is not displayed at Selection tab of questionnaire page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at questionnaire page is not displayed')
            }
        })
    }

    //To verify the options tab at questionnaire
    questionnaire_options_tab() {
        cy.get('body').then((body) => {
            if (body.find('#questionnaire-view-page > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@id='questionnaire-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Options']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Options")) {
                            assert.exists('Options', 'Options tab at questionnaire page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Options" is not displayed at Options tab of questionnaire page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at questionnaire page is not displayed')
            }
        })
    }

    //To verify the invitation tab at questionnaire
    questionnaire_invitation_tab() {
        cy.get('body').then((body) => {
            if (body.find('#questionnaire-view-page > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@id='questionnaire-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Invitation']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Invitation")) {
                            assert.exists('Invitation', 'Invitation tab at questionnaire page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Invitation" is not displayed at Invitation tab of questionnaire page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at questionnaire page is not displayed')
            }
        })
    }

    //To verify the visibility tab at questionnaire
    questionnaire_visibility_tab() {
        cy.get('body').then((body) => {
            if (body.find('#questionnaire-view-page > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@id='questionnaire-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Visibility']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Visibility")) {
                            assert.exists('Visibility', 'Visibility tab at questionnaire page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Visibility" is not displayed at Visibility tab of questionnaire page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at questionnaire page is not displayed')
            }
        })
    }

    //To verify the result tab at questionnaire
    questionnaire_results_tab() {
        cy.get('body').then((body) => {
            if (body.find('#questionnaire-view-page > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@id='questionnaire-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Results']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Results")) {
                            assert.exists('Results', 'Result tab at questionnaire page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Result" is not displayed at Result tab of questionnaire page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at questionnaire page is not displayed')
            }
        })
    }

    //To click the results tab at the questionnaire page
    click_questionnaire_results_tab() {
        cy.xpath("//a[text()='Results']")
            .click()
            .log("Successfully clicked the results tab at the questionnaire page");
    }

    verify_tab_on_questionnaire_results_page() {
        this.click_questionnaire_results_tab()
        this.questionnaire_per_evaluation_tab();
        this.questionnaire_per_user_tab();
        this.questionnaire_free_and_public_tests_tab();
    }

    //To verify the per evaluation tab at the questionnaire page
    questionnaire_per_evaluation_tab() {
        cy.get('body').then((body) => {
            if (body.find('div.content-holder >ul>li>a').length > 0) {
                cy.xpath("//div[@class='content-holder']//ul//li//a[text()='Per evaluation']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Per evaluation")) {
                            assert.exists('Per evaluation', 'Per evaluation tab of results tab at the questionnaire page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Per evaluation" is not displayed at Per evaluation tab of results tab at questionnaire page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at results tab of questionnaire page is not displayed')
            }
        })
    }

    //To verify the per user tab at the questionnaire page
    questionnaire_per_user_tab() {
        cy.get('body').then((body) => {
            if (body.find('div.content-holder >ul>li>a').length > 0) {
                cy.xpath("//div[@class='content-holder']//ul//li//a[text()='Per user']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Per user")) {
                            assert.exists('Per user', 'Per user tab of results tab at the questionnaire page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Per user" is not displayed at Per user tab of results tab at questionnaire page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at results tab of questionnaire page is not displayed')
            }
        })
    }

    //To verify the free and public tests tab at the questionnaire page
    questionnaire_free_and_public_tests_tab() {
        cy.get('body').then((body) => {
            if (body.find('div.content-holder >ul>li>a').length > 0) {
                cy.xpath("//div[@class='content-holder']//ul//li//a[text()='Free and public tests']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Free and public tests")) {
                            assert.exists('Free and public tests', 'Free and public tests tab of results tab at the questionnaire page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Free and public tests" is not displayed at Free and public tests tab of results tab at questionnaire page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at results tab of questionnaire page is not displayed')
            }
        })
    }

    //To verify media tab at media page
    medias_tab() {
        cy.get('body').then((body) => {
            if (body.find('div.main-holder > div.tabs-wrapper > ul >li >a').length > 0) {
                cy.xpath("//div[@class='main-holder ui-front ']//div[@class='tabs-wrapper']//ul//li//a[text()='Medias']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Medias")) {
                            assert.exists('Medias', 'Medias tab at Medias page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Medias" is not displayed at Medias tab of Medias page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at Medias page is not displayed')
            }
        })
    }

    //To verify visibility tab at media page
    visibility_tab() {
        cy.get('body').then((body) => {
            if (body.find('div.main-holder > div.tabs-wrapper > ul >li >a').length > 0) {
                cy.xpath("//div[@class='main-holder ui-front ']//div[@class='tabs-wrapper']//ul//li//a[text()='Visibility']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Visibility")) {
                            assert.exists('Visibility', 'Visibility tab at Medias page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Visibility" is not displayed at Visibility tab of Medias page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at Medias page is not displayed')
            }
        })
    }

    //To verify users tab at users page
    users_users_tab() {
        cy.get('body').then((body) => {
            if (body.find('#users-list-page > div.tabs-wrapper >ul > li >a').length > 0) {
                cy.xpath("//div[@id='users-list-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Users']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Users")) {
                            assert.exists('', 'Users tab at Users page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Users" is not displayed at Users tab of Users page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at Users page is not displayed')
            }
        })
    }

    //To verify groups tab at users page
    users_groups_tab() {
        cy.get('body').then((body) => {
            if (body.find('#users-list-page > div.tabs-wrapper >ul > li >a').length > 0) {
                cy.xpath("//div[@id='users-list-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Groups']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Groups")) {
                            assert.exists('', 'Groups tab at Users page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Groups" is not displayed at Groups tab of Users page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at Users page is not displayed')
            }
        })
    }

    //To verify invite tab at users page
    users_invite_tab() {
        cy.get('body').then((body) => {
            if (body.find('#users-list-page > div.tabs-wrapper >ul > li >a').length > 0) {
                cy.xpath("//div[@id='users-list-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Invite']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Invite")) {
                            assert.exists('', 'Invite tab at Users page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Invite" is not displayed at Invite tab of Users page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at Users page is not displayed')
            }
        })
    }

    //To verify mailing tab at users page
    users_mailing_tab() {
        cy.get('body').then((body) => {
            if (body.find('#users-list-page > div.tabs-wrapper >ul > li >a').length > 0) {
                cy.xpath("//div[@id='users-list-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Mailing']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Mailing")) {
                            assert.exists('', 'Mailing tab at Users page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Mailing" is not displayed at Mailing tab of Users page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at Users page is not displayed')
            }
        })
    }

    //To verify contacts tab at users page
    users_contacts_tab() {
        cy.get('body').then((body) => {
            if (body.find('#users-list-page > div.tabs-wrapper >ul > li >a').length > 0) {
                cy.xpath("//div[@id='users-list-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Contacts']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Contacts")) {
                            assert.exists('', 'Contacts tab at Users page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Contacts" is not displayed at Contacts tab of Users page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at Users page is not displayed')
            }
        })
    }

    //To verify contacts tab at users page
    users_imports_exports_tab() {
        cy.get('body').then((body) => {
            if (body.find('#users-list-page > div.tabs-wrapper >ul > li >a').length > 0) {
                cy.xpath("//div[@id='users-list-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Import & Export']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Import & Export")) {
                            assert.exists('', 'Import & Export tab at Users page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Import & Export" is not displayed at Import & Export tab of Users page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at Users page is not displayed')
            }
        })
    }

    //To verify hierarchy tab at users page
    users_hierarchy_tab() {
        cy.get('body').then((body) => {
            if (body.find('#users-list-page > div.tabs-wrapper >ul > li >a').length > 0) {
                cy.xpath("//div[@id='users-list-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Hierarchy']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Hierarchy")) {
                            assert.exists('', 'Hierarchy tab at Users page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Hierarchy" is not displayed at Hierarchy tab of Users page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at Users page is not displayed')
            }
        })
    }

    //To verify renewals tab at users page
    users_renewals_tab() {
        cy.get('body').then((body) => {
            if (body.find('#users-list-page > div.tabs-wrapper >ul > li >a').length > 0) {
                cy.xpath("//div[@id='users-list-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Renewals']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Renewals")) {
                            assert.exists('', 'Renewals tab at Users page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Renewals" is not displayed at Renewals tab of Users page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at Users page is not displayed')
            }
        })
    }

    //To click the see activities button
    click_see_activities_button(admindatas) {
        let last_name = admindatas.last_name.toUpperCase();
        cy.xpath("//div[@class='cell name'][contains(text(),'" + last_name + " " + admindatas["first_name"] + "')]//..//div[@class='cell btns pop-btns']//ul//li//a[@title='See activities']")
            .click().log("Successfully clicked the see activities button");
    }

    //To verify the overview tab of see activities
    see_activities_overview_tab() {
        cy.get('body').then((body) => {
            if (body.find('#enterprise_user-view-page > div.tabs-wrapper >ul > li >a').length > 0) {
                cy.xpath("//div[@id='enterprise_user-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Overview']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Overview")) {
                            assert.exists('Overview', 'Overview tab of the see activities at Users page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Overview" is not displayed at Overview tab of the see activities at Users page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs of the see activities at Users page is not displayed')
            }
        })
    }

    //To verify the results tab of see activities
    see_activities_results_tab() {
        cy.get('body').then((body) => {
            if (body.find('#enterprise_user-view-page > div.tabs-wrapper >ul > li >a').length > 0) {
                cy.xpath("//div[@id='enterprise_user-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Results']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Results")) {
                            assert.exists('Results', 'Results tab of the see activities at Users page is displayed')
                            this.see_activities_click_result_tab();
                            this.see_activities_per_evaluation_tab();
                            this.see_activities_free_and_public_tests_tab();
                        } else {
                            assert.notExists(null, 'The text,"Results" is not displayed at Results tab of the see activities at Users page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs of the see activities at Users page is not displayed')
            }
        })
    }

    //To verify the trainings tab of see activities
    see_activities_trainings_tab() {
        cy.get('body').then((body) => {
            if (body.find('#user-evaluations-page > div.tabs-wrapper >ul > li >a').length > 0) {
                cy.xpath("//div[@id='user-evaluations-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Trainings']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Trainings")) {
                            assert.exists('Trainings', 'Trainings tab of the see activities at Users page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Trainings" is not displayed at Trainings tab of the see activities at Users page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs of the see activities at Users page is not displayed')
            }
        })
    }

    //To verify the modules tab of see activities
    see_activities_modules_tab() {
        cy.get('body').then((body) => {
            if (body.find('#user-evaluations-page > div.tabs-wrapper >ul > li >a').length > 0) {
                cy.xpath("//div[@id='user-evaluations-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Modules']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Modules")) {
                            assert.exists('Modules', 'Modules tab of the see activities at Users page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Modules" is not displayed at Modules tab of the see activities at Users page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs of the see activities at Users page is not displayed')
            }
        })
    }

    //To verify the paths tab of see activities
    see_activities_paths_tab() {
        cy.get('body').then((body) => {
            if (body.find('#user-evaluations-page > div.tabs-wrapper >ul > li >a').length > 0) {
                cy.xpath("//div[@id='user-evaluations-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Paths']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Paths")) {
                            assert.exists('Paths', 'Paths tab of the see activities at Users page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Paths" is not displayed at Paths tab of the see activities at Users page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs of the see activities at Users page is not displayed')
            }
        })
    }

    //To verify the skills tab of see activities
    see_activities_skills_tab() {
        cy.get('body').then((body) => {
            if (body.find('#user-evaluations-page > div.tabs-wrapper >ul > li >a').length > 0) {
                cy.xpath("//div[@id='user-evaluations-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Skills']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Skills")) {
                            assert.exists('Skills', 'Skills tab of the see activities at Users page is displayed')
                            this.click_see_activities_skills_tab();
                            this.see_activities_skills_tab_into_skills();
                            this.see_activities_qualifications_tab_into_skills();
                            this.see_activities_jobs_tab_into_skills();
                            this.see_activities_history_tab_into_skills();
                        } else {
                            assert.notExists(null, 'The text,"Skills" is not displayed at Skills tab of the see activities at Users page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs of the see activities at Users page is not displayed')
            }
        })
    }

    //To click skills tab into the see activities page
    click_see_activities_skills_tab() {
        cy.xpath("//div[@id='user-evaluations-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Skills']")
            .click().log("skills tab at see activities page is clicked");
    }


    //To verify the sub skills tab of skills tab at the see activities page
    see_activities_skills_tab_into_skills() {
        cy.get('body').then((body) => {
            if (body.find("div[class='tabs-wrapper'] ul[class='nav-tabs-list list-unstyled sub-nav-tabs'] li").length > 0) {
                cy.xpath("//div[@class='tabs-wrapper']//ul[@class='nav-tabs-list list-unstyled sub-nav-tabs']//li//a[text()='Skills']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Skills")) {
                            assert.exists('Skills', 'sub skills tab of skills tab at see activities page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Skills" is not displayed at sub skills tab of skills tab at see activities page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs of skills at see activities page is not displayed')
            }
        })
    }

    //To verify the Qualifications tab of skills tab at the see activities page
    see_activities_qualifications_tab_into_skills() {
        cy.get('body').then((body) => {
            if (body.find("div[class='tabs-wrapper'] ul[class='nav-tabs-list list-unstyled sub-nav-tabs'] li").length > 0) {
                cy.xpath("//div[@class='tabs-wrapper']//ul[@class='nav-tabs-list list-unstyled sub-nav-tabs']//li//a[text()='Qualifications']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Qualifications")) {
                            assert.exists('Qualifications', 'Qualifications tab of skills tab at see activities page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Qualifications" is not displayed at Qualifications tab of skills tab at see activities page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs of skills tab at see activities page is not displayed')
            }
        })
    }

    //To verify the jobs tab of skills tab at the see activities page
    see_activities_jobs_tab_into_skills() {
        cy.get('body').then((body) => {
            if (body.find("div[class='tabs-wrapper'] ul[class='nav-tabs-list list-unstyled sub-nav-tabs'] li").length > 0) {
                cy.xpath("//div[@class='tabs-wrapper']//ul[@class='nav-tabs-list list-unstyled sub-nav-tabs']//li//a[text()='Jobs']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Jobs")) {
                            assert.exists('Jobs', 'Jobs tab of skills tab at see activities page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Jobs" is not displayed at Jobs tab of skills tab at see activities page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs of skills tab at see activities page is not displayed')
            }
        })
    }


//To verify the history tab of skills tab at the see activities page
    see_activities_history_tab_into_skills() {
        cy.get('body').then((body) => {
            if (body.find("div[class='tabs-wrapper'] ul[class='nav-tabs-list list-unstyled sub-nav-tabs'] li").length > 0) {
                cy.xpath("//div[@class='tabs-wrapper']//ul[@class='nav-tabs-list list-unstyled sub-nav-tabs']//li//a[text()='History']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("History")) {
                            assert.exists('History', 'History tab of skills tab at see activities page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"History" is not displayed at History tab of skills tab at see activities page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs of skills tab at see activities page is not displayed')
            }
        })
    }


    //To verify the audit log tab of see activities
    see_activities_users_audit_log_tab() {
        cy.get('body').then((body) => {
            if (body.find("div[class='tabs-wrapper'] ul[class='nav-tabs-list list-unstyled compact'] li").length > 0) {
                cy.xpath("//div[@id='user-skills-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Audit log']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Audit log")) {
                            assert.exists('Audit log', 'Audit log tab of the see activities at Users page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Audit log" is not displayed at Audit log tab of the see activities at Users page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs of the see activities at Users page is not displayed')
            }
        })
    }

    //To click the result tab
    see_activities_click_result_tab() {
        cy.xpath("//div[@id='enterprise_user-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Results']")
            .click().log("Successfully clicked the result tab at users page");
    }

    //To verify the per evaluation tab of results tab at see activities
    see_activities_per_evaluation_tab() {
        cy.get('body').then((body) => {
            if (body.find('div.content-holder > ul >li >a').length > 0) {
                cy.xpath("//div[@class='content-holder']//ul//li//a[text()='Per evaluation']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Per evaluation")) {
                            assert.exists('Per evaluation', 'Per evaluation tab of results tab at see activities is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Per evaluation" is not displayed at Per evaluation tab of results tab at see activities')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs of results tab at see activities is not displayed')
            }
        })
    }

    //To verify the free and public tests tab of results tab at see activities
    see_activities_free_and_public_tests_tab() {
        cy.get('body').then((body) => {
            if (body.find('div.content-holder > ul >li >a').length > 0) {
                cy.xpath("//div[@class='content-holder']//ul//li//a[text()='Free and public tests']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Free and public tests")) {
                            assert.exists('Free and public tests', 'Free and public tests tab of results tab at see activities is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Free and public tests" is not displayed at Free and public tests tab of results tab at see activities')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs of results tab at see activities is not displayed')
            }
        })
    }

    //To click the results menu from the side menu
    click_results_menu() {
        cy.xpath("//a[@href='/enterprise/evaluations']//span[text()='Results']").click()
            .log("Successfully clicked the results menu from the side menu");
    }

    //To verify the per evaluation tab at the results page
    results_per_evaluation_tab() {
        cy.get('body').then((body) => {
            if (body.find('#list-of-evaluations-page>div.tabs-wrapper>ul>li>a').length > 0) {
                cy.xpath("//div[@id='list-of-evaluations-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Per evaluation']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Per evaluation")) {
                            assert.exists('Per evaluation', 'Per evaluation tab at the results page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Per evaluation" is not displayed at Per evaluation tab of results page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at results page is not displayed')
            }
        })
    }

    //To verify the per user tab at the results page
    results_per_user_tab() {
        cy.get('body').then((body) => {
            if (body.find('#list-of-evaluations-page>div.tabs-wrapper>ul>li>a').length > 0) {
                cy.xpath("//div[@id='list-of-evaluations-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Per user']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Per user")) {
                            assert.exists('Per user', 'Per user tab at results page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Per user" is not displayed at Per user tab of results page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at results page is not displayed')
            }
        })
    }

    //To verify the free and public tests tab at the results page
    results_free_and_public_tests_tab() {
        cy.get('body').then((body) => {
            if (body.find('#list-of-evaluations-page>div.tabs-wrapper>ul>li>a').length > 0) {
                cy.xpath("//div[@id='list-of-evaluations-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Free and public tests']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Free and public tests")) {
                            assert.exists('Free and public tests', 'Free and public tests tab at results page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Free and public tests" is not displayed at Free and public tests tab of results page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at results page is not displayed')
            }
        })
    }

    //To verify the overview tab at cycles page
    cycles_overview_tab() {
        cy.get('body').then((body) => {
            if (body.find('#cycle-view-page > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@id='cycle-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Overview']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Overview")) {
                            assert.exists('Overview', 'Overview tab at cycles page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Overview" is not displayed at Overview tab of cycles page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at cycles page is not displayed')
            }
        })
    }

    //To verify the steps tab at cycles page
    cycles_steps_tab() {
        cy.get('body').then((body) => {
            if (body.find('#cycle-view-page > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@id='cycle-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Steps']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Steps")) {
                            assert.exists('Steps', 'Steps tab at cycles page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Steps" is not displayed at Steps tab of cycles page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at cycles page is not displayed')
            }
        })
    }

    //To verify the invitations tab at cycles page
    cycles_invitations_tab() {
        cy.get('body').then((body) => {
            if (body.find('#cycle-view-page > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@id='cycle-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Invitations']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Invitations")) {
                            assert.exists('Invitations', 'Invitations tab at cycles page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Invitations" is not displayed at Invitations tab of cycles page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at cycles page is not displayed')
            }
        })
    }

    //To verify the users tab at cycles page
    cycles_users_tab() {
        cy.get('body').then((body) => {
            if (body.find('#cycle-view-page > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@id='cycle-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Users']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Users")) {
                            assert.exists('Users', 'Users tab at cycles page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Users" is not displayed at Users tab of cycles page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at cycles page is not displayed')
            }
        })
    }

    //To click the skills tab from the side menu
    click_skills_side_menu() {
        cy.xpath("//a[@href='/enterprise/skills/list']//span[text()='Skills']").click().log("Successfully clicked the skills menu from side menu");

    }

    //To verify the skills tab at the skills page
    skills_skills_tab() {
        cy.get('body').then((body) => {
            if (body.find('#skills-block-page > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@id='skills-block-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Skills']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Skills")) {
                            assert.exists('Skills', 'Skills tab at Skills page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Skills" is not displayed at Skills tab of Skills page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at Skills page is not displayed')
            }
        })
    }

    //To verify the qualifications tab at the skills page
    skills_qualifications_tab() {
        cy.get('body').then((body) => {
            if (body.find('#skills-block-page > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@id='skills-block-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Qualifications']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Qualifications")) {
                            assert.exists('Qualifications', 'Qualifications tab at Skills page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Qualifications" is not displayed at Qualifications tab of Skills page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at Skills page is not displayed')
            }
        })
    }

    //To verify the jobs tab at the skills page
    skills_jobs_tab() {
        cy.get('body').then((body) => {
            if (body.find('#skills-block-page > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@id='skills-block-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Jobs']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Jobs")) {
                            assert.exists('Jobs', 'Jobs tab at Skills page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Jobs" is not displayed at Jobs tab of Skills page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at Skills page is not displayed')
            }
        })
    }

    //To verify the overview tab at the skills page
    skills_overview_tab() {
        cy.get('body').then((body) => {
            if (body.find('.content-holder > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@class='content-holder skills-sub-tab']//div[@class='tabs-wrapper']//ul//li//a[text()='Overview ']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Overview")) {
                            assert.exists('Overview', 'Overview tab at Skills page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Overview" is not displayed at Overview tab of Skills page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at Skills page is not displayed')
            }
        })
    }


    //To verify the users tab at the skills page
    skills_users_tab() {
        cy.get('body').then((body) => {
            if (body.find('.content-holder > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@class='content-holder skills-sub-tab']//div[@class='tabs-wrapper']//ul//li//a[text()='Users']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Users")) {
                            assert.exists('Users', 'Users tab at Skills page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Users" is not displayed at Users tab of Skills page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at Skills page is not displayed')
            }
        })
    }

    //To verify the import tab at the skills page
    skills_import_tab() {
        cy.get('body').then((body) => {
            if (body.find('.content-holder > div.tabs-wrapper > ul > li > a').length > 0) {
                cy.xpath("//div[@class='content-holder skills-sub-tab']//div[@class='tabs-wrapper']//ul//li//a[text()='Import']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Import")) {
                            assert.exists('Import', 'Import tab at Skills page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Import" is not displayed at Import tab of Skills page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at Skills page is not displayed')
            }
        })
    }

    //To click overview tab at training
    click_overview_tab_at_training() {
        cy.xpath("//div[@class='tabs-wrapper']//ul//li//a[text()='Overview']").click().log("Successfully clicked overview tab at training");
    }

    //To click overview tab from result tab
    click_overview_tab_from_result_tab() {

        cy.xpath("//div[@id='training-results-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Overview']").click().log("Successfully clicked overview from result tab");

    }

    //To verify the overview tab at the training page
    training_overview_tab() {
        cy.get('body').then((body) => {
            if (body.find('div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@class='tabs-wrapper']//ul//li//a[text()='Overview']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Overview")) {
                            assert.exists('Overview', 'Overview tab at training page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Overview" is not displayed at Overview tab of training page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at training page is not displayed')
            }
        })
    }

    //To verify the questionnaires tab at the training page
    training_questionnaires_tab() {
        cy.get('body').then((body) => {
            if (body.find('div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@class='tabs-wrapper']//ul//li//a[text()='Questionnaires']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Questionnaires")) {
                            assert.exists('Questionnaires', 'Questionnaires tab at training page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Questionnaires" is not displayed at Questionnaires tab of training page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at training page is not displayed')
            }
        })
    }

    //To verify the medias tab at the training page
    training_medias_tab() {
        cy.get('body').then((body) => {
            if (body.find('div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@class='tabs-wrapper']//ul//li//a[text()='Medias']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Medias")) {
                            assert.exists('Medias', 'Medias tab at training page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Medias" is not displayed at Medias tab of training page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at training page is not displayed')
            }
        })
    }

    //To verify the invitations tab at the training page
    training_invitations_tab() {
        cy.get('body').then((body) => {
            if (body.find('div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@class='tabs-wrapper']//ul//li//a[text()='Invitations']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Invitations")) {
                            assert.exists('Invitations', 'Invitations tab at training page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Invitations" is not displayed at Invitations tab of training page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at training page is not displayed')
            }
        })
    }

    //To verify the participants tab at the training page
    training_participants_tab() {
        cy.get('body').then((body) => {
            if (body.find('div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@class='tabs-wrapper']//ul//li//a[text()='Participants']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Participants")) {
                            assert.exists('Participants', 'Participants tab at training page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Participants" is not displayed at Participants tab of training page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at training page is not displayed')
            }
        })
    }

    //To verify the chat tab at the training page
    training_chat_tab() {
        cy.get('body').then((body) => {
            if (body.find('div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@class='tabs-wrapper']//ul//li//a[text()='Chat']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Chat")) {
                            assert.exists('Chat', 'Chat tab at training page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Chat" is not displayed at Chat tab of training page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at training page is not displayed')
            }
        })
    }

    //To verify the black board tab at the training page
    training_black_board_tab() {
        cy.get('body').then((body) => {
            if (body.find('div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@class='tabs-wrapper']//ul//li//a[text()='Blackboard']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Blackboard")) {
                            assert.exists('Blackboard', 'Blackboard tab at training page is displayed')
                            this.click_black_board_tab_from_overview_tab()
                            this.training_brainstorming_tab();
                            this.training_test_question_tab();
                            this.training_survey_question_tab();
                        } else {
                            assert.notExists(null, 'The text,"Blackboard" is not displayed at Blackboard tab of training page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at training page is not displayed')
            }
        })

    }

    //To verify the results tab at the training page
    training_results_tab() {
        cy.get('body').then((body) => {
            if (body.find('div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@class='tabs-wrapper']//ul//li//a[text()='Results']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Results")) {
                            assert.exists('Results', 'Results tab at training page is displayed')
                            this.click_results_tab_from_black_board_tab();
                            this.training_results_evaluations_tab();
                            this.training_results_cycles_tab();
                            this.training_results_report_tab();
                            this.training_results_activities_tab();
                        } else {
                            assert.notExists(null, 'The text,"Results" is not displayed at Results tab of training page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at training page is not displayed')
            }
        })
    }

    //To verify the attendance sheet tab at the training page
    training_attendance_sheet_tab() {
        cy.get('body').then((body) => {
            if (body.find('div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@class='tabs-wrapper']//ul//li//a[text()='Attendance sheet']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Attendance sheet")) {
                            assert.exists('Attendance sheet', 'Attendance sheet tab at training page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Attendance sheet" is not displayed at Attendance sheet tab of training page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at training page is not displayed')
            }
        })
    }

    //To verify the surveys tab at the training page
    training_surveys_tab() {
        cy.get('body').then((body) => {
            if (body.find('div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@class='tabs-wrapper']//ul//li//a[text()='Surveys']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Surveys")) {
                            assert.exists('Surveys', 'Surveys tab at training page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Surveys" is not displayed at Surveys tab of training page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at training page is not displayed')
            }
        })
    }

    //To click black board tab at training page
    click_black_board_tab_from_overview_tab() {

        cy.xpath("//a[text()='Blackboard']")
            .click()
            .log("clicked the blackboard tab")

        cy.wait(50000)

        cy.get('body').then($activity => {
            const isVisible = $activity.find("div[class='bb-activity-mode create-mode'] h1")
                .is(':visible');
            if (isVisible) {
                // cy.xpath("div[class='bb-activity-mode create-mode'] h1")
                //     .then(function ($act){
                //         let get_text = $act.text()
                cy.log('My blackboard activity text is--')
                // })
            } else {
                this.click_black_board_tab_from_overview_tab()
            }
        });
    }


    //To verify 'brainstorming' tab at black board tab
    training_brainstorming_tab() {
        cy.get('body').then((body) => {
            if (body.find('#activity-create > ul>li>a').length > 0) {
                cy.xpath("//div[@id='activity-create']//ul//li//a[@title='Brainstorming']", {timeout: 20000})
                    .should('be.visible');
                cy.log('----')
                // assert.exists('The tab Brainstorming is displayed');
            } else {
                assert.notExists(null, 'Tabs at black board tab at training page is not displayed')
            }
        })
    }

    //To verify 'Test question' tab at black board tab
    training_test_question_tab() {
        cy.get('body').then((body) => {
            if (body.find('#activity-create > ul>li>a').length > 0) {
                cy.xpath("//div[@id='activity-create']//ul//li//a[@title='Test question']", {timeout: 20000})
                    .should('be.visible');
                // assert.exists('The tab Test question is displayed');
            } else {
                assert.notExists(null, 'Tabs at black board tab at training page is not displayed')
            }
        })
    }

    //To verify 'Survey question' tab at black board tab
    training_survey_question_tab() {
        cy.get('body').then((body) => {
            if (body.find('#activity-create > ul>li>a').length > 0) {
                cy.xpath("//div[@id='activity-create']//ul//li//a[@title='Survey question']")
                    .should('be.visible');
                // assert.exists('The tab Survey question is displayed');
            } else {
                assert.notExists(null, 'Tabs at black board tab at training page is not displayed')
            }
        })
    }

    //To click results tab at training page
    click_results_tab_from_black_board_tab() {

        cy.xpath("//div[@class='tabs-wrapper']//ul//li//a[text()='Results']")
            .click().log("Successfully clicked result tab at training");

    }

    //Verify evaluation tab of results at training page
    training_results_evaluations_tab() {
        cy.get('body').then((body) => {
            if (body.find('.tabs-wrapper> ul.nav-tabs-list >li>a').length > 0) {
                cy.xpath("//div[@class='tabs-wrapper']//ul//li//a[text()='Evaluations']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Evaluations")) {
                            assert.exists('Evaluations', 'Evaluations tab at training page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Evaluations" is not displayed at Evaluations tab of training page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at training page is not displayed')
            }
        })
    }

    //Verify cycles tab of results at training page
    training_results_cycles_tab() {
        cy.get('body').then((body) => {
            if (body.find('.tabs-wrapper> ul.nav-tabs-list >li>a').length > 0) {
                cy.xpath("//div[@class='tabs-wrapper']//ul//li//a[text()='Cycles']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Cycles")) {
                            assert.exists('Cycles', 'Cycles tab at training page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Cycles" is not displayed at Cycles tab of training page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at training page is not displayed')
            }
        })
    }

    //Verify report tab of results at training page
    training_results_report_tab() {
        cy.get('body').then((body) => {
            if (body.find('.tabs-wrapper> ul.nav-tabs-list >li>a').length > 0) {
                cy.xpath("//div[@class='tabs-wrapper']//ul//li//a[text()='Report']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Report")) {
                            assert.exists('Report', 'Report tab at training page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Report" is not displayed at Report tab of training page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at training page is not displayed')
            }
        })
    }

    //Verify activities tab of results at training page
    training_results_activities_tab() {
        cy.get('body').then((body) => {
            if (body.find('.tabs-wrapper> ul.nav-tabs-list >li>a').length > 0) {
                cy.xpath("//div[@class='tabs-wrapper']//ul//li//a[text()='Activities']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Activities")) {
                            assert.exists('Activities', 'Activities tab at training page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Activities" is not displayed at Activities tab of training page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at training page is not displayed')
            }
        })
    }


    //Click see as trainer button at the training page
    click_see_as_trainer_button() {

        cy.xpath("//div[@class='top-right']//a[@class='btn btn-default'][text()='See as trainer']").click()
            .log("Successfully clicked the 'see as trainer' button at training page");

    }

    //Click see as admin button at the training page
    click_see_as_admin_button() {

        cy.xpath("//div[@class='top-right']//a[@class='btn btn-default'][text()='See as admin']").click()
            .log("Successfully clicked the 'see as admin' button at training page");

    }

    //To verify the overview tab at the modules page
    module_overview_tab() {
        cy.get('body').then((body) => {
            if (body.find('#module-view-page > div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@id='module-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Overview']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Overview")) {
                            assert.exists('Overview', 'Overview tab at module page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Overview" is not displayed at Overview tab of module page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at module page is not displayed')
            }
        })
    }

    //To verify the elements tab at the modules page
    module_elements_tab() {
        cy.get('body').then((body) => {
            if (body.find('#module-view-page > div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@id='module-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Elements']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Elements")) {
                            assert.exists('Elements', 'Elements tab at module page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Elements" is not displayed at Elements tab of module page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at module page is not displayed')
            }
        })
    }

    //To verify the steps tab at the modules page
    module_steps_tab() {
        cy.get('body').then((body) => {
            if (body.find('#module-view-page > div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@id='module-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Steps']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Steps")) {
                            assert.exists('Steps', 'Steps tab at module page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Steps" is not displayed at Steps tab of module page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at module page is not displayed')
            }
        })
    }

    //To verify the invitation tab at the modules page
    module_invitation_tab() {
        cy.get('body').then((body) => {
            if (body.find('#module-view-page > div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@id='module-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Invitation']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Invitation")) {
                            assert.exists('Invitation', 'Invitation tab at module page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Invitation" is not displayed at Invitation tab of module page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at module page is not displayed')
            }
        })
    }

    //To verify the results tab at the modules page
    module_results_tab() {
        cy.get('body').then((body) => {
            if (body.find('#module-view-page > div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@id='module-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Results']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Results")) {
                            assert.exists('Results', 'Results tab at module page is displayed')
                            this.click_result_tab_into_module_page();
                            this.module_sessions_tab();
                            this.module_users_tab();
                        } else {
                            assert.notExists(null, 'The text,"Results" is not displayed at Results tab of module page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at module page is not displayed')
            }
        })
    }

    //To click result tab into the module page
    click_result_tab_into_module_page() {

        cy.xpath("//div[@id='module-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Results']")
            .click().log("Result tab at module page is clicked");

    }

    //To verify the Sessions tab of results tab at the modules page
    module_sessions_tab() {
        cy.get('body').then((body) => {
            if (body.find('#module-users-page > div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@id='module-users-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Sessions']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Sessions")) {
                            assert.exists('Sessions', 'Sessions tab of result tab at module page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Sessions" is not displayed at Sessions tab of result tab at module page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at module page is not displayed')
            }
        })
    }

    //To verify the users tab of results tab at the modules page
    module_users_tab() {
        cy.get('body').then((body) => {
            if (body.find('#module-users-page > div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@id='module-users-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Users']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Users")) {
                            assert.exists('Users', 'Users tab of result tab at module page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Users" is not displayed at Users tab of result tab at module page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at module page is not displayed')
            }
        })
    }

    //To verify the visibility tab at the modules page
    module_visibility_tab() {
        cy.get('body').then((body) => {
            if (body.find('#module-users-page > div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@id='module-users-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Visibility']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Visibility")) {
                            assert.exists('Visibility', 'Visibility tab at module page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Visibility" is not displayed at Visibility tab of module page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at module page is not displayed')
            }
        })
    }

    //To verify the overview tab at the path page
    path_overview_tab() {
        cy.get('body').then((body) => {
            if (body.find('.main-content > div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@class='main-content path-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Overview']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Overview")) {
                            assert.exists('Overview', 'Overview tab at path page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Overview" is not displayed at Overview tab of path page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at path page is not displayed')
            }
        })
    }

    //To verify the modules tab at the path page
    path_modules_tab() {
        cy.get('body').then((body) => {
            if (body.find('.main-content > div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@class='main-content path-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Modules']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Modules")) {
                            assert.exists('Modules', 'Modules tab at path page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Modules" is not displayed at Modules tab of path page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at path page is not displayed')
            }
        })
    }

    //To verify the invitation tab at the path page
    path_invitation_tab() {
        cy.get('body').then((body) => {
            if (body.find('.main-content > div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@class='main-content path-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Invitation']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Invitation")) {
                            assert.exists('Invitation', 'Invitation tab at path page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Invitation" is not displayed at Invitation tab of path page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at path page is not displayed')
            }
        })
    }

    //To verify the results tab at the path page
    path_results_tab() {
        cy.get('body').then((body) => {
            if (body.find('.main-content > div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@class='main-content path-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Results']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Results")) {
                            assert.exists('Results', 'Results tab at path page is displayed');
                            this.click_result_tab_into_path_page();
                            this.path_sessions_tab();
                            this.path_users_tab();
                        } else {
                            assert.notExists(null, 'The text,"Results" is not displayed at Results tab of path page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at path page is not displayed')
            }
        })
    }

    //To click result tab into the path page
    click_result_tab_into_path_page() {

        cy.xpath("//div[@class='main-content path-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Results']")
            .click().log("Result tab at path page is clicked");

    }

    //To verify the Sessions tab of results tab at the path page
    path_sessions_tab() {
        cy.get('body').then((body) => {
            if (body.find('.main-content > div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@class='main-content path-results-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Sessions']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Sessions")) {
                            assert.exists('Sessions', 'Sessions tab of result tab at path page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Sessions" is not displayed at Sessions tab of result tab at path page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at path page is not displayed')
            }
        })
    }

    //To verify the users tab of results tab at the path page
    path_users_tab() {
        cy.get('body').then((body) => {
            if (body.find('.main-content > div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@class='main-content path-results-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Users']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Users")) {
                            assert.exists('Users', 'Users tab of result tab at path page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Users" is not displayed at Users tab of result tab at path page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at path page is not displayed')
            }
        })
    }

    //To verify the overview tab at the survey page
    surveys_overview_tab() {
        cy.get('body').then((body) => {
            if (body.find('div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@class='tabs-wrapper']//ul//li//a[text()='Overview']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Overview")) {
                            assert.exists('Overview', 'Overview tab at surveys page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Overview" is not displayed at Overview tab of surveys page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at surveys page is not displayed')
            }
        })
    }

    //To verify the invitation tab at the survey page
    surveys_invitation_tab() {
        cy.get('body').then((body) => {
            if (body.find('div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@class='tabs-wrapper']//ul//li//a[text()='Invitations']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Invitations")) {
                            assert.exists('Invitations', 'Invitations tab at surveys page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Invitations" is not displayed at Invitations tab of surveys page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at surveys page is not displayed')
            }
        })
    }

    //To verify the results tab at the survey page
    surveys_results_tab() {
        cy.get('body').then((body) => {
            if (body.find('div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@class='tabs-wrapper']//ul//li//a[text()='Results']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Results")) {
                            assert.exists('Results', 'Results tab at surveys page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Results" is not displayed at Results tab of surveys page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at surveys page is not displayed')
            }
        })
    }

    //To verify the certifications tab at the certificates page
    certificates_certifications_tab() {
        cy.get('body').then((body) => {
            if (body.find('#list-of-evaluations-page >div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@id='list-of-evaluations-page']//div[@class='tabs-wrapper']//ul//li//a[contains(text(),'Certifications')]")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Certifications")) {
                            assert.exists('Certifications', 'Certifications tab at certificates page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Certifications" is not displayed at Certifications tab of certificates page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at certificates page is not displayed')
            }
        })
    }

    //To verify the modules tab at the certificates page
    certificates_modules_tab() {
        cy.get('body').then((body) => {
            if (body.find('#list-of-evaluations-page >div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@id='list-of-evaluations-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Modules']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Modules")) {
                            assert.exists('Modules', 'Modules tab at certificates page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Modules" is not displayed at Modules tab of certificates page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at certificates page is not displayed')
            }
        })
    }

    //To verify the enterprise tab at the settings page
    settings_enterprise_tab() {
        cy.get('body').then((body) => {
            if (body.find('#enterprise-view-page >div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@id='enterprise-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Enterprise']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Enterprise")) {
                            assert.exists('Enterprise', 'Enterprise tab at settings page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Enterprise" is not displayed at Enterprise tab of settings page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at settings page is not displayed')
            }
        })
    }

    //To verify the Settings tab at the settings page
    settings_settings_tab() {
        cy.get('body').then((body) => {
            if (body.find('#enterprise-view-page >div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@id='enterprise-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Settings ']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Settings ")) {
                            assert.exists('Settings ', 'Settings  tab at settings page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Settings " is not displayed at Settings  tab of settings page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at settings page is not displayed')
            }
        })
    }

    //To verify the Customization tab at the settings page
    settings_customization_tab() {
        cy.get('body').then((body) => {
            if (body.find('#enterprise-view-page >div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@id='enterprise-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Customization']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Customization")) {
                            assert.exists('Customization', 'Customization tab at settings page is displayed')
                            this.click_customization_tab_into_settings();
                            this.settings_user_menu_tab();
                            this.settings_player_tab();
                            this.settings_colors_tab();
                            this.settings_email_template_tab();
                            this.settings_certificate_tab();
                            this.settings_attendance_sheet_tab();
                            this.settings_glossary_tab();
                        } else {
                            assert.notExists(null, 'The text,"Customization" is not displayed at Customization tab of settings page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at settings page is not displayed')
            }
        })
    }

    //To click customization tab into the settings page
    click_customization_tab_into_settings() {

        cy.xpath("//div[@id='enterprise-view-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Customization']")
            .click().log("Customization tab at settings page is clicked");

    }


    //To verify the user menu tab of customization tab at the settings page
    settings_user_menu_tab() {
        cy.get('body').then((body) => {
            if (body.find('.main-content > div.tabs-wrapper > ul > li >a').length > 0) {
                cy.xpath("//div[@class='main-content user_menu_options']//div[@class='tabs-wrapper']//ul//li//a[text()='User Menu']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("User Menu")) {
                            assert.exists('User Menu', 'User Menu tab of customization tab at settings page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"User Menu" is not displayed at User Menu tab of customization tab at settings page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at settings page is not displayed')
            }
        })
    }


    //To verify the player tab of customization tab at the settings page
    settings_player_tab() {
        cy.get('body').then((body) => {
            if (body.find('.main-content > div.tabs-wrapper > ul > li >a').length > 0) {
                cy.xpath("//div[@class='main-content user_menu_options']//div[@class='tabs-wrapper']//ul//li//a[text()='Player']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Player")) {
                            assert.exists('Player', 'Player tab of customization tab at settings page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Player" is not displayed at Player tab of customization tab at settings page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at settings page is not displayed')
            }
        })
    }

    //To verify the colors tab of customization tab at the settings page
    settings_colors_tab() {
        cy.get('body').then((body) => {
            if (body.find('.main-content > div.tabs-wrapper > ul > li >a').length > 0) {
                cy.xpath("//div[@class='main-content user_menu_options']//div[@class='tabs-wrapper']//ul//li//a[text()='Colors']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Colors")) {
                            assert.exists('Colors', 'Colors tab of customization tab at settings page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Colors" is not displayed at Colors tab of customization tab at settings page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at settings page is not displayed')
            }
        })
    }


//To verify the email template tab of customization tab at the settings page
    settings_email_template_tab() {
        cy.get('body').then((body) => {
            if (body.find('.main-content > div.tabs-wrapper > ul > li >a').length > 0) {
                cy.xpath("//div[@class='main-content user_menu_options']//div[@class='tabs-wrapper']//ul//li//a[text()='Email template']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Email template")) {
                            assert.exists('Email template', 'Email template tab of customization tab at settings page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Email template" is not displayed at Email template tab of customization tab at settings page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at settings page is not displayed')
            }
        })
    }


//To verify the certificate tab of customization tab at the settings page
    settings_certificate_tab() {
        cy.get('body').then((body) => {
            if (body.find('.main-content > div.tabs-wrapper > ul > li >a').length > 0) {
                cy.xpath("//div[@class='main-content user_menu_options']//div[@class='tabs-wrapper']//ul//li//a[text()='Certificate']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Certificate")) {
                            assert.exists('Certificate', 'Certificate tab of customization tab at settings page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Certificate" is not displayed at Certificate tab of customization tab at settings page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at settings page is not displayed')
            }
        })
    }

    //To verify the attendance sheet tab of customization tab at the settings page
    settings_attendance_sheet_tab() {
        cy.get('body').then((body) => {
            if (body.find('.main-content > div.tabs-wrapper > ul > li >a').length > 0) {
                cy.xpath("//div[@class='main-content user_menu_options']//div[@class='tabs-wrapper']//ul//li//a[text()='Attendance sheet']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Attendance sheet")) {
                            assert.exists('Attendance sheet', 'Attendance sheet tab of customization tab at settings page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Attendance sheet" is not displayed at Attendance sheet tab of customization tab at settings page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at settings page is not displayed')
            }
        })
    }


    //To verify the glossary tab of customization tab at the settings page
    settings_glossary_tab() {
        cy.get('body').then((body) => {
            if (body.find('.main-content > div.tabs-wrapper > ul > li >a').length > 0) {
                cy.xpath("//div[@class='main-content user_menu_options']//div[@class='tabs-wrapper']//ul//li//a[text()='Glossary']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Glossary")) {
                            assert.exists('Glossary', 'Glossary tab of customization tab at settings page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Glossary" is not displayed at Glossary tab of customization tab at settings page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at settings page is not displayed')
            }
        })
    }

//To verify the Administration tab at the settings page
    settings_administration_tab() {
        cy.get('body').then((body) => {
            if (body.find('.main-content > div.tabs-wrapper > ul > li >a').length > 0) {
                cy.xpath("//div[@class='main-content user_menu_options']//div[@class='tabs-wrapper']//ul//li//a[text()='Administration']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Administration")) {
                            assert.exists('Administration', 'Administration tab at settings page is displayed')
                            this.click_administration_tab_into_settings();
                            this.settings_audit_log_tab();
                            this.settings_email_sms_log_tab();
                            this.settings_statistics_tab();
                            this.settings_deferred_jobs_tab();

                        } else {
                            assert.notExists(null, 'The text,"Administration" is not displayed at Administration tab of settings page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at settings page is not displayed')
            }
        })
    }

    //To click administration tab into the settings page
    click_administration_tab_into_settings() {

        cy.xpath("//div[@class='main-content user_menu_options']//div[@class='tabs-wrapper']//ul//li//a[text()='Administration']")
            .click().log("Administration tab at settings page is clicked");

    }

    //To verify the audit log tab of administration tab at the settings page
    settings_audit_log_tab() {
        cy.get('body').then((body) => {
            if (body.find('#audit-log-page >div.tabs-wrapper>ul>li>a').length > 0) {
                cy.xpath("//div[@id='audit-log-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Audit log']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Audit log")) {
                            assert.exists('Audit log', 'Audit log tab of administration tab at settings page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Audit log" is not displayed at Audit log tab of administration tab at settings page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at settings page is not displayed')
            }
        })
    }


//To verify the Email & Sms log tab of administration tab at the settings page
    settings_email_sms_log_tab() {
        cy.get('body').then((body) => {
            if (body.find('#audit-log-page >div.tabs-wrapper>ul>li>a').length > 0) {
                cy.xpath("//div[@id='audit-log-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Email & Sms log']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Email & Sms log")) {
                            assert.exists('Email & Sms log', 'Email & Sms log tab of administration tab at settings page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Email & Sms log" is not displayed at Email & Sms log tab of administration tab at settings page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at settings page is not displayed')
            }
        })
    }


//To verify the Statistics tab of administration tab at the settings page
    settings_statistics_tab() {
        cy.get('body').then((body) => {
            if (body.find('#audit-log-page >div.tabs-wrapper>ul>li>a').length > 0) {
                cy.xpath("//div[@id='audit-log-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Statistics']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Statistics")) {
                            assert.exists('Statistics', 'Statistics tab of administration tab at settings page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Statistics" is not displayed at Statistics tab of administration tab at settings page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at settings page is not displayed')
            }
        })
    }

//To verify the Deferred jobs tab of administration tab at the settings page
    settings_deferred_jobs_tab() {
        cy.get('body').then((body) => {
            if (body.find('#audit-log-page >div.tabs-wrapper>ul>li>a').length > 0) {
                cy.xpath("//div[@id='audit-log-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Deferred jobs']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Deferred jobs")) {
                            assert.exists('Deferred jobs', 'Deferred jobs tab of administration tab at settings page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Deferred jobs" is not displayed at Deferred jobs tab of administration tab at settings page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at settings page is not displayed')
            }
        })
    }

    //To click user view menu from side menu
    click_user_view_side_menu() {
        cy.xpath("//div[@class='nav-wrapper']//a[@href='/user/dashboard']//span[text()='User view']")
            .click()
            .log("successfully clicked the user view from side menu")

    }

    //To click admin view menu from side menu
    click_admin_view_side_menu() {
        cy.xpath("//div[@class='nav-wrapper']//a[@href='/enterprise/dashboard']//span[text()='Admin view']")
            .click()
            .log("successfully clicked the admin view from side menu")

    }

    //To click the my results from side menu
    click_my_results_side_menu() {

        cy.xpath("//div[@class='nav-wrapper']//a[@href='/user/evaluations/list/eval']//span[text()='My results']").click()
            .log("Successfully clicked the my results menu from side menu");

    }

    //To verify the evaluation tab at the my results page
    my_results_evaluation_tab() {
        cy.get('body').then((body) => {
            if (body.find('.main-content>div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@class='main-content evaluations-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Evaluations']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Evaluations")) {
                            assert.exists('Evaluations', 'Evaluations tab at my results page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Evaluations" is not displayed at Evaluations tab of my results page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at my results page is not displayed')
            }
        })
    }

    //To verify the Free tests tab at the my results page
    my_results_free_tests_tab() {
        cy.get('body').then((body) => {
            if (body.find('.main-content>div.tabs-wrapper >ul > li > a').length > 0) {
                cy.xpath("//div[@class='main-content evaluations-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Free tests']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Free tests")) {
                            assert.exists('Free tests', 'Free tests tab at my results page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Free tests" is not displayed at Free tests tab of my results page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at my results page is not displayed')
            }
        })
    }

    //To click the my certificates menu from side menu
    click_my_certificates_side_menu() {

        cy.xpath("//div[@class='nav-wrapper']//a[@href='/user/certificates/user/view']//span[text()='My certificates']").click()
            .log("Successfully clicked the my certificates menu from side menu");

    }

    //To click the my skills menu from side menu
    click_my_skills_side_menu() {
        cy.xpath("//div[@class='nav-wrapper']//a[@href='/user/skills/view']//span[text()='My skills']")
            .click().log("Successfully clicked the my skills menu from side menu");

    }

    //To click the Business Intelligence menu from side menu
    click_business_intelligence_side_menu() {
        cy.xpath("//div[@class='nav-wrapper']//a[@href='/enterprise/bi/view/dashboards/list']//span[text()='Business Intelligence']")
            .click({force: true}).log("Successfully clicked the Business Intelligence from side menu");

    }

    //To verify the analytics tab at the Business Intelligence page
    business_intelligence_analytics() {
        cy.get('body').then((body) => {
            if (body.find('#bi-dashboard-page > div.tabs-wrapper > ul > li >a').length > 0) {
                cy.xpath("//div[@id='bi-dashboard-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Analytics']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Analytics")) {
                            assert.exists('Analytics', 'Analytics tab at Business Intelligence page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Analytics" is not displayed at Analytics tab at Business Intelligence page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at Business Intelligence page is not displayed')
            }
        })
    }

    //To verify the Charts tab at the Business Intelligence page
    business_intelligence_charts() {
        cy.get('body').then((body) => {
            if (body.find('#bi-dashboard-page > div.tabs-wrapper > ul > li >a').length > 0) {
                cy.xpath("//div[@id='bi-dashboard-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Charts']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Charts")) {
                            assert.exists('Charts', 'Charts tab at Business Intelligence page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Charts" is not displayed at Charts tab at Business Intelligence page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at Business Intelligence page is not displayed')
            }
        })
    }

    //To click the dashboard at business intelligence
    click_dashboard_at_business_intelligence() {

        cy.xpath("//section[@id='dashboard-list']//div[@class='dashboard-div flex-row']//a[contains(text(),'My first Dashboard')]")
            .click().log("Successfully clicked the dashboard at business intelligence");

    }

    //To verify the overview tab of dashboard at the Business Intelligence page
    business_intelligence_overview() {
        cy.get('body').then((body) => {
            if (body.find('#bi-dashboard-page > div.tabs-wrapper > ul > li >a').length > 0) {
                cy.xpath("//div[@id='bi-dashboard-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Overview']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Overview")) {
                            assert.exists('Overview', 'Overview tab at Business Intelligence page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Overview" is not displayed at Overview tab of Business Intelligence page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at Business Intelligence page is not displayed')
            }
        })
    }

    //To verify the Description tab of dashboard at the Business Intelligence page
    business_intelligence_description() {
        cy.get('body').then((body) => {
            if (body.find('#bi-dashboard-page > div.tabs-wrapper > ul > li >a').length > 0) {
                cy.xpath("//div[@id='bi-dashboard-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Description']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Description")) {
                            assert.exists('Description', 'Description tab of Business Intelligence page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Description" is not displayed at Description tab of Business Intelligence page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at Business Intelligence page is not displayed')
            }
        })
    }

    //To verify the Visibility tab of dashboard at the Business Intelligence page
    business_intelligence_visibility() {
        cy.get('body').then((body) => {
            if (body.find('#bi-dashboard-page > div.tabs-wrapper > ul > li >a').length > 0) {
                cy.xpath("//div[@id='bi-dashboard-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Visibility']")
                    .then(value => {
                        const text = value.text();
                        cy.log("Text inside the xpath ", text)
                        if (text.includes("Visibility")) {
                            assert.exists('Visibility', 'Visibility tab of Business Intelligence page is displayed')
                        } else {
                            assert.notExists(null, 'The text,"Visibility" is not displayed at Visibility tab at Business Intelligence page')
                        }
                    })
            } else {
                assert.notExists(null, 'Tabs at Business Intelligence page is not displayed')
            }
        })
    }

    //To click the charts tab at business intelligence
    click_business_intelligence_charts() {

        cy.xpath("//div[@id='bi-dashboard-page']//div[@class='tabs-wrapper']//ul//li//a[text()='Charts']")
            .click().log("Clicked the charts at business intelligence page");

    }

    //To click the dashboard at business intelligence
    click_charts_at_business_intelligence() {

        cy.xpath("//div[@id='chart-row']//div[@class='dashboard-div flex-row']//a[contains(text(),'Certification rate global indicator')]")
            .click().log("Successfully clicked the chart at business intelligence");

    }


}


export default TabsPageSpec