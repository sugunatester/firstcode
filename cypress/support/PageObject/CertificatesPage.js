class CertificatesPage {
//To click the certificate menu from side menu
    click_certificates_side_menu() {

        cy.xpath("//div[@class='nav-wrapper']//a[@href='/enterprise/certificates/view']//span[text()='Certificates']").click()
            .log("Successfully clicked the certificates menu from side menu");

    }
}

export default CertificatesPage