import {stringify} from "mocha/lib/utils";

class UserPage {


    click_invite_tab_on_user_page() {

        cy.xpath("//a[@href='/enterprise/users/invite']").click();

    }

    my_random_number() {
        const uuid = () => Cypress._.random(0, 1e6)
        const id = uuid()
        const randno = `no${id}`
        cy.log('my rand no is', randno);
        Cypress.env('Static_Random_number', randno)
        cy.readFile("cypress/fixtures/data.json", (err, data) => {
            if (err) {
                return cy.log("error");
            }
        }).then((data) => {
            data.rand_no = randno
            cy.writeFile("cypress/fixtures/data.json", JSON.stringify(data))
        })

    }


    enter_mail_subject(mail_sub, rand) {

        cy.xpath("//input[@id='email_subject']")
            .clear()
            .type(mail_sub["mail_subject"] + rand["rand_no"])
            .log("Successfully entered the mail subject + static random number")

    }

    enter_mail_message(mail_sub) {

        cy.xpath("//div[@class='email-message ']//textarea[@id='email_message']")
            .clear()
            .type(mail_sub["mail_message"])
            .log("Successfully entered the mail message")

    }

    current_date() {
        let current_date_with_time;
        const moment = require('moment');
        let today = new Date()
        let convert_today_date_to_number = today.toLocaleDateString()
        const today_date_format = moment(convert_today_date_to_number).format('YYYY-MM-DD');
        let today_date_current_time = today.getTime()
        let today_date_of_time_format = moment(today_date_current_time).format('HH:mm')
        cy.log('Today Date: ' + today_date_format);
        cy.log('Today Date of Current time: ' + today_date_of_time_format);
        current_date_with_time = today_date_format + " " + today_date_of_time_format;
        return current_date_with_time
    }

    tomorrow_date() {
        let tomorrow_date_with_time;
        const moment = require('moment');
        let today_date = new Date()
        let tomorrow_date = new Date()
        tomorrow_date.setDate(today_date.getDate() + 1);
        let convert_tomorrow_date_to_number = tomorrow_date.toLocaleDateString()
        const tomorrow_date_format = moment(convert_tomorrow_date_to_number).format('YYYY-MM-DD');
        let tomorrow_date_of_current_time = tomorrow_date.getTime()
        let tomorrow_date_of_time_format = moment(tomorrow_date_of_current_time).format('HH:mm')
        cy.log('Tomorrow Date: ' + tomorrow_date_format);
        tomorrow_date_with_time = tomorrow_date_format + " " + tomorrow_date_of_time_format
        return tomorrow_date_with_time
    }


    enter_start_date_as_current_date() {

        let current_date_with_time = this.current_date()
        cy.xpath("//input[@class='form_datetime form-control' and @id='start_date']")
            .clear()
            .type(current_date_with_time)
            .log(current_date_with_time)
            .log("Successfully entered the today date")

    }

    enter_end_date_as_tomorrow_date() {
        let tomorrow_date_with_time = this.tomorrow_date()

        cy.xpath("//input[@class='form_datetime form-control' and @id='end_date']")
            .clear()
            .type(tomorrow_date_with_time)
            .log(tomorrow_date_with_time)
            .log("Successfully entered the end date")

    }


    click_emails_and_mobile() {
        cy.get('#select-contacts-btn').click();

    }

    enter_user_email_id_on_user_page_on_invite_window(mailid, domain) {
        cy.get("#contacts-participants")
            .type(mailid + domain)
            .log("successfully entered the user email id");

    }

    click_save_button_invite_user_page() {
        cy.xpath("//a[@id = 'save-contacts']").click();

    }

    click_invite_button() {
        cy.xpath("//*[@name='invite-btn']").click();

    }


    enter_user_first_name_on_user_registeration_window(name) {

        // cy.xpath("//form[@id='enterprise-register-form']//input[@id='first_name']").type(first_name);
        cy.get(':nth-child(2) > #first_name').clear().type(name);


    }

    enter_user_last_name_on_user_registeration_window(name) {

        // cy.xpath("//form[@id='enterprise-register-form']//input[@id='last_name']").type(last_name);
        cy.get(':nth-child(3) > #last_name').clear().type(name);


    }

    enter_user_password_on_user_registeration_window(pwd) {

        cy.xpath("//form[@id='enterprise-register-form']//input[@id='password']").type(pwd);

    }

    enter_user_confirm_password_on_user_registeration_window(confirm) {

        cy.xpath("//form[@id='enterprise-register-form']//input[@id='password2']").type(confirm);

    }

    click_register_button_on_user_registeration_window() {

        // cy.xpath("//*[@id='enterprise-register-form']/div//button").click();
        cy.get('#enterprise-register-form > .btn-toolbar > .btn-primary').click({force: true});

    }

    enter_user_mail_id_on_connection_window(mail_id, domain) {

        cy.get('#sign-in-form > :nth-child(1) > .form-control')
            .type(mail_id + domain)
            .log("successfully entered the user mail id");

    }

    enter_user_password_on_connection_window(password) {
        cy.xpath("//form[@id='sign-in-form']//input[@name='password']")
            .type(password)
            .log("successfully entered the user password", password);

    }

    enter_user_password_on_connection_window_after_getting_password() {
        let my_got_password_from_email_log = Cypress.env('my_passwords')
        cy.log('my got password from email_log--', my_got_password_from_email_log)
        let my_got_password_from_mail = Cypress.env('my_got_password_from_mail')
        cy.log('my got password on from mail--', my_got_password_from_mail)
        if (my_got_password_from_email_log === my_got_password_from_mail) {
            cy.xpath("//form[@id='sign-in-form']//input[@name='password']")
                .type(my_got_password_from_mail)
                .log("successfully entered the user matched password");
        } else {
            cy.xpath("//form[@id='sign-in-form']//input[@name='password']")
                .type(my_got_password_from_email_log)
            cy.log("enter the user unmatched password")
        }
    }

    enter_user_password_on_connection_window_after_getting_password_from_email_and_sms_log(user_password) {
        // let my_got_password_from_email_log = Cypress.env('my_passwords')
        cy.log('my got password from email_log--', user_password)
        cy.xpath("//form[@id='sign-in-form']//input[@name='password']")
            .type(user_password)
            .log("successfully entered the user matched password");
    }


    click_sign_in_button_by_user() {

        cy.xpath("//button[text()='Sign-in']")
            .click();

        cy.wait(5000)
    }

    verify_GDPR_message_is_accept_if_displayed(settingdata) {
        cy.xpath("//p[contains(text(),'" + settingdata["gdpr_message"] + "')]", {timeout: 50000})
            .then($button => {
                if ($button.is(':visible')) {
                    cy.log('the GDPR Message is displayed')
                    cy.xpath("//input[@value='I accept']").click();
                } else {
                    cy.log("The GDPR message is not displayed")
                }
            })
    }

    enable_email_on_perferred_channel_option() {

        cy.xpath("//label[@for='preferred_channel-0']").click();

    }

    click_save_button_on_create_user_page() {

        cy.xpath("(//button[@class='btn btn-primary'])[1]").click();

    }

    verify_created_user_is_existed(last_name, first_name) {
        this.click_all_status_filter_field_on_user_list_page()
        this.filter_the_created_status_on_user_list_page()
        cy.xpath("//div[@class='cell name' and text()='" + last_name.toUpperCase() + " " + first_name[0].toUpperCase() + first_name.slice(1) + "']")
            .should('be.exist')
    }


    verify_invited_user_is_existed(user_name, domain) {
        this.click_all_status_filter_field_on_user_list_page()
        this.filter_the_invited_status_on_user_list_page()
        cy.get('body').then($iniviteduser => {
            // const isVisible = $iniviteduser.find("//div[@class='cell name' and text()='[" + user_name + domain + "]']")
            const isVisible = $iniviteduser.find("div:contains('[" + user_name + domain + "]')")
            if (isVisible) {
                cy.log("invited user is existed in the invited user status")
            } else {
                cy.reload()
                this.verify_invited_user_is_existed();
            }
        });
    }

    //     this.click_all_status_filter_field_on_user_list_page()
    //     this.filter_the_all_status_option_on_user_list_page()
    //     this.click_all_status_filter_field_on_user_list_page()
    //     this.filter_the_invited_status_on_user_list_page()
    //     let users;
    //     users = cy.xpath("//div[@class='cell name' and text()='[" + user_name + domain + "]']",{timeout:5000})
    //     if (users) {
    //         cy.log("user is exist")
    //     } else {
    //         cy.log("user is not exist")
    //     }
    // }


    click_all_status_filter_field_on_user_list_page() {

        cy.xpath("//span[@id='status-filter-button']")
            .click()
            .log("successfully clicked the 'All status' filter field on user list page")

    }

    filter_the_created_status_on_user_list_page() {

        cy.xpath("//*[contains(@id,'ui-id')][contains(text(),'created')]")
            .click()
            .log("successfully filter the created status on user list page")

    }

    filter_the_invited_status_on_user_list_page() {

        cy.xpath("//*[contains(@id,'ui-id')][contains(text(),'invited')]")
            .click()
            .log("successfully filter the invited status on user list page")

    }

    filter_the_all_status_option_on_user_list_page() {

        cy.xpath("//*[contains(@id,'ui-id')][contains(text(),'All status')]")
            .click()
            .log("successfully filter the invited status on user list page")

    }


    click_activate_button_on_created_user(user) {
        // cy.xpath("//div[text()='" + user["last_name"] + " " + user["first_name"] + "']//following::a[contains(@title,'Activation')]").click();
        cy.xpath("(//div[text()='" + user["last_name"].toUpperCase() + " " + user["first_name"][0].toUpperCase() + user["first_name"].slice(1) + "']//following::a[contains(@title,'Activation')])[1]").click();
    }

    verify_the_user_is_active(user) {
        this.click_all_status_filter_field_on_user_list_page()
        this.filter_the_all_status_option_on_user_list_page();
        let active;
        active = cy.xpath("(//div[text()='" + user["last_name"].toUpperCase() + " " + user["first_name"][0].toUpperCase() + user["first_name"].slice(1) + "']//following::div[contains(text(),'active')])[1]")
        if (active) {
            cy.log("user is activated")
        } else {
            cy.log("user is not active")
        }
    }

    click_new_user_button() {

        cy.xpath("//a[@href='/enterprise/users/create']").click();

    }

    enter_user_first_name_on_create_user_page(user) {

        cy.get('#first_name').type(user)

    }

    enter_user_last_name_on_create_user_page(user) {

        cy.get('#last_name').type(user)

    }

    enter_user_email_id_on_create_user_page(mailid, domain) {

        cy.get('#email').type(mailid + domain)

    }

    click_connection_button() {
        cy.get("#main-nav > :nth-child(7) > a", {timeout: 50000}).click()

    }

    log_out_user() {
        cy.xpath("//div[@class='login-slide']")
            .should('be.hidden')
            .invoke('show')
        cy.xpath("//div[@class='login-slide']//li//a[text()='Log Out']")
            .click()
    }


    click_questionnaire_invitation(questionnaire) {
        // cy.xpath("//div[normalize-space(text()='" + questionnaire["questionnaire_name"] + "')]//parent::a[contains(@href,'/ev/')]",{timeout:90000})
        cy.xpath("//div[contains(normalize-space(text()),'" + questionnaire["questionnaire_name"] + "')]", {timeout: 90000})
            .click()
    }

    verify_cycle_event_of_questionnaire_invitation_is_exist(questionnaire) {
        // cy.xpath("//div[normalize-space(text()='" + questionnaire["questionnaire_name"] + "')]//parent::a[contains(@href,'/ev/')]")
        cy.xpath("//div[normalize-space(text())='" + questionnaire["questionnaire_name"] + "']")
            .should('be.exist')
    }

    click_free_to_play_questionnaire(questionnaire) {
        cy.xpath("//div[@class='new-play-card-wrapper']//div[contains(@class,'wrapper')]//div[contains(text(),'" + questionnaire["questionnaire_name"] + "')]")
            .click()
    }

    click_survey_invitation(surveydata) {
        cy.xpath("//div[contains(text(),'SURVEY')]/..//div[contains(text(),'" + surveydata["form_name"] + "')]", {timeout: 50000})
            .click()

    }

    verify_cycle_event_of_survey_invitation_is_exist(surveydata) {
        cy.xpath("//div[contains(text(),'SURVEY')]/..//div[contains(text(),'" + surveydata["form_name"] + "')]", {timeout: 50000})
            .should('be.exist')

    }

    verify_all_user_side_menu_is_exist() {
        this.verify_myquestion_side_menu_is_exist_on_user_account()
        this.verify_mymedia_side_menu_is_exist_on_user_account()
        this.verify_myresult_side_menu_is_exist_on_user_account()
        this.verify_mytraining_side_menu_is_exist_on_user_account()
        this.verify_mymodule_side_menu_is_exist_on_user_account()
        this.verify_mypath_side_menu_is_exist_on_user_account()
        this.verify_mysurvey_side_menu_is_exist_on_user_account()
        this.verify_myscorecard_side_menu_is_exist_on_user_account()
        this.verify_myreview_side_menu_is_exist_on_user_account()
        this.verify_mycertificate_side_menu_is_exist_on_user_account()
        this.verify_myskill_side_menu_is_exist_on_user_account()
        this.verify_mybattle_side_menu_is_exist_on_user_account()
    }

    verify_myquestion_side_menu_is_exist_on_user_account() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/user/questions'] span[class='text']").length > 0) {
                cy.get("a[href='/user/questions'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("My questions")) {
                            cy.log('the my question side menu is exist in user account')
                        } else {
                            assert.notExists(null, 'the my question side menu is not exist in user account')
                        }
                    });
            } else {
                assert.notExists(null, 'the my question side menu is not exist in user account')

            }
        });
    }

    verify_mymedia_side_menu_is_exist_on_user_account() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/user/media'] span[class='text']").length > 0) {
                cy.get("a[href='/user/media'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("My medias")) {
                            cy.log('the my medias side menu is exist in user account')
                        } else {
                            assert.notExists(null, 'the my media side menu is not exist in user account')
                        }
                    });
            } else {
                assert.notExists(null, 'the my media side menu is not exist in user account')

            }
        });
    }

    verify_myresult_side_menu_is_exist_on_user_account() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/user/evaluations/list/eval'] span[class='text']").length > 0) {
                cy.get("a[href='/user/evaluations/list/eval'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("My results")) {
                            cy.log('the my result side menu is exist in user account')
                        } else {
                            assert.notExists(null, 'the my result side menu is not exist in user account')
                        }
                    });
            } else {
                assert.notExists(null, 'the my result side menu is not exist in user account')

            }
        });
    }

    verify_mytraining_side_menu_is_exist_on_user_account() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/user/trainings'] span[class='text']").length > 0) {
                cy.get("a[href='/user/trainings'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("My trainings")) {
                            cy.log('the my training side menu is exist in user account')
                        } else {
                            assert.notExists(null, 'the my training side menu is not exist in user account')
                        }
                    });
            } else {
                assert.notExists(null, 'the my training side menu is not exist in user account')

            }
        });
    }

    verify_mymodule_side_menu_is_exist_on_user_account() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/user/modules'] span[class='text']").length > 0) {
                cy.get("a[href='/user/modules'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("My modules")) {
                            cy.log('the my modules side menu is exist in user account')
                        } else {
                            assert.notExists(null, 'the my modules side menu is not exist in user account')
                        }
                    });
            } else {
                assert.notExists(null, 'the my modules side menu is not exist in user account')

            }
        });
    }

    verify_mypath_side_menu_is_exist_on_user_account() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/user/paths/list'] span[class='text']").length > 0) {
                cy.get("a[href='/user/paths/list'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("My paths")) {
                            cy.log('the my paths side menu is exist in user account')
                        } else {
                            assert.notExists(null, 'the my paths side menu is not exist in user account')
                        }
                    });
            } else {
                assert.notExists(null, 'the my paths side menu is not exist in user account')

            }
        });
    }

    verify_mysurvey_side_menu_is_exist_on_user_account() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/user/surveys/list'] span[class='text']").length > 0) {
                cy.get("a[href='/user/surveys/list'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("My surveys")) {
                            cy.log('the my survey side menu is exist in user account')
                        } else {
                            assert.notExists(null, 'the my survey side menu is not exist in user account')
                        }
                    });
            } else {
                assert.notExists(null, 'the my survey side menu is not exist in user account')

            }
        });
    }

    verify_myscorecard_side_menu_is_exist_on_user_account() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/user/scorecards'] span[class='text']").length > 0) {
                cy.get("a[href='/user/scorecards'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("My scorecards")) {
                            cy.log('the my scorecard side menu is exist in user account')
                        } else {
                            assert.notExists(null, 'the my scorcard side menu is not exist in user account')
                        }
                    });
            } else {
                assert.notExists(null, 'the my scorecard side menu is not exist in user account')

            }
        });
    }

    verify_myreview_side_menu_is_exist_on_user_account() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/user/reviews/list'] span[class='text']").length > 0) {
                cy.get("a[href='/user/reviews/list'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("My reviews")) {
                            cy.log('the my reviews side menu is exist in user account')
                        } else {
                            assert.notExists(null, 'the my reviews side menu is not exist in user account')
                        }
                    });
            } else {
                assert.notExists(null, 'the my reviews side menu is not exist in user account')

            }
        });
    }

    verify_mycertificate_side_menu_is_exist_on_user_account() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/user/certificates/user/view'] span[class='text']").length > 0) {
                cy.get("a[href='/user/certificates/user/view'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("My certificates")) {
                            cy.log('the my certificates side menu is exist in user account')
                        } else {
                            assert.notExists(null, 'the my certificates side menu is not exist in user account')
                        }
                    });
            } else {
                assert.notExists(null, 'the my certificates side menu is not exist in user account')

            }
        });
    }

    verify_myskill_side_menu_is_exist_on_user_account() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/user/skills/view'] span[class='text']").length > 0) {
                cy.get("a[href='/user/skills/view'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("My skills")) {
                            cy.log('the my skills side menu is exist in user account')
                        } else {
                            assert.notExists(null, 'the my skills side menu is not exist in user account')
                        }
                    });
            } else {
                assert.notExists(null, 'the my skills side menu is not exist in user account')

            }
        });
    }

    verify_mybattle_side_menu_is_exist_on_user_account() {
        cy.get('body').then((body) => {
            if (body.find("a[href='/user/battles'] span[class='text']").length > 0) {
                cy.get("a[href='/user/battles'] span[class='text']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("My battles")) {
                            cy.log('the my battles side menu is exist in user account')
                        } else {
                            assert.notExists(null, 'the my battles side menu is not exist in user account')
                        }
                    });
            } else {
                assert.notExists(null, 'the my battles side menu is not exist in user account')

            }
        });
    }

    click_group_tab_on_user_list_page() {
        cy.xpath("//a[text()='Groups']").click()
            .log("Successfully clicked the 'Groups' tab on the user list page")
    }

    click_create_group_button() {
        cy.xpath("//a[text()='Create Group']").click()
            .log("Successfully clicked the create group button on the group list page")
    }

    enter_group_name(group) {
        cy.log('my group name is', group)
        cy.xpath("(//input[@id='group_name'])[1]").type(group["group-1"])
            .log("Successfully clicked the create group button on the group list page")
    }

    click_create_button_on_group_popup() {
        cy.xpath("(//button[@class='btn btn-primary'])[1]").click()
            .log("Successfully clicked the create group button on the group list page")
    }

    click_group_filter_field() {
        cy.xpath("//span[@id='group-filter-button']").click({timeout: 5000})
            .log("Successfully clicked the create group button on the group list page")
    }

    select_created_group(group) {
        cy.xpath("//div[text()='" + group["group-1"] + "']").click({timeout: 5000})
            .log("Successfully clicked the create group button on the group list page")
    }

    select_all_group() {
        cy.xpath("//div[contains(@id,'ui-id')][contains(text(),'All users')]").click({timeout: 5000})
            .log("Successfully clicked the create group button on the group list page")
        cy.wait(5000)
    }

    assign_user(user) {
        cy.wrap(user).each((group) => {
            cy.xpath("//div[text()='" + group["last_name"].toUpperCase() + " " + group["first_name"][0].toUpperCase() + group["first_name"].slice(1) + "']/..//span[@class='fa fa-square-o unchecked  ']")
                .click({timeout: 50000})
        })

    }

    click_save_button_after_user_assign() {
        cy.xpath("//a[@id='checked-save-btn']")
            .click({timeout: 60000})
    }

    verify_user_on_created_group(group, usersdatas) {
        this.click_group_filter_field()
        this.select_all_group(group)
        cy.wrap(usersdatas).each((user) => {
            // let user_on_group = cy.xpath("//span[@title='" + group + "']/../../div/a[contains(text(),'" + user["last_name"].upper() + " " + user["first_name"] + "')]")
            cy.xpath("//span[@title='" + group["group-1"] + "']/../../div/a[contains(text(),'" + user["last_name"].toUpperCase() + " " + user["first_name"][0].toUpperCase() + user["first_name"].slice(1) + "')]")
                .should('exist')
        })
    }


    verify_GDPR_for_contact_user_and_accept_GDPR_message(settingdata) {
        cy.xpath("//p[contains(text(),'" + settingdata["gdpr_message"] + "')]", {timeout: 50000})
            .then($button => {
                if ($button.is(':visible')) {
                    cy.log('the GDPR Message is displayed')
                    cy.xpath("//input[@value='I accept']").click();
                } else {
                    cy.log("The GDPR message is not displayed")
                }
            })
    }

    click_training_invitation_from_active_user_dashboard(training) {
        cy.xpath("//div[contains(.,'" + training["training_name"] + "')]/a[contains(@href,'/user/trainings')]")
            .click()
    }

    accept_training_invitation_by_active_user() {
        cy.xpath("//a[contains(@href,'/confirm')]")
            .click()
    }

    click_questionnaire_on_practice_section_of_training(training_ques) {
        cy.wait(50000)
        cy.wrap(training_ques).each(tq => {
            if (tq.usage === "free") {
                // cy.get('.flex-table > .flex-row')
                // cy.get("section[id='free-evals-bloc-2'] a[href]")
                cy.xpath("//div[text()='" + tq["questionnaire_name"] + "']/../../..//following::div[@class='flex-table']//a[@class='flex-row']")
                    .invoke('removeAttr', 'target').click()
                cy.xpath("//a[text()='Start']").click()
            }
        })
    }

    click_cycle_questionnaire_on_practice_section_of_training(training_ques) {
        cy.wait(50000)
        cy.wrap(training_ques).each(cq => {
            if (cq.questionnaire_name === "second_questionnaires") {
                // cy.get('.flex-table > .flex-row')
                // cy.get("section[id='free-evals-bloc-2'] a[href]")
                cy.xpath("//div[text()='" + cq["questionnaire_name"] + "']/../../..//following::div[@class='flex-table']//a[@class='flex-row']")
                    .invoke('removeAttr', 'target').click()
                cy.xpath("//a[text()='Start']").click()
            }
        })
    }

    click_questionnaire_on_evaluation_section_of_training(training_ques) {
        cy.wait(5000)
        cy.wrap(training_ques).each(tq => {
            if (tq.usage === "evaluation") {
                // cy.get('.flex-table > .flex-row')
                // cy.get("section[id='free-evals-bloc-2'] a[href]")
                cy.xpath("//i[@class='icon icon-evaluation']/../..//div[text()='" + tq["questionnaire_name"] + "']/../../..//following::div[@class='flex-table']//a[@class='flex-row']")
                    .invoke('removeAttr', 'target').click()
                cy.xpath("//a[text()='Start']").click()
            }
        })
    }

    click_exit_button_on_questionnaire_testscore_page_while_contact_user_play_test() {
        cy.xpath("//a[text()='Exit']").click()
    }

    click_edit_user_button(userdata) {
        cy.wrap(userdata).each(user => {
            cy.xpath("//div[text()='" + user["last_name"].toUpperCase() + " " + user["first_name"][0].toUpperCase() + user["first_name"].slice(1) + "']/..//a[@title='Edit user']")
                .click()
        })
    }

    click_define_boss_button_on_edit_user_page() {
        cy.xpath("//a[normalize-space(text())='Define boss']")
            .click()
    }

    select_boss_for_admin(admindata) {
        cy.xpath("//span[normalize-space(text())='" + admindata["last_name"].toUpperCase() + " " + admindata["first_name"][0].toUpperCase() + admindata["first_name"].slice(1) + "']")
            .click()
    }

    verify_boss_user_is_added_in_hierarchy_section(admindata) {
        cy.xpath("//span[normalize-space(text())='Hierarchy']/../../..//span[text()='" + admindata["last_name"].toUpperCase() + " " + admindata["first_name"][0].toUpperCase() + admindata["first_name"].slice(1) + "']")
            .should('be.exist')

    }


}

export default UserPage;