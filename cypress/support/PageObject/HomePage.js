function delete_if_admin_account_is_exist(common_data) {
    let delete_admin_account = common_data["delete_www_account"]
    cy.visit(delete_admin_account)
}

class HomePage {

    click_register_button() {

        // return cy.xpath("//a[contains(.,'Register')]").click()
        cy.xpath("(//a[contains(.,'Free trial')])[1]")
            .click()

    }

    enter_email_id_on_registeration_page(email) {
        let email_id;
        email_id = email["last_name"] + email["first_name"] + email["domain"];
        return cy.xpath("//*[@id='register-form']//*[@name='email_or_mobile']").type(email_id).log("Successfully entered the email address");

    }

    enter_first_name_on_registeration_page(name) {
        cy.get('#first_name').type(name["first_name"]).log("Successfully entered the first name");

    }

    enter_last_name_on_registeration_page(name) {
        cy.xpath("//*[@id='last_name']").type(name["last_name"]).log("Successfully entered the last name");

    }

    enter_password_on_registeration_page(password) {
        cy.xpath("//form[@id='register-form']//input[@name='password']").type(password["password"]).log("Successfully entered the password");

    }

    enter_confirm_password_on_registeration_page(password) {
        cy.xpath("//form[@id='register-form']//input[@name='password2']").type(password["password"]).log("Successfully entered the confirmed password");

    }

    enter_enterprise_name_on_registeration_page(enterprise_name) {
        cy.xpath("//input[@name='enterprise_name']")
            .type(enterprise_name["www_enterprise"]).log("Successfully entered the confirmed password");

    }

    click_register_button_on_registeration_page() {
        cy.xpath("//button[text()='Register']")
            .click();
    }

    click_connection_button() {
        cy.get("#main-nav > :nth-child(7) > a", {timeout: 50000}).click()
        cy.log("Clicking the connection");

    }

    enter_admin_email_id(email) {

        let email_id;
        email_id = email["last_name"] + email["first_name"] + email["domain"];
        return cy.get('#sign-in-form > :nth-child(1) > .form-control', {timeout: 50000}).type(email_id).log("Successfully entered the email address");

    }

    enter_admin_password(passwords) {

        cy.xpath("//form[@id='sign-in-form']//input[@name='password']").type(passwords["password"]).log("Successfully entered the password");

    }

    click_sign_in_button() {

        cy.get('#sign-in-form > .btn-toolbar > .btn-primary').click()
        cy.wait(5000)
    }

    log_out_admin() {
        cy.xpath("//div[@class='login-slide']")
            .should('be.hidden')
            .invoke('show')
        cy.xpath("//div[@class='login-slide']//li//a[text()='Log Out']")
            .click({force: true})
    }


    verify_and_delete_registered_admin_if_logged(common_data) {
        cy.wait(6000)
        cy.get('body').then((body) => {
            if (body.find("#sign-in-form > .error-message").length > 0) {
                cy.get('#sign-in-form > .error-message')
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("Invalid email or password")){
                        // if (text.includes("Enterprise is erroneous")) {
                            cy.get('#sign-in-form > .btn-toolbar > .btn-default').click()
                        } else {
                            delete_if_admin_account_is_exist(common_data)
                        }
                    });
            } else {
                delete_if_admin_account_is_exist(common_data)
            }
        });

    }

    delete_if_admin_account_is_exist(link) {
        let delete_admin_account = link["delete_www_account"]
        cy.visit(delete_admin_account)
    }

    verify_error_message_for_unregistered_account(common_data) {
        cy.get('body').then((body) => {
            if (body.find("#sign-in-form > .error-message").length > 0) {
                cy.get('#sign-in-form > .error-message')
                cy.log('the registered account is deleted')
            } else {
                delete_if_admin_account_is_exist(common_data)
            }
        })
    }
    

}

export default HomePage;