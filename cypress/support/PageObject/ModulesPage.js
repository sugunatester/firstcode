import questionnairePage from "./QuestionnairePage";
import QuestionnairePage from "./QuestionnairePage";
import moment from "moment";

const questionnairepage = new QuestionnairePage();

class ModulesPage {

//To click the modules menu from side menu
    click_modules_side_menu() {

        cy.xpath("//div[@class='nav-wrapper']//a[@href='/enterprise/modules/list']//span[text()='Modules']").click()
            .log("Successfully clicked the modules menu from side menu");

    }

    //To click the new modules button at modules page
    click_new_modules_button() {

        cy.xpath("//a[@id='new-module-btn'][normalize-space(text()='New Modules')]")
            .click().log("Successfully clicked the 'new module' button at module page")

    }

    //To enter module name at modules creation page
    enter_module_name(module_name) {

        cy.xpath("//div[@class='bloc-body']//div//div//label[contains(text(),'Name of module')]//..//input[@id='module_name']")
            .type(module_name).log("Successfully entered the module name at module creation page");

    }

    //To click the save button at the module creation page
    click_save_button_at_module_creation_page() {

        cy.xpath("//button[@type='submit'][@form='module-form']").click()
            .log("Successfully clicked the save button at module creation page");

    }

    //To open the created module
    open_created_module(module) {
        cy.xpath("//a[contains(@class,'cell module-name')][contains(normalize-space(.),'" + module + "')]")
            .click()
    }

    //To click the elements tab at module overview page
    click_elements_tab() {
        cy.xpath("//a[text()='Elements']")
            .click()
    }

    click_steps_tab_on_module() {
        cy.xpath("//a[text()='Steps']")
            .click()
    }

    //To filter created base on element page of modules
    filter_created_base_on_element_page(basedatas) {
        this.click_topic_filter_field(basedatas)
    }

    //To click the topic filter
    click_topic_filter_field(basedatas) {
        cy.xpath("//*[@id='topic-filter-button']").click({force: true});
        this.select_base_on_topic_filter_field(basedatas)
    }

    //To select all options
    select_all_topic_option() {
        cy.xpath("//div[contains(@id,'ui-id')][contains(text(),'All active topics')]").click();
    }

    //To select the base on the topic filter
    select_base_on_topic_filter_field(basedatas) {
        cy.xpath("//ul[@id='topic-filter-menu']//div[text()='" + basedatas["base_name"] + "']").click();
    }

    //To add the questionnaire to the module
    select_questionnaire_to_module(ele) {
        cy.xpath("//span[contains(text(),'" + ele["name"] + "')]")
            .trigger("mousedown", {which: 1})
        cy.get('#items-set-3')
            .trigger("mousemove")
            .trigger("mouseup", {force: true})
        cy.log("Questionnaire is added to the module");

    }

    select_image_on_module_steps_page(ele) {
        cy.xpath("//span[contains(text(),'" + ele["name"] + "')]")
            .trigger("mousedown", {which: 1})
        cy.get('#items-set-3')
            .trigger("mousemove")
            .trigger("mouseup", {force: true})
        cy.log("media is added to the module");
    }

    //To verify success message after added elements into the modules
    verify_success_message_after_added_element_into_module() {
        cy.xpath("//div[@class='xq-flashes xq-m collapse']//ul//li[normalize-space(text()='Your settings have been saved')]", {timeout: 30000})
            .should('be.visible');
        cy.wait(5000)
    }

    click_module_trial_play_button(module) {
        cy.xpath("//a[contains(@class,'cell module-name')][contains(normalize-space(.),'" + module["module_name"] + "')]/..//a[@title='Try module']")
            .click()
    }

    play_module(module, question) {
        if (module.type === "questionnaire") {
            this.click_to_play_questionnaire_on_module_steps_player_page(module)
            this.answer_static_questionnaire_on_module_player_page(question)
        }
    }

    click_to_play_questionnaire_on_module_steps_player_page(module) {
        cy.xpath("//div[@class='new-play-card-wrapper']//div[contains(text(),'" + module["name"] + "')]").click()
    }

    answer_static_questionnaire_on_module_player_page(question) {
        questionnairepage.verify_start_button_before_playing_static_questionnaire()
        questionnairepage.select_answers_in_static_player_by_active_user_on_module_test(question)
    }

    click_next_step_button_on_module_player_page() {
        cy.xpath("//div[@id='std-toolbar']//a[@id='next-btn']").click()
    }

    close_module_test() {
        cy.xpath("//div[@id='std-toolbar']/a[@data-target='#module-close-modal']").click()
    }

    click_close_button_on_confirmation_popup() {
        cy.xpath("//*[@id='save-btn']").click()
    }

    click_select_questionnaire_tab_on_module_elements_page() {
        cy.xpath("//a[text()='Questionnaires']").click()
    }

    click_select_media_tab_on_module_elements_page() {
        cy.xpath("//a[text()='Medias']").click()
    }

    enable_practice_usage_on_module_steps_page() {
        cy.xpath("//label[@for='elements_options-0-usage-0']").click()
    }

    enable_evaluation_usage_on_module_steps_page() {
        cy.xpath("//label[@for='elements_options-1-usage-1']").click()
    }

    enable_certification_usage_on_module_steps_page() {
        cy.xpath("//label[@for='elements_options-2-usage-2']").click()
    }

    click_save_button_on_module_steps_page() {
        cy.xpath("//button[text()='Save']").click()
    }

    click_invitation_tab_on_module() {
        cy.xpath("//a[text()='Invitation']").click()
    }

    enter_mail_subject(mail_sub, rand) {
        cy.xpath("//input[@id='email_subject']")
            .clear()
            .type(mail_sub["mail_subject"] + rand["rand_no"])
            .log("Successfully entered the mail subject + static random number")
    }

    enter_mail_message() {
        cy.xpath("//div[@class='email-message ']//textarea[@id='email_message']")
            .clear()
            .type(mail_sub["mail_message"])
            .log("Successfully entered the mail message")
    }

    current_date() {
        let current_date_with_time
        const today_date = moment()

        const tdy_date_format = today_date.format('YYYY-MM-DD')
        cy.log(tdy_date_format)

        const nowTime = today_date.format('H:m')
        cy.log(nowTime)

        current_date_with_time = tdy_date_format + " " + nowTime;
        return current_date_with_time
    }


    tomorrow_date() {
        let tmr_date_with_time
        const tomorrow_date = moment().add(1, 'days');
        const tmr_date_format = tomorrow_date.format('YYYY-MM-DD')
        cy.log(tmr_date_format)

        const nowTime = tomorrow_date.format('H:m')
        cy.log(nowTime)

        tmr_date_with_time = tmr_date_format + " " + nowTime;
        return tmr_date_with_time
    }


    enter_start_date_as_current_date() {
        let current_date_with_time = this.current_date()
        cy.xpath("//input[@class='form_datetime form-control' and @id='start_date']")
            .clear()
            .type(current_date_with_time)
            .log("Successfully entered the today date")
    }

    enter_end_date_as_tomorrow_date() {
        let tomorrow_date_with_time = this.tomorrow_date()

        cy.xpath("//input[@class='form_datetime form-control' and @id='end_date']")
            .clear()
            .type(tomorrow_date_with_time)
            .log("Successfully entered the end date")

    }

    click_select_user_button_on_module_invitation_page() {
        cy.xpath("//a[@id='select-employees-btn']")
            .click()
    }

    select_activated_user_on_module_invitation_page(userdata) {
        cy.wrap(userdata).each(user => {
            cy.xpath("//span[contains(text(),'" + user["last_name"].toUpperCase() + " " + user["first_name"] + "')]/../..//div[@class='cell btns']")
                .click()
        })
    }

    click_validate_buttton_on_module_invitation_page() {
        cy.xpath("//a[@class='btn btn-primary save-btn']")
            .click()
    }

    click_invite_button_on_module_invitation_page() {
        cy.xpath("//button[@id='invite']").click();

    }
}

export default ModulesPage