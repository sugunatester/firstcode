class AdminDashboardPage {

    click_dashaboard_menu() {
        cy.xpath("//i[@class='icon icon-dashboard']").click();

    }

    click_admin_base_menu() {
        cy.get(":nth-child(3) > a > .icon").click()

    }

    click_admin_question_menu() {

        cy.xpath("//a[@href='/enterprise/questions/all']//span[text()='Questions']")
            .click()

    }

    click_admin_questionnaire_menu() {
        cy.wait(5000)
        cy.xpath("(//a[@href='/enterprise/questionnaires'])[1]")
            .click()

    }

    log_out_admin() {
        cy.xpath("//div[@class='login-slide']")
            .should('be.hidden')
            .invoke('show')
        cy.xpath("//div[@class='login-slide']//li//a[text()='Log Out']")
            .click()
    }

    click_my_profile_option_on_admin_profile() {
        cy.xpath("//div[@class='login-slide']")
            .should('be.hidden')
            .invoke('show')
        cy.xpath("//div[@class='login-slide']//li//a[text()='Log Out']")
            .click()
    }

}

export default AdminDashboardPage;
