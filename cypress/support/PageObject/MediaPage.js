class MediaPage {

    click_media_side_menu() {
        cy.xpath("//a[@href='/enterprise/media']").click().log("successfully clicked media side menu")

    }

    click_create_media_button() {
        cy.xpath("//a[@id='open-btn']", {timeout: 500000})
            .click()
            .log("successfully clicked create media button")
    }

    click_create_zip_button() {
        cy.xpath("//*[@id='zip-btn']", {timeout: 500000})
            .click().log("successfully clicked create media button")
    }

    click_upload_a_file_button(media) {
        if (media["type"] === "pdf" || media["type"] === "docx" || media["type"] === "image" || media["type"] === "xlsx") {
            // const filepath = "media/" + media["name"] + ""
            const filepath = media["name"]
            cy.log(filepath)
            const fileName = media["name"]
            cy.fixture(filepath).then((fileContent) => {
                cy.get('input[name="upl"]')
                    .wait(3000)
                    .attachFile({
                            fileContent, fileName, mimeType: 'docx/pdf/png/xlsx',
                        },
                        {
                            subjectType: 'drag-n-drop'
                        },
                    )
                cy.wait(3000)
            })
        } else {
            cy.xpath("//a[@id='cancel-btn']")
                .click()
            cy.log("finished to uploaded the medias")
        }
    }


    verify_uploaded_media_is_exist(media) {
        cy.xpath("//div[@class='filename']//span[text()='" + media + "']", {timeout: 50000})
            .should('be.visible')
    }

    click_factsheet_button() {

        cy.xpath("//*[@id='factsheet-btn']", {timeout: 50000})
            .click()
            .log("the Factsheet button is clicked successfully")

    }

    enter_factsheet_title(media_name) {

        cy.get('#factsheet-edit-modal > .modal-dialog > .modal-content > .modal-body > #factsheet-form > :nth-child(5) > #title')
            .type(media_name)
            .log("Successfully entered the fact sheet title")

    }

    enter_factsheet_contents(factsheet_content) {

        cy.xpath("//iframe[@class='cke_wysiwyg_frame cke_reset']").then($element => {
            const $body = $element.contents().find('body')
            let stripe = cy.wrap($body)
            stripe.find('p').eq(0).click().type(factsheet_content)
                .log("Successfully entered the fact sheet content")
        })

    }


    save_factsheet_sheet() {

        cy.xpath("//button[@title='Save Sheet']")
            .click()
            .log("Successfully saved the fact sheet ")

        cy.wait(1000)
    }

    click_embed_button() {

        cy.xpath("//*[@id='embedded-video-btn']", {timeout: 50000})
            .click()
            .log("Successfully clicked the embedded button ")

    }

    enter_embed_title(media_name) {

        cy.xpath("//form[@id='embedded-video-form']//input[@id='title']", {timeout: 50000})
            .type(media_name)
            .log("Successfully enter the embedded title ")

    }

    enter_embed_code(embed_code) {

        cy.xpath("//*[@id='html']")
            .type(embed_code)
            .log("Successfully entered the embedded code ")


    }

    save_embed_video() {

        cy.xpath("//*[@title='Save Embedded Video']")
            .click()
            .log("Successfully saved the embedded file ")

    }

}

export default MediaPage;