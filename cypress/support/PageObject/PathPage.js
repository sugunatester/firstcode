class PathPage {

    //To click the path menu from side menu
    click_path_side_menu() {

        cy.xpath("//div[@class='nav-wrapper']//a[@href='/enterprise/paths/list']//span[text()='Paths']", {timeout: 30000}).click()
            .log("Successfully clicked the paths menu from side menu");

    }

    //To click the new path button at path page
    click_new_path_button() {

        cy.xpath("//a[@id='new-path-btn'][normalize-space(text()='New Path')]")
            .click().log("Successfully clicked the 'new path' button at path page")

    }

    //To enter path name at path creation page
    enter_path_name(pathdata) {
        let path_name = pathdata.path_name;

        cy.xpath("//div[@class='bloc-body']//div//div//label[contains(text(),'Name')]//..//input[@id='path_name']")
            .type(path_name).log("Successfully entered the path name at path creation page");

    }

    //To click the save button at the path creation page
    click_save_button_at_path_creation_page() {

        cy.xpath("//button[@type='submit'][@form='path-form']").click()
            .log("Successfully clicked the save button at path creation page");

    }

    //To open the created path
    open_created_path(pathdata) {
        let path_name = pathdata.path_name;

        cy.xpath("//a[contains(@class,'cell path-name')][contains(normalize-space(.),'" + path_name + "')]", {timeout: 30000})
            .click()
            .log("Successfully clicked the created path");

    }

    //To click the modules tab at path overview page
    click_modules_tab() {

        cy.xpath("//div[@class='main-content path-view-page']//div//ul//li//a[text()='Modules']")
            .click()
            .log("Successfully clicked the modules tab");

    }


    //To filter created base on modules page
    filter_created_base_on_element_page(basedatas) {
        this.click_topic_filter_field()
        this.select_all_topic_option()
        this.click_topic_filter_field()
        this.select_base_on_topic_filter_field(basedatas)
    }

    //To click the topic filter
    click_topic_filter_field() {
        cy.xpath("//*[@id='topic-filter-button']").click();

    }

    //To select all options
    select_all_topic_option() {
        cy.xpath("//div[contains(@id,'ui-id')][contains(text(),'All topics')]").click();

    }

    //To select the base on the topic filter
    select_base_on_topic_filter_field(basedatas) {
        cy.xpath("//ul[@id='topic-filter-menu']//div[text()='" + basedatas["base_name"] + "']").click();

    }

    //To add the module to the path
    select_module_to_path(module_name) {

        cy.xpath("//div[contains(@class,'module-line')]//div[contains(text(),'" + module_name + "')]")
            .trigger("mousedown", {which: 1})
        cy.get('#selected-modules')
            .trigger("mousemove")
            .trigger("mouseup", {force: true})
        cy.log("Module is added to the path");

    }

    //To click save button at add module page of path
    click_save_button_at_add_module_page() {
        cy.xpath("//a[@id='save-and-quit'][text()='Save']").click()
            .log("Successfully clicked the save button at add module page of path");

    }

    //To verify success message after added module
    verify_success_message_after_added_module_into_path(module_name) {
        cy.xpath("//div[@class='xq-flashes xq-m collapse']//ul//li[normalize-space(text()='Your path " + module_name + " has been successfully saved')]")
            .should('be.visible');
    }
}

export default PathPage