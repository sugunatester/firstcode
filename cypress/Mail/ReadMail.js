class ReadMail {

    get_register_link_from_user_mail(mail_sub, randno) {
        cy.wait(60000)
        cy.task('gmailchecks', {
            options: {
                subject: mail_sub["mail_subject"]+randno["rand_no"],
                include_body: true,
            }
        })
        cy.log('my mail subjecs was', mail_sub["mail_subject"]+randno["rand_no"])

            .then(emails => {
                assert.isAtLeast(
                    emails.length,
                    1,
                    "Expected to find at least one email, but none were found!"
                );
                const body = emails[0].body.html;
                let href = body.match(/href="([^"]*)/)[1];
                cy.log('href value is - ', href);
                cy.visit(href);
            });
    }


    get_password_from_user_mail(mail_sub) {
        cy.task('gmailchecks', {
            options: {
                subject: mail_sub["activate_user_subject"],
                include_body: true,
            }
        })

            .then(emails => {
                assert.isAtLeast(
                    emails.length,
                    1,
                    "Expected to find at least one email, but none were found!"
                );
                const body = emails[0].body.html;
                const convert_html_to_text = body.replace(/(<([^>]+)>)/g, "")
                cy.log('my text from the html tag is--', convert_html_to_text)
                const my_password = convert_html_to_text.split(".com")[1]
                const my_password1 = my_password.split("Please use")[0]
                const my_password2 = my_password1.split("Password :")[1]
                const remove_space_on_password_from_mail = my_password2.replace(/ /g, "").trim()
                let count = remove_space_on_password_from_mail.length
                cy.log('password length is--',count)
                cy.log('we got the password',remove_space_on_password_from_mail)
                Cypress.env('my_got_password_from_mail', remove_space_on_password_from_mail)
            })
    }



}
export default ReadMail;

