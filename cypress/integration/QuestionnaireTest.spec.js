import HomeAction from "../Action/HomeAction.js";
import BaseAction from "../Action/BaseAction.js";
import UserAction from "../Action/UserAction.js";
import MediaAction from "../Action/MediaAction.js";
import SettingAction from "../Action/SettingAction.js";
import QuestionnaireAction from "../Action/QuestionnaireAction.js";
import SurveysAction from "../Action/SurveysAction";
import ReadMail from "../Mail/ReadMail";

describe('Questionnaire', () => {

    const homeaction = new HomeAction;
    const baseaction = new BaseAction;
    const useraction = new UserAction;
    const mediaaction = new MediaAction;
    const settingaction = new SettingAction;
    const questionnaireaction = new QuestionnaireAction;
    const surveysaction = new SurveysAction();
    const readmail = new ReadMail();

    let basedatas;
    let questiondatas;
    let questionnairedatas;
    let admindatas;
    let usersdatas;
    let without_mail_user;
    let settingdata;
    let link;
    let mediadata;
    let mail_sub;
    let contactuser;
    let public_ques;
    let questiondatas2;
    let group;
    let simple_surveydata;


    beforeEach(() => {

        cy.viewport(1280, 720);

        cy.exec('npm cache clear --force')


        cy.fixture("base_questionnaires").then((data) => {
            basedatas = data.bases.base_1;
        })

        cy.fixture("questions").then((data) => {
            questiondatas = data.all_type;
            questiondatas2 = data.single_type;
        })

        cy.fixture("base_questionnaires").then((data) => {
            questionnairedatas = data.questionnaires.questionnaire_1;
            public_ques = data.questionnaires.questionnaire_1.public;
        })

        cy.fixture("users").then((data) => {
            admindatas = data.tets_beta_admin;
            // admindatas = data.tets_www_admin;
            // admindatas = data.priyanga;
            usersdatas = data.users.tets2020;
            without_mail_user = data.users.Inactive_mail_user;
            contactuser = data.contact_user;
            group = data.group;

        })

        cy.fixture("common_data").then((data) => {
            link = data.links;
            settingdata = data
            mail_sub = data.mail_subjects

        })

        cy.fixture("media_modules").then((data) => {
            mediadata = data.medias.media1.items;
        })

        cy.fixture('form').then((data) => {
            simple_surveydata = data.forms.simple_form;
        })

        Cypress.on('uncaught:exception', (err, runnable) => {
            return false
        })

    })

    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    })

    it('1. Create static questionnaire & play trial by admin', () => {
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        settingaction.enable_all_user_type_option_and_their_side_menu()
        baseaction.create_base_with_single_domain_and_verify(basedatas);
        baseaction.create_question_on_created_base(basedatas, questiondatas);
        questionnaireaction.create_static_type_questionnaire(basedatas, questionnairedatas)
        questionnaireaction.select_all_question_on_created_static_type_questionnaire(basedatas, questionnairedatas)
        questionnaireaction.play_static_trial_questionnaire(basedatas, questiondatas, questionnairedatas, settingdata)
        homeaction.delete_admin_account_if_signed(link)
    })


    it('2. Create static questionnaire and invite the active user & verify the result', () => {
        cy.visit('/')
        useraction.rand()
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        settingaction.enable_all_user_type_option_and_their_side_menu()
        baseaction.create_base_with_single_domain_and_verify(basedatas);
        baseaction.create_question_on_created_base(basedatas, questiondatas);
        questionnaireaction.create_static_type_questionnaire(basedatas, questionnairedatas)
        questionnaireaction.select_all_question_on_created_static_type_questionnaire(basedatas, questionnairedatas)
        questionnaireaction.play_static_trial_questionnaire(basedatas, questiondatas, questionnairedatas, settingdata)
        useraction.create_one_user_by_admin(without_mail_user)
        useraction.activate_the_created_user_by_admin(without_mail_user)
        homeaction.admin_user_sign_in(admindatas);
        settingaction.get_password_from_preview_message_on_email_and_log_tab(without_mail_user, mail_sub)
        cy.fixture('user_reg_link').then(function (pwd) {
            let user_password = pwd.activated_user_password
            cy.log('my_stored_user_reg_link_is', user_password)
            useraction.login_by_user_after_getting_password(without_mail_user, settingdata, user_password)
            homeaction.admin_user_sign_in(admindatas)
            let randno
            cy.fixture('data').then(function (datas) {
                randno = datas
                cy.log(randno)
                questionnaireaction.invite_questionnaire_to_the_user_by_clicking_select_user_button(basedatas, questionnairedatas, without_mail_user, randno, mail_sub)
                useraction.login_by_user_after_getting_password_without_logout(without_mail_user, user_password)
                useraction.click_questionnaire_invitation_from_userdashboard_page(questionnairedatas)
                questionnaireaction.select_answer_on_playing_static_questionnaire_by_active_user(questiondatas)
                homeaction.admin_user_sign_in(admindatas)
                questionnaireaction.verify_questionnaire_result_for_active_user(basedatas, questionnairedatas, without_mail_user)
                homeaction.delete_admin_account_if_signed(link)
            })
        })
    })


    it('3. Create static questionnaire and invite the contact user & verify the result', () => {
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        settingaction.enable_all_user_type_option_and_their_side_menu()
        baseaction.create_base_with_single_domain_and_verify(basedatas);
        baseaction.create_question_on_created_base(basedatas, questiondatas);
        questionnaireaction.create_static_type_questionnaire(basedatas, questionnairedatas)
        questionnaireaction.select_all_question_on_created_static_type_questionnaire(basedatas, questionnairedatas)
        questionnaireaction.play_static_trial_questionnaire(basedatas, questiondatas, questionnairedatas)
        homeaction.admin_user_sign_in(admindatas)
        useraction.rand()
        let randno
        cy.fixture('data').then(function (datas) {
            randno = datas
            cy.log(randno)
            questionnaireaction.invite_questionnaire_to_the_contact_user(basedatas, questionnairedatas, contactuser, randno, mail_sub)
            homeaction.admin_user_sign_in(admindatas)
            settingaction.get_contact_user_of_questionnaire_play_link_on_email_and_log_tab(contactuser, mail_sub, randno)
        })
        cy.fixture('user_reg_link').then(function (reg_link) {
            let user_reg_link = reg_link.user_reg_link
            cy.log('my_stored_user_reg_link_is', user_reg_link)
            cy.visit(user_reg_link)
            questionnaireaction.select_answer_on_playing_static_questionnaire_by_contact_user(questiondatas)
            homeaction.admin_user_sign_in(admindatas)
            questionnaireaction.verify_questionnaire_result_for_active_user(basedatas, questionnairedatas, contactuser)
            homeaction.delete_admin_account_if_signed(link)
        })
    })


    it('4. Create dynamic questionnaire & play trial by admin', () => {
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        settingaction.enable_all_user_type_option_and_their_side_menu()
        baseaction.create_base_with_single_domain_and_verify(basedatas);
        baseaction.create_question_on_created_base(basedatas, questiondatas);
        questionnaireaction.create_dynamic_type_questionnaire(basedatas, questionnairedatas)
        questionnaireaction.enter_question_count_on_created_dynamic_type_questionnaire(basedatas, questionnairedatas)
        homeaction.admin_user_sign_in(admindatas)
        questionnaireaction.play_dynamic_trial_questionnaire(basedatas, questiondatas, questionnairedatas, settingdata)
        homeaction.delete_admin_account_if_signed(link)

    })

    it('5. Create public questionnaire with return link & play by guest user', () => {
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        settingaction.enable_all_user_type_option_and_their_side_menu()
        baseaction.create_base_with_single_domain_and_verify(basedatas);
        baseaction.create_question_on_created_base(basedatas, questiondatas);
        questionnaireaction.create_dynamic_type_questionnaire(basedatas, questionnairedatas)
        questionnaireaction.enter_question_count_on_created_dynamic_type_questionnaire(basedatas, questionnairedatas)
        questionnaireaction.create_public_questionnaire(basedatas, questionnairedatas, public_ques, link)
        questionnaireaction.copy_the_public_link_on_quetionnaire_visibility_page(basedatas, questionnairedatas)
        cy.fixture('user_reg_link').then(function (publiclink) {
            let publiclinkques = publiclink.public_link
            cy.log('my_stored_user_reg_link_is', publiclinkques)
            cy.visit(publiclinkques)
            questionnaireaction.playing_public_dynamic_questionnaire_by_guest_user(questiondatas, questionnairedatas, settingdata)
            homeaction.admin_user_sign_in(admindatas)
            questionnaireaction.verify_public_questionnaire_result_for_guest_user(basedatas, questionnairedatas)
        })
        homeaction.delete_admin_account_if_signed(link)

    })

    it('6. Create visibility questionnaire & play visibility by active user', () => {
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        settingaction.enable_all_user_type_option_and_their_side_menu()
        useraction.create_one_user_by_admin(without_mail_user)
        useraction.activate_the_created_user_by_admin(without_mail_user)
        homeaction.admin_user_sign_in(admindatas)
        baseaction.create_base_with_single_domain_and_verify(basedatas);
        baseaction.create_question_on_created_base(basedatas, questiondatas);
        questionnaireaction.create_dynamic_type_questionnaire(basedatas, questionnairedatas)
        questionnaireaction.enter_question_count_on_created_dynamic_type_questionnaire(basedatas, questionnairedatas)
        useraction.create_group(group)
        useraction.assign_user_on_created_group(group, without_mail_user)
        questionnaireaction.create_visbility_questionnaire(basedatas, questionnairedatas, group)
        settingaction.get_password_from_preview_message_on_email_and_log_tab(without_mail_user, mail_sub)
        cy.fixture('user_reg_link').then(function (pwd) {
            let user_password = pwd.activated_user_password
            cy.log('my_stored_user_reg_link_is', user_password)
            useraction.login_by_user_after_getting_password(without_mail_user, settingdata, user_password)
            useraction.login_by_user_after_getting_password_without_logout(without_mail_user, user_password)
            useraction.click_free_to_play_questionnaire_from_userdashboard_page(questionnairedatas)
            questionnaireaction.select_answer_on_playing_dynamic_questionnaire(questiondatas, questionnairedatas)
            homeaction.admin_user_sign_in(admindatas)
            questionnaireaction.verify_result_for_active_user_played_visibility_questionnaire(basedatas, questionnairedatas, without_mail_user)
            homeaction.delete_admin_account_if_signed(link)
        })
    })


    it('7. Create questionnaire with survey & invite to the contact user', () => {
        cy.visit('/')
        useraction.rand()
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        settingaction.enable_all_user_type_option_and_their_side_menu()
        surveysaction.create_surveys(simple_surveydata)
        surveysaction.create_all_fields(simple_surveydata)
        baseaction.create_base_with_single_domain_and_verify(basedatas)
        baseaction.create_question_on_created_base(basedatas, questiondatas2)
        questionnaireaction.create_dynamic_type_questionnaire(basedatas, questionnairedatas)
        questionnaireaction.enter_question_count_on_created_dynamic_type_questionnaire(basedatas, questionnairedatas)
        questionnaireaction.select_survey_form_on_created_questionnaire(basedatas, questionnairedatas, simple_surveydata)
        questionnaireaction.trialplay_questionnaire_with_survey_form(basedatas, simple_surveydata, questionnairedatas, questiondatas)
        homeaction.admin_user_sign_in(admindatas)
        let randno
        cy.fixture('data').then(function (datas) {
            randno = datas
            cy.log(randno)
            questionnaireaction.invite_questionnaire_to_the_contact_user(basedatas, questionnairedatas, contactuser, mail_sub)
            settingaction.get_contact_user_of_questionnaire_play_link_on_email_and_log_tab(contactuser, mail_sub, randno)
        })
        cy.fixture('user_reg_link').then(function (queslink) {
            let surwithqueslink = queslink.user_reg_link
            cy.log('my_stored_user_reg_link_is', surwithqueslink)
            cy.visit(surwithqueslink)
            questionnaireaction.contact_user_play_questionnaire_with_survey_form(simple_surveydata, questionnairedatas, questiondatas)
        })
        homeaction.admin_user_sign_in(admindatas)
        questionnaireaction.verify_questionnaire_result_for_contact_user(basedatas, questionnairedatas, contactuser)
        homeaction.delete_admin_account_if_signed(link)
    })

    it('8. Create static questionnaire and invite the contact user and play from contact user mail', () => {
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        settingaction.enable_all_user_type_option_and_their_side_menu()
        baseaction.create_base_with_single_domain_and_verify(basedatas);
        baseaction.create_question_on_created_base(basedatas, questiondatas);
        questionnaireaction.create_static_type_questionnaire(basedatas, questionnairedatas)
        questionnaireaction.select_all_question_on_created_static_type_questionnaire(basedatas, questionnairedatas)
        useraction.rand()
        let randno
        cy.fixture('data').then(function (datas) {
            randno = datas
            cy.log(randno)
            questionnaireaction.invite_questionnaire_to_the_contact_user(basedatas, questionnairedatas, contactuser, randno, mail_sub)
        })
    })

    it('8 continue: Play questionnaire from the user mail account', () => {
        let randno
        cy.fixture('data').then(function (datas) {
            randno = datas
            cy.log(randno)
            readmail.get_register_link_from_user_mail(mail_sub, randno)
            questionnaireaction.select_answer_on_static_questionnaire_from_mail_account(questiondatas, settingdata)
        })
    })

    it('8 continue: Verify questionnaire result for Played user mail account', () => {
            cy.visit('/')
            homeaction.admin_user_sign_in(admindatas)
            questionnaireaction.verify_questionnaire_result_for_contact_user(basedatas, questionnairedatas, contactuser)
    })



})