import HomeAction from "../Action/HomeAction.js";
import SettingAction from "../Action/SettingAction.js";
import BaseAction from "../Action/BaseAction.js";
import QuestionnaireAction from "../Action/QuestionnaireAction.js";
import ModulesAction from "../Action/ModulesAction";
import UserAction from "../Action/UserAction";

describe('Modules',()=>{

    const homeaction = new HomeAction();
    const settingaction = new SettingAction();
    const baseaction = new BaseAction();
    const questionnaireaction = new QuestionnaireAction();
    const modulesaction = new ModulesAction();
    const useraction = new UserAction();

    let admindatas;
    let link;
    let settingdata;
    let basedatas;
    let questiondatas;
    let questiondatas2;
    let questionnairedatas;
    let modulesdata;
    let mediadata;
    let three_qunn;
    let modules_qunn;
    let without_mail_user;
    let mail_sub;

    beforeEach(()=>{
        cy.visit('/')
        cy.viewport(1280, 720);

        cy.fixture('base_questionnaires').then((data)=>{
            basedatas = data.bases.base_1;
            questionnairedatas = data.questionnaires.questionnaire_1;
            three_qunn = data.questionnaires.three_questionnaire;
        })

        cy.fixture("users").then((data) => {
            admindatas = data.tets_beta_admin;
            // admindatas = data.tets_www_admin;
            // admindatas = data.priyanga;
            without_mail_user = data.users.Inactive_mail_user;
        })

        cy.fixture("common_data").then((data) => {
            link = data.links;
            settingdata = data
            mail_sub = data.mail_subjects
        })

        cy.fixture("questions").then((data) => {
            questiondatas = data.all_type;
            questiondatas2 = data.single_type;

        })

        cy.fixture('media_modules').then((data)=>{
            modulesdata = data.module.module_list;
            modules_qunn = data.module.questionnaire_list;
            mediadata = data.medias.media1.items;
        })

    })

    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    })

    it('1. Create module with steps & play trial',()=>{
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        baseaction.create_base_with_single_domain_and_verify(basedatas);
        baseaction.create_media_on_base(basedatas,mediadata)
        baseaction.create_question_on_created_base(basedatas, questiondatas);
        questionnaireaction.create_static_type_questionnaire(basedatas, questionnairedatas);
        questionnaireaction.select_all_question_on_created_static_type_questionnaire(basedatas, questionnairedatas);
        modulesaction.create_module(modulesdata);
        modulesaction.add_element_into_created_module(basedatas, modulesdata);
        modulesaction.click_trial_play_button_on_created_module(modulesdata)
        modulesaction.answer_on_module_player_page(modulesdata, questiondatas)
        homeaction.delete_admin_account_if_signed(link)
    })

    it('2. Create module with 3 usage of questionnaire steps & invite and play user',()=>{
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        baseaction.create_base_with_single_domain_and_verify(basedatas);
        baseaction.create_media_on_base(basedatas,mediadata)
        baseaction.create_question_on_created_base(basedatas, questiondatas);
        questionnaireaction.create_three_dynamic_questionnaire(basedatas, three_qunn);
        questionnaireaction.enter_question_count_on_created_three_dynamic_type_questionnaire(basedatas, three_qunn);
        modulesaction.create_module(modules_qunn);
        modulesaction.add_element_into_created_module(basedatas, modules_qunn);
        modulesaction.set_questionnaire_usage(basedatas, modules_qunn)
        // modulesaction.click_trial_play_button_on_created_module(modules_qunn)
        // modulesaction.answer_on_module_player_page(modules_qunn, questiondatas)
        useraction.create_one_user_by_admin(without_mail_user)
        useraction.activate_the_created_user_by_admin(without_mail_user)
        homeaction.admin_user_sign_in(admindatas);
        settingaction.get_password_from_preview_message_on_email_and_log_tab(without_mail_user, mail_sub)
        let randno
        cy.fixture('data').then(function (datas) {
            randno = datas
            cy.log(randno)
            modulesaction.invite_module_to_active_user(basedatas, questionnairedatas, without_mail_user, randno, mail_sub)
        })
        homeaction.delete_admin_account_if_signed(link)
    })


})