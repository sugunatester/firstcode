// import UserAction from "../Action/UserAction";
// import MediaAction from "../Action/MediaAction.js";
// import SettingAction from "../Action/SettingAction.js";
// import HomeAction from "../Action/HomeAction.js";
// import BaseAction from "../Action/BaseAction";

const BaseAction = require('../Action/BaseAction')
const UserAction = require('../Action/UserAction')
const HomeAction = require('../Action/HomeAction')
const SettingAction = require('../Action/SettingAction')
const MediaAction = require('../Action/MediaAction')

// import * as UserAction from '../Action/UserAction'
// import * as MediaAction from '../Action/MediaAction'
// import * as HomeAction from '../Action/HomeAction'
// import * as SettingAction from "../Action/SettingAction.js";
// import * as BaseAction from "../Action/BaseAction";

describe('Media', () => {
    const homeaction = new HomeAction;
    const baseaction = new BaseAction;
    const useraction = new UserAction;
    const mediaaction = new MediaAction;
    const settingaction = new SettingAction;


    let basedatas;
    let questiondatas;
    let admindatas;
    let usersdatas;
    let settingdata;
    let link;
    let mediadata;

    beforeEach(() => {
        cy.viewport(1280, 720);
        cy.exec('npm cache clear --force')

        cy.fixture("base_questionnaires").then((data) => {
            basedatas = data.bases.base_1;
        })
        cy.fixture("questions").then((data) => {
            questiondatas = data.all_type;
        })
        cy.fixture("users").then((data) => {
            admindatas = data.tets_beta_admin;
            // admindatas = data.tets_www_admin;
        })
        cy.fixture("users").then((data) => {
            usersdatas = data.users.tets2020;
        })
        cy.fixture("common_data").then((data) => {
            link = data.links;
            settingdata = data
        })
        cy.fixture("media_modules").then((data) => {
            mediadata = data.medias.media1.items;
        })
    })

    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    })

    it('1. Create media on media page', () => {
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        mediaaction.create_media_on_mediapage(mediadata)
        homeaction.delete_admin_account_if_signed(link)

    })
})

