import UserAction from '../Action/UserAction.js';
import HomeAction from '../Action/HomeAction.js';
import ReadMail from '../Mail/ReadMail';
import SettingAction from "../Action/SettingAction.js";

describe('Users', () => {
    const useraction = new UserAction;
    const homeaction = new HomeAction;
    const readmail = new ReadMail;
    const settingaction = new SettingAction()

    let admindatas;
    let usersdatas;
    let link;
    let settingdata;
    let mail_sub;
    let without_mail_user;

    beforeEach(() => {

        cy.viewport(1580, 720);

        cy.exec('npm cache clear --force')

        cy.fixture("users").then((data) => {
            // admindatas = data.tets_beta_admin;
            admindatas = data.tets_www_admin;
            usersdatas = data.users.tets2020;
            without_mail_user = data.users.Inactive_mail_user;

        })

        cy.fixture("common_data").then((data) => {
            link = data.links;
            settingdata = data
            mail_sub = data.mail_subjects
        })

        cy.writeFile('cypress/fixtures/user_reg_link.json', '{"user_reg_link":""}')

    })

    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    })


    it.only('1. Admin Invite User through "Emails and Mobiles" action', () => {
        cy.visit('/')
        useraction.rand()
        let randno
        cy.fixture('data').then(function (datas) {
            randno = datas
            cy.log(randno)
            homeaction.verify_admin_exist_and_deleted(admindatas, link);
            homeaction.register_as_administraion(admindatas);
            settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
            settingaction.enable_all_user_type_option_and_their_side_menu()
            useraction.invite_user_by_admin(without_mail_user, mail_sub, randno);
            homeaction.admin_user_sign_in(admindatas)
            settingaction.get_user_registration_link_on_email_and_log_tab(without_mail_user, mail_sub, randno)
        })
        cy.fixture('user_reg_link').then(function (reg_link) {
            let user_reg_link = reg_link.user_reg_link
            cy.log('my_stored_user_reg_link_is', user_reg_link)
            cy.visit(user_reg_link)
            useraction.register_by_user_self_after_getting_invitation_link(without_mail_user)
            useraction.login_by_user_on_experquiz_account(without_mail_user, settingdata)
            homeaction.delete_admin_account(admindatas, link)
        })
    })


    it('Gmail Concepts: Get invitation link from mail and register by user self', () => {
        let randno
        cy.fixture('data').then(function (datas) {
            randno = datas
            cy.log(randno)
            readmail.get_register_link_from_user_mail(mail_sub, randno)
        })
        useraction.register_by_user_self_after_getting_invitation_link(usersdatas)
    })


    it('Gmail Concepts: Login by user after registration', () => {
        cy.visit('/')
        useraction.login_by_user_on_experquiz_account(usersdatas)
        homeaction.delete_admin_account(admindatas, link)
    })

    it.only('2. Created one user by admin and activated & login by activated user', () => {
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        settingaction.enable_all_user_type_option_and_their_side_menu()
        useraction.create_one_user_by_admin(without_mail_user)
        useraction.activate_the_created_user_by_admin(without_mail_user)
        homeaction.admin_user_sign_in(admindatas);
        settingaction.get_password_from_preview_message_on_email_and_log_tab(without_mail_user, mail_sub)
        cy.fixture('user_reg_link').then(function (pwd) {
            let user_password = pwd.activated_user_password
            cy.log('my_stored_user_reg_link_is', user_password)
            useraction.login_by_user_after_getting_password(without_mail_user, settingdata, user_password)
            homeaction.delete_admin_account(admindatas, link)
        })
    })



    it('Gmail Concepts: Get password from activated user mail account', () => {
        readmail.get_password_from_user_mail(mail_sub)
    })



})