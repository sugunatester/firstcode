import CyclesAction from "../Action/CyclesAction";
import HomeAction from "../Action/HomeAction.js";
import SettingAction from "../Action/SettingAction.js";
import BaseAction from "../Action/BaseAction.js";
import QuestionnaireAction from "../Action/QuestionnaireAction.js";
import SurveysAction from "../Action/SurveysAction";
import UserAction from "../Action/UserAction";


describe('Cycles',()=>{

    const cyclesaction = new CyclesAction();
    const homeaction = new HomeAction();
    const settingaction = new SettingAction();
    const baseaction = new BaseAction();
    const questionnaireaction = new QuestionnaireAction();
    const surveysaction = new SurveysAction();
    const useraction = new UserAction()



    let cycledata;
    let admindatas;
    let link;
    let settingdata;
    let basedatas;
    let questiondatas;
    let questiondatas2;
    let questionnairedatas;
    let cy_ques;
    let simple_surveydata
    let long_surveydata
    let mail_sub
    let contactuser
    let group
    let usersdatas
    let without_mail_user


    beforeEach(()=>{
        cy.viewport(1280, 720);

        cy.fixture('base_questionnaires').then((data)=>{
            basedatas = data.bases.base_1;
            cycledata= data.cycle.cycle_2;
            questionnairedatas = data.questionnaires.questionnaire_1;
            cy_ques = data.questionnaires.two_questionnaire;
        })

        cy.fixture("users").then((data) => {
            // admindatas = data.priyanga;
            // admindatas = data.tets_www_admin;
            admindatas = data.tets_beta_admin;
            usersdatas = data.users.tets2020;
            without_mail_user = data.users.Inactive_mail_user;
            contactuser = data.contact_user;
            group = data.group;
        })

        cy.fixture("common_data").then((data) => {
            link = data.links;
            settingdata = data
        })

        cy.fixture("questions").then((data) => {
            questiondatas = data.all_type;
            questiondatas2 = data.single_type;
        })

        cy.fixture('form').then((data) => {
            simple_surveydata = data.forms.simple_form;
            long_surveydata = data.forms.form_1;
        })

        cy.fixture("common_data").then((data) => {
            link = data.links;
            settingdata = data
            mail_sub = data.mail_subjects
        })
    })

    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    })

    it('1. Create cycles with steps of questionnaire and survey & invite to the contact user',()=>{
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        baseaction.create_base_with_single_domain_and_verify(basedatas);
        baseaction.create_question_on_created_base(basedatas, questiondatas);
        questionnaireaction.create_two_static_type_questionnaire(basedatas, cy_ques);
        questionnaireaction.select_all_question_on_created_two_static_type_questionnaire(basedatas, cy_ques);
        useraction.rand()
        surveysaction.create_surveys(long_surveydata)
        surveysaction.create_all_fields(long_surveydata)
        cyclesaction.create_cycles(cycledata);
        cyclesaction.create_steps_into_created_cycles(basedatas,cycledata);
        cy.fixture('data').then(function (datas) {
            let randno = datas
            cy.log(randno)
            cyclesaction.invite_cycle_with_3_messages_to_contact_user(cycledata, contactuser, randno, mail_sub,)
            homeaction.admin_user_sign_in(admindatas)
            settingaction.get_contact_user_of_cycle_event_play_link_on_email_and_log_tab(contactuser, mail_sub, randno)
    })
        cy.fixture('user_reg_link').then(function (ques) {
            let questionnaire = ques.cycle_questionnaire
            let survey = ques.cycle_survey
            cy.log(questionnaire)
            cy.log(survey)
            cy.visit(questionnaire)
            useraction.verify_GDPR_for_contact_user_and_accept_GDPR_message_while_playing_test(settingdata)
            questionnaireaction.select_answer_on_playing_static_questionnaire_by_contact_user(questiondatas)
            cy.visit(survey)
            surveysaction.play_survey_by_contact_user(long_surveydata, link)
        })
        homeaction.admin_user_sign_in(admindatas)
        cyclesaction.verify_contact_user_cycle_result(cycledata, contactuser)
        homeaction.delete_admin_account_if_signed(link)
    })


    it('2. Delete ongoing cycle and verify it on user account',()=>{
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        baseaction.create_base_with_single_domain_and_verify(basedatas);
        baseaction.create_question_on_created_base(basedatas, questiondatas);
        questionnaireaction.create_two_static_type_questionnaire(basedatas, cy_ques);
        questionnaireaction.select_all_question_on_created_two_static_type_questionnaire(basedatas, cy_ques);
        useraction.rand()
        surveysaction.create_surveys(long_surveydata)
        surveysaction.create_all_fields(long_surveydata)
        cyclesaction.create_cycles(cycledata);
        cyclesaction.create_steps_into_created_cycles(basedatas,cycledata);
        useraction.create_one_user_by_admin(without_mail_user)
        useraction.activate_the_created_user_by_admin(without_mail_user)
        homeaction.admin_user_sign_in(admindatas);
        settingaction.get_password_from_preview_message_on_email_and_log_tab(without_mail_user, mail_sub)
        homeaction.admin_user_sign_in(admindatas);
        cy.fixture('data').then(function (datas) {
            let randno = datas
            cy.log(randno)
            cyclesaction.invite_cycle_with_3_messages_to_active_user(cycledata, without_mail_user, randno, mail_sub,)
        })
        cy.fixture('user_reg_link').then(function (pwd) {
            let user_password = pwd.activated_user_password
            cy.log('my_stored_user_reg_link_is', user_password)
            useraction.login_by_user_after_getting_password(without_mail_user, settingdata, user_password)
            useraction.login_by_user_after_getting_password_without_logout(without_mail_user, user_password)
            useraction.verify_cycle_event_is_exist_in_userdashboard_page(cycledata)
            homeaction.admin_user_sign_in(admindatas);
            cyclesaction.verify_delete_ongoing_cycle_on_user_page(cycledata, without_mail_user)
            useraction.login_by_user_after_getting_password_without_logout(without_mail_user, user_password)
            useraction.verify_deleted_ongoing_cycle_is_exist_on_user_dashboard_page(cycledata)
            homeaction.admin_user_sign_in(admindatas);
            homeaction.delete_admin_account_if_signed(link)
        })
    })

    // it('Login by activated user after getting the password & verify the deleted cycle test', () => {
    //     cy.visit('/')
    //     cy.log(Cypress.env('my_passwords'))
    //     useraction.login_by_user_after_getting_password(without_mail_user, settingdata)
    //     useraction.login_by_user_after_getting_password_without_logout(without_mail_user)
    //     useraction.verify_cycle_event_is_exist_in_userdashboard_page(cycledata)
    //     homeaction.admin_user_sign_in(admindatas);
    //     cyclesaction.verify_delete_ongoing_cycle_on_user_page(cycledata, without_mail_user)
    //     useraction.login_by_user_after_getting_password_without_logout(without_mail_user)
    //     useraction.verify_deleted_ongoing_cycle_is_exist_on_user_dashboard_page(cycledata)
    //     homeaction.admin_user_sign_in(admindatas);
    //     homeaction.delete_admin_account_if_signed(link)
    //
    // })

})