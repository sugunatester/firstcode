import HomeAction from "../Action/HomeAction.js";
import readMail from "../Mail/ReadMail";
import ReadMail from "../Mail/ReadMail";

describe('Home', function () {
    const homeaction = new HomeAction();
    const readmail = new ReadMail()

    let AdminDatas;
    let link;


    beforeEach(() => {
        cy.viewport(1280, 720);

        cy.exec('npm cache clear --force')

        cy.fixture("users").then((data) => {
            AdminDatas = data.tets_beta_admin;
            // AdminDatas = data.tets_www_admin;
            // AdminDatas = data.priyanga;
        })

        cy.fixture("common_data").then((data) => {
            link = data.links;
        })
    })


    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    })

    it("1. Register as administration", function () {
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(AdminDatas, link);
        homeaction.register_as_administraion(AdminDatas);
        homeaction.delete_admin_account_if_signed(link)
    })
    


})
