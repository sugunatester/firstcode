import SettingAction from "../Action/SettingAction.js";
import UserAction from "../Action/UserAction.js";
import HomeAction from "../Action/HomeAction.js";
import ReadMail from "../Mail/ReadMail";


describe('Setting', () => {

    const settingaction = new SettingAction()
    const useraction = new UserAction()
    const homeaction = new HomeAction()
    const readmail = new ReadMail()

    let common_data;
    let settingdata;
    let mail_sub;
    let admindatas;
    let usersdatas;
    let without_mail_user;


    beforeEach(() => {
        cy.viewport(1280, 720);

        cy.exec('npm cache clear --force')


        cy.fixture("common_data").then((data) => {
            common_data = data.links;
            settingdata = data
            mail_sub = data.mail_subjects
        })
        cy.fixture("users").then((data) => {
            admindatas = data.tets_beta_admin;
            // admindatas = data.tets_www_admin;
            usersdatas = data.users.tets2020;
            without_mail_user = data.users.Inactive_mail_user;
        })
        cy.writeFile('cypress/fixtures/user_reg_link.json', '{"user_reg_link":""}')
    })

    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    })

    it.only('Random number generated and stored in the json file', () => {
        useraction.rand()
    })


    it.only('1. Enable admin side menu, user side menu and verify their side menus', () => {
        cy.visit('/')
        let randno
        cy.fixture('data').then(function (datas) {
            randno = datas
            cy.log(randno)
            homeaction.verify_admin_exist_and_deleted(admindatas, common_data);
            homeaction.register_as_administraion(admindatas);
            settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, common_data)
            settingaction.enable_all_user_type_option_and_their_side_menu()
            useraction.invite_user_by_admin(without_mail_user, mail_sub, randno);
            homeaction.admin_user_sign_in(admindatas)
            settingaction.get_user_registration_link_on_email_and_log_tab(without_mail_user, mail_sub, randno)
        })
        cy.fixture('user_reg_link').then(function (reg_link) {
            let user_reg_link = reg_link.user_reg_link
            cy.log('my_stored_user_reg_link_is', user_reg_link)
            cy.visit(user_reg_link)
            useraction.register_by_user_self_after_getting_invitation_link(without_mail_user)
        })
        useraction.login_by_user_on_experquiz_account(without_mail_user, settingdata)
        useraction.login_by_user_on_experquiz_account_without_logout(without_mail_user, settingdata)
        useraction.verify_all_side_menu_is_exist_on_user_account()
        homeaction.delete_admin_account(admindatas, common_data)
    })

    it('Gmaill concepts: get invitations link from mail and register by user self', () => {
        let randno
        cy.fixture('data').then(function(datas){
            randno = datas
            cy.log(randno)
            readmail.get_register_link_from_user_mail(mail_sub, randno)
        })
        useraction.register_by_user_self_after_getting_invitation_link(usersdatas)
        useraction.login_by_user_on_experquiz_account_without_logout(usersdatas, settingdata)
        useraction.verify_all_side_menu_is_exist_on_user_account()
    })


})


