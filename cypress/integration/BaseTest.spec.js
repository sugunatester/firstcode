import HomeAction from "../Action/HomeAction.js";
import BaseAction from "../Action/BaseAction.js";
import UserAction from "../Action/UserAction.js";
import MediaAction from "../Action/MediaAction.js";
import SettingAction from "../Action/SettingAction.js";
import QuestionnaireAction from "../Action/QuestionnaireAction.js";
import ReadMail from "../Mail/ReadMail";
import BaseModuleAction from "../Action/BaseModuleAction.js"

describe('Bases', () => {

    const homeaction = new HomeAction;
    const baseaction = new BaseAction;
    const useraction = new UserAction;
    const mediaaction = new MediaAction;
    const settingaction = new SettingAction;
    const questionnaireaction = new QuestionnaireAction;
    const readmail = new ReadMail()
    const baseModuleaction = new BaseModuleAction();

    let basedatas;
    let basedatas1;
    let questiondatas;
    let questionnairedatas;
    let admindatas;
    let usersdatas;
    let link;
    let mail_sub;
    let mediadata;
    let settingdata;
    let three_domain_base;
    let questiondatas2
    let without_mail_user;
    let long_surveydata
    let conditiondata
    let simple_surveydata
    let randno
    let training_ques
    let baseeditdatas
    let modulesdata
    let training
    let randomNumber = 0


    beforeEach(() => {

        cy.viewport(1280, 720);

        cy.exec('npm cache clear --force')

        cy.fixture("base_questionnaires").then((data) => {
            basedatas = data.bases.base_1;
            basedatas1 = data.bases.base_0;
            three_domain_base = data.bases.base_3;
            baseeditdatas = data.bases.base_1.edit_base_1;
            training = data.training.training_1;
            training_ques = data.questionnaires.training_questionnaire_1;
        })

        cy.fixture("questions").then((data) => {
            questiondatas = data.all_type;
            questiondatas2 = data.single_type;
        })

        cy.fixture("base_questionnaires").then((data) => {
            questionnairedatas = data.questionnaires.questionnaire_1;
        })


        cy.fixture("users").then((data) => {
            // admindatas = data.tets_beta_admin;
            admindatas = data.tets_www_admin;
            usersdatas = data.users.tets2020;
            without_mail_user = data.users.Inactive_mail_user;
        })

        cy.fixture("common_data").then((data) => {
            link = data.links;
            settingdata = data
            mail_sub = data.mail_subjects
        })

        cy.fixture("media_modules").then((data) => {
            mediadata = data.medias.media1.items;
            modulesdata = data.module.module_list;
        })

        cy.fixture('form').then((data) => {
            simple_surveydata = data.forms.simple_form;
            long_surveydata = data.forms.form_1;
            conditiondata = data.forms.condition_form;
        })

        cy.writeFile('cypress/fixtures/user_reg_link.json', '{"user_reg_link":""}')

        cy.fixture('data').then(function (datas) {
            randno = datas
        })

        cy.fixture('user_reg_link').then(function (reg_link) {
            let user_reg_link = reg_link.user_reg_link
        })

    })

    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    })


    it('1. create base,question,questionnaire, media and create user', () => {
        useraction.rand()
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        settingaction.enable_all_user_type_option_and_their_side_menu()
        useraction.invite_user_by_admin(without_mail_user, mail_sub, randno)
        homeaction.admin_user_sign_in(admindatas)
        mediaaction.create_media_on_mediapage(mediadata)
        baseaction.create_base_with_single_domain_and_verify(basedatas);
        baseaction.create_question_on_created_base(basedatas, questiondatas);
        questionnaireaction.create_static_type_questionnaire(basedatas, questionnairedatas);
        questionnaireaction.select_all_question_on_created_static_type_questionnaire(basedatas, questionnairedatas)
        questionnaireaction.play_static_trial_questionnaire(basedatas, questiondatas, questionnairedatas, admindatas)
        settingaction.get_user_registration_link_on_email_and_log_tab(without_mail_user, mail_sub, randno)
        //Register link
        cy.visit(user_reg_link)
        useraction.register_by_user_self_after_getting_invitation_link(without_mail_user)
        useraction.login_by_user_on_experquiz_account(without_mail_user, settingdata)
        homeaction.admin_user_sign_in(admindatas)
        questionnaireaction.invite_questionnaire_to_the_user(basedatas, questionnairedatas, without_mail_user, mail_sub, randno)
        useraction.login_by_user_on_experquiz_account_without_logout(without_mail_user)
        useraction.click_questionnaire_invitation_from_userdashboard_page(questionnairedatas)
        questionnaireaction.select_answer_on_playing_static_questionnaire_by_active_user(questiondatas)

    })

    it('Gmaill concept: Get invitation link from mail and register by user self', () => {
        readmail.get_register_link_from_user_mail(mail_sub, randno)
        useraction.register_by_user_self_after_getting_invitation_link(usersdatas)
        useraction.login_by_user_on_experquiz_account(usersdatas)
    })

    it.skip('Creating base with questions to create dublicate base', () => {
        useraction.rand()
        randomNumber = Math.floor((Math.random() * 1000) + 1);
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link)
        homeaction.register_as_administraion(admindatas)
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        settingaction.enable_all_user_type_option_and_their_side_menu()
        homeaction.admin_user_sign_in(admindatas)
        baseaction.create_base_with_single_domain_and_verify(basedatas)
        baseaction.create_question_on_created_base(basedatas, questiondatas)
        baseModuleaction.EditQuestionsToChangeStatus(basedatas, questiondatas)
        baseModuleaction.createQuestionnaireWithDifferentMode(basedatas, long_surveydata, randomNumber)
        baseModuleaction.openOverviewTab(basedatas)
        baseModuleaction.getQuestionsCount()
        baseModuleaction.getQuestionnaireCount()
        baseModuleaction.createDuplicateBase(basedatas)
        baseModuleaction.verifyDuplicateBaseCreatedAsExpected(basedatas)
    })

    it.only('Transfer the created base to same enterprise and verify that it trasfered', () => {
        useraction.rand()
        cy.visit('/')
        randomNumber = Math.floor((Math.random() * 1000) + 1);
        homeaction.verify_admin_exist_and_deleted(admindatas, link)
        homeaction.register_as_administraion(admindatas)
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        settingaction.enable_all_user_type_option_and_their_side_menu()
        baseaction.logout()
        homeaction.admin_user_sign_in(admindatas)
        baseaction.create_base_with_single_domain_and_verify(basedatas)
        baseaction.create_question_on_created_base(basedatas, questiondatas)
        baseModuleaction.EditQuestionsToChangeStatus(basedatas, questiondatas)
        baseModuleaction.createQuestionnaireWithDifferentMode(basedatas, long_surveydata, randomNumber)
        baseModuleaction.openOverviewTab(basedatas)
        baseModuleaction.getQuestionsCount()
        baseModuleaction.getQuestionnaireCount()
        baseModuleaction.TransferBase(basedatas, randomNumber)
        baseModuleaction.CopyLinkOfTranferBase(randomNumber)
        baseModuleaction.transferByLinkAndVerify()
        baseModuleaction.openOverviewTab(basedatas)
        baseModuleaction.getQuestionsCount()
        baseModuleaction.getQuestionnaireCount()
        baseModuleaction.verifyTransferBaseCreatedAsExpected(basedatas)
    })

    it('Create Base with Tag', () => {
        cy.visit('/')
        homeaction.admin_user_sign_in(admindatas)
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        settingaction.enable_all_user_type_option_and_their_side_menu()
        baseaction.createBaseWithTag(basedatas, basedatas1)
        baseaction.create_question_on_created_base(basedatas, questiondatas);
        questionnaireaction.create_static_type_questionnaire(basedatas, questionnairedatas);
        baseaction.create_trainings_without_cycle(basedatas, training, admindatas)
        baseaction.create_media_on_mediapage(mediadata)
        baseaction.verifyToFilterwithTag(basedatas)
        baseaction.verifyTheArchivedBaseInQuestionnaire(basedatas, questionnairedatas)
        baseaction.verifyTheArchivedBaseInQuestions(basedatas, questiondatas)
        baseaction.verifyTheArchivedBaseInMedia(basedatas, mediadata)
        baseaction.verifyTheArchivedBaseInTraining(basedatas, training)
        baseaction.verifyTheActiveFilterInBase(basedatas)
    })

    it('Create training,Module,Media on Base', () => {
        cy.visit('/')
        homeaction.admin_user_sign_in(admindatas)
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        settingaction.enable_all_user_type_option_and_their_side_menu()
        baseaction.create_base_with_single_domain_and_verify(basedatas);
        baseaction.create_question_on_created_base(basedatas, questiondatas);
        questionnaireaction.create_two_static_type_questionnaire(basedatas, training_ques);
        questionnaireaction.select_all_question_on_created_two_static_type_questionnaire(basedatas, training_ques)

        baseaction.create_trainings_without_cycle(basedatas, training, admindatas)
        baseaction.drag_questionnaire_on_practice_section_in_training(basedatas, training, training_ques)
        baseaction.add_participants_into_created_training(basedatas, without_mail_user, training)
        baseaction.DragandDrop_present_user_to_reg_user_section_on_training_participant_page(basedatas, training, without_mail_user)
        baseaction.invite_selected_participant_on_training(basedatas, without_mail_user, training)
        homeaction.admin_user_sign_in(admindatas)
        useraction.login_by_user_on_experquiz_account(without_mail_user, settingdata)


    })

    it('Edit The Existing Base', () => {
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        settingaction.enable_all_user_type_option_and_their_side_menu()
        baseaction.create_base_with_single_domain_and_verify(basedatas);
        baseaction.verifyTheCreatedBaseDetails(basedatas, admindatas)
        useraction.create_one_user_by_admin(without_mail_user)
        useraction.activate_the_created_user_by_admin_without_logout(without_mail_user)
        baseaction.editBaseDetails(basedatas, baseeditdatas, without_mail_user)
        baseaction.verifyEditedBaseDetails(baseeditdatas, without_mail_user)
    })

    it('Delete the Base', () => {
        cy.visit('/')
        homeaction.admin_user_sign_in(admindatas)
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        settingaction.enable_all_user_type_option_and_their_side_menu()
        baseaction.create_base_with_single_domain_and_verify(basedatas);
        baseaction.create_question_on_created_base(basedatas, questiondatas);
        questionnaireaction.create_static_type_questionnaire(basedatas, questionnairedatas);
        baseaction.create_trainings_without_cycle(basedatas, training, admindatas)
        baseaction.create_media_on_mediapage(mediadata)
        baseaction.create_module(basedatas, modulesdata);
        baseaction.add_element_into_created_module(modulesdata);
        baseaction.deleteCreatedBase(basedatas)
        baseaction.verifyDeletedBaseInQuestionnaire(basedatas, questionnairedatas)
        baseaction.verifyDeletedBaseInQuestions(basedatas, questiondatas)
        baseaction.verifyDeletedBaseInMedia(basedatas, mediadata)
        baseaction.verifyDeletedBaseInTraining(basedatas, training)
        baseaction.verifyDeletedBaseInModules(basedatas)
    })

})





