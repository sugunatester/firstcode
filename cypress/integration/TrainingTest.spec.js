import HomeAction from "../Action/HomeAction.js";
import SettingAction from "../Action/SettingAction.js";
import BaseAction from "../Action/BaseAction.js";
import QuestionnaireAction from "../Action/QuestionnaireAction.js";
import CyclesAction from "../Action/CyclesAction";
import UserAction from "../Action/UserAction.js";
import TrainingAction from "../Action/TrainingAction";
import SurveysAction from "../Action/SurveysAction";


describe('Trainings',()=>{

    const homeaction = new HomeAction();
    const settingaction = new SettingAction();
    const baseaction = new BaseAction();
    const questionnaireaction = new QuestionnaireAction();
    const trainingaction = new TrainingAction();
    const cyclesaction = new CyclesAction();
    const useraction = new UserAction;
    const surveyaction = new SurveysAction();

    let admindatas;
    let link;
    let settingdata;
    let basedatas;
    let questiondatas;
    let questiondatas2;
    let questionnairedatas;
    let cycledata;
    let usersdatas;
    let without_mail_user;
    let training_ques
    let training
    let mail_sub
    let cy_ques
    let simple_surveydata
    let long_surveydata


    beforeEach(()=>{
        cy.viewport(1280, 720);

        cy.fixture('base_questionnaires').then((data)=>{
            basedatas = data.bases.base_1;
            questionnairedatas = data.questionnaires.questionnaire_1;
            cycledata= data.cycle.cycle_2;
            cy_ques = data.questionnaires.two_questionnaire;
            training_ques = data.questionnaires.training_questionnaire_1;
            training = data.training.training_1;
        })

        cy.fixture("users").then((data) => {
            // admindatas = data.tets_beta_admin;
            admindatas = data.tets_www_admin;
            usersdatas = data.users.tets2020;
            without_mail_user = data.users.Inactive_mail_user;
        })

        cy.fixture("common_data").then((data) => {
            link = data.links;
            settingdata = data
            mail_sub = data.mail_subjects

        })

        cy.fixture("questions").then((data) => {
            questiondatas = data.all_type;
            questiondatas2 = data.single_type;
        })

        cy.fixture('form').then((data)=> {
            simple_surveydata = data.forms.simple_form;
            long_surveydata = data.forms.form_1;
        })
    })

    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    })

    // it.skip('child window',()=> {
    //
    //     cy.visit("https://www.qafeast.com/demo")
    //     // cy.xpath("//label[text()='Hyperlink']").click()
    //     // cy.xpath("//a[text()='Desktop automation']").click()
    //     //     cy.window().then((win) => {
    //     //     cy.spy(win, 'open').as(`redirect`);
    //     // });
    //     // cy.get('@redirect').should('be.called')
    //     // cy.xpath("//button[@class='button-magnifying-glasss']").click()
    //
    //     cy.xpath("//label[text()='Hyperlink']").click()
    //     cy.get('[href="https://www.microfocus.com"]')
    //         .invoke('removeAttr', 'target').click()
    //     cy.xpath("//button[@class='button-magnifying-glasss']").click()
    //
    //     // cy.go('back');
    //
    // })


    it('1. Invite practice and evaluation training to participant',()=>{
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        baseaction.create_base_with_single_domain_and_verify(basedatas);
        baseaction.create_question_on_created_base(basedatas, questiondatas);
        questionnaireaction.create_two_static_type_questionnaire(basedatas, training_ques)
        questionnaireaction.select_all_question_on_created_two_static_type_questionnaire(basedatas, training_ques)
        useraction.create_one_user_by_admin(without_mail_user)
        useraction.activate_the_created_user_by_admin_without_logout(without_mail_user)
        trainingaction.create_trainings_without_cycle(basedatas,training,admindatas);
        trainingaction.drag_questionnaire_on_practice_section_in_training(basedatas,training, training_ques)
        trainingaction.add_participants_into_created_training(basedatas,without_mail_user,training)
        trainingaction.DragandDrop_present_user_to_reg_user_section_on_training_participant_page(basedatas, training, without_mail_user)
        trainingaction.invite_selected_participant_on_training(basedatas,without_mail_user,training)
        homeaction.admin_user_sign_in(admindatas)
        settingaction.get_password_from_preview_message_on_email_and_log_tab(without_mail_user, mail_sub)
        cy.fixture('user_reg_link').then(function (pwd) {
            let user_password = pwd.activated_user_password
            cy.log('my_stored_user_reg_link_is', user_password)
            useraction.login_by_user_after_getting_password(without_mail_user, settingdata, user_password)
            useraction.login_by_user_after_getting_password_without_logout(without_mail_user, settingdata, user_password)
            useraction.play_invited_practice_training_by_active_user(without_mail_user, training, training_ques)
            questionnaireaction.select_answer_on_training_questionnaire_test_of_static_questionnaire_by_active_user(questiondatas)
            homeaction.admin_user_sign_in(admindatas)
            trainingaction.verify_practice_questionnaire_test_result_on_training_result_page(basedatas, training, without_mail_user)
            trainingaction.invite_training_as_evaluation_to_active_user(basedatas, training, training_ques)
            useraction.login_by_user_after_getting_password_without_logout(without_mail_user, settingdata, user_password)
            useraction.play_invited_evaluation_training_by_active_user(without_mail_user, training, training_ques)
            questionnaireaction.select_answer_on_training_questionnaire_test_of_static_questionnaire_by_active_user(questiondatas)
            homeaction.admin_user_sign_in(admindatas)
            trainingaction.verify_evaluation_questionnaire_test_result_on_training_result_page(basedatas, training, without_mail_user, training_ques)
            homeaction.delete_admin_account_if_signed(link)
        })
    })

    // it('Login by activated user after getting the password', () => {
    //     cy.visit('/')
    //     cy.log(Cypress.env('my_passwords'))
    //     useraction.login_by_user_after_getting_password(without_mail_user, settingdata)
    //     useraction.login_by_user_after_getting_password_without_logout(without_mail_user, settingdata)
    //     useraction.play_invited_practice_training_by_active_user(without_mail_user, training, training_ques)
    //     questionnaireaction.select_answer_on_training_questionnaire_test_of_static_questionnaire_by_active_user(questiondatas)
    //     homeaction.admin_user_sign_in(admindatas)
    //     trainingaction.verify_practice_questionnaire_test_result_on_training_result_page(basedatas, training, without_mail_user)
    //     trainingaction.invite_training_as_evaluation_to_active_user(basedatas, training, training_ques)
    //     useraction.login_by_user_after_getting_password_without_logout(without_mail_user, settingdata)
    //     useraction.play_invited_evaluation_training_by_active_user(without_mail_user, training, training_ques)
    //     questionnaireaction.select_answer_on_training_questionnaire_test_of_static_questionnaire_by_active_user(questiondatas)
    //     homeaction.admin_user_sign_in(admindatas)
    //     trainingaction.verify_evaluation_questionnaire_test_result_on_training_result_page(basedatas, training, without_mail_user, training_ques)
    //     homeaction.delete_admin_account_if_signed(link)
    // })

    it('2. Create training with cycle & invite to the user ', () => {
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        baseaction.create_base_with_single_domain_and_verify(basedatas);
        baseaction.create_question_on_created_base(basedatas, questiondatas);
        questionnaireaction.create_two_static_type_questionnaire(basedatas, cy_ques);
        questionnaireaction.select_all_question_on_created_two_static_type_questionnaire(basedatas, cy_ques);
        useraction.create_one_user_by_admin(without_mail_user)
        useraction.activate_the_created_user_by_admin_without_logout(without_mail_user)
        useraction.define_boss_for_admin_user(without_mail_user, admindatas)
        surveyaction.create_surveys(long_surveydata);
        surveyaction.create_all_fields(long_surveydata);
        cyclesaction.create_cycles(cycledata);
        cyclesaction.create_steps_into_created_cycles(basedatas,cycledata);
        trainingaction.create_trainings_with_cycle(basedatas,training,admindatas, cycledata);
        trainingaction.drag_cycle_questionnaire_on_practice_section_in_training(basedatas,training, cy_ques)
        trainingaction.add_participants_into_created_training(basedatas,without_mail_user,training);
        trainingaction.invite_selected_participant_on_training(basedatas,without_mail_user,training)
        trainingaction.invite_cycle_for_selected_participant_on_training(basedatas, training, without_mail_user)
        homeaction.admin_user_sign_in(admindatas)
        settingaction.get_password_from_preview_message_on_email_and_log_tab(without_mail_user, mail_sub)
        cy.fixture('user_reg_link').then(function (pwd) {
            let user_password = pwd.activated_user_password
            cy.log('my_stored_user_reg_link_is', user_password)
            useraction.login_by_user_after_getting_password(without_mail_user, settingdata, user_password)
            useraction.login_by_user_after_getting_password_without_logout(without_mail_user, settingdata, user_password)
            useraction.play_invited_practice_cycle_training_by_active_user(without_mail_user, training, cy_ques)
            questionnaireaction.select_answer_on_training_questionnaire_test_of_static_questionnaire_by_active_user(questiondatas)
            useraction.login_by_user_after_getting_password_without_logout(without_mail_user, settingdata, user_password)
            useraction.play_cycle_event_on_user_dashboard_page(cycledata, questiondatas, long_surveydata)
            homeaction.admin_user_sign_in(admindatas)
            trainingaction.play_cycle_event_by_admin(cycledata, questiondatas, long_surveydata)
            homeaction.admin_user_sign_in(admindatas)
            trainingaction.verify_practice_questionnaire_test_result_on_training_result_page(basedatas, training, without_mail_user)
            trainingaction.verify_cycle_event_of_questionnaire_result_on_training_of_cycle_tab(basedatas, training, without_mail_user, admindatas)
            trainingaction.verify_cycle_event_of_survey_result_on_training_page(basedatas, training, long_surveydata)
        })
    })

    // it('Login by activated user after getting the password', () => {
    //     cy.visit('/')
    //     cy.log(Cypress.env('my_passwords'))
    //     useraction.login_by_user_after_getting_password(without_mail_user, settingdata)
    //     useraction.login_by_user_after_getting_password_without_logout(without_mail_user, settingdata)
    //     useraction.play_invited_practice_cycle_training_by_active_user(without_mail_user, training, cy_ques)
    //     questionnaireaction.select_answer_on_training_questionnaire_test_of_static_questionnaire_by_active_user(questiondatas)
    //     useraction.login_by_user_after_getting_password_without_logout(without_mail_user, settingdata)
    //     useraction.play_cycle_event_on_user_dashboard_page(cycledata, questiondatas, long_surveydata)
    //     homeaction.admin_user_sign_in(admindatas)
    //     trainingaction.play_cycle_event_by_admin(cycledata, questiondatas, long_surveydata)
    //     homeaction.admin_user_sign_in(admindatas)
    //     trainingaction.verify_practice_questionnaire_test_result_on_training_result_page(basedatas, training, without_mail_user)
    //     trainingaction.verify_cycle_event_of_questionnaire_result_on_training_of_cycle_tab(basedatas, training, without_mail_user, admindatas)
    //     trainingaction.verify_cycle_event_of_survey_result_on_training_page(basedatas, training, long_surveydata)
    // })


})