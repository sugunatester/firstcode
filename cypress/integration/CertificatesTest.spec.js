import HomeAction from "../Action/HomeAction.js";
import SettingAction from "../Action/SettingAction.js";
import CertificatesAction from "../Action/CertificatesAction";

describe('Certificates',()=>{

    const homeaction = new HomeAction();
    const settingaction = new SettingAction();
    const certificatesaction = new CertificatesAction();

    let admindatas;
    let link;
    let settingdata;



    beforeEach(()=>{
        cy.visit('/')
        cy.viewport(1280, 720);

        cy.fixture("users").then((data) => {
            admindatas = data.tets_beta_admin;
            // admindatas = data.tets_www_admin;
        })

        cy.fixture("common_data").then((data) => {
            link = data.links;
            settingdata = data
        })

    })

    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    })

    it('1. certificate verification',()=>{
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        certificatesaction.open_certificates_module();
        homeaction.delete_admin_account_if_signed(link)

    })


})