import HomeAction from "../Action/HomeAction.js";
import SettingAction from "../Action/SettingAction.js";
import SurveysAction from "../Action/SurveysAction";
import MediaAction from "../Action/MediaAction.js";
import UserAction from "../Action/UserAction.js";

describe('Surveys',()=>{

    const homeaction = new HomeAction();
    const settingaction = new SettingAction();
    const surveysaction = new SurveysAction();
    const mediaaction = new MediaAction();
    const useraction = new UserAction();

    let admindatas;
    let link;
    let settingdata;
    let simple_surveydata;
    let long_surveydata;
    let mediadata;
    let conditiondata;
    let mail_sub;
    let usersdatas;
    let without_mail_user;
    let contactuser;


    beforeEach(()=>{
        cy.viewport(1280, 720);

        cy.fixture("users").then((data) => {
            admindatas = data.tets_beta_admin;
            // admindatas = data.tets_www_admin;
            usersdatas = data.users.tets2020;
            without_mail_user = data.users.Inactive_mail_user;
            contactuser= data.contact_user;

        })

        cy.fixture("common_data").then((data) => {
            link = data.links;
            settingdata = data
            mail_sub = data.mail_subjects

        })

        cy.fixture('form').then((data)=>{
            simple_surveydata = data.forms.simple_form;
            long_surveydata = data.forms.form_1;
            conditiondata = data.forms.condition_form;
        })
        cy.fixture("media_modules").then((data) => {
            mediadata = data.medias.media1.items;
        })
    })

    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    })

    it('1. Create surveys with all fields and play a trial',()=>{
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        mediaaction.create_media_on_mediapage(mediadata)
        surveysaction.create_surveys(long_surveydata);
        surveysaction.create_all_fields(long_surveydata)
        surveysaction.trial_play_on_created_survey(long_surveydata)
        homeaction.delete_admin_account_if_signed(link)

    })

    it('2. Create surveys with condition and play a trial',()=>{
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        mediaaction.create_media_on_mediapage(mediadata)
        surveysaction.create_surveys(conditiondata);
        surveysaction.create_fields_with_condition(conditiondata)
        surveysaction.trial_play_on_condition_survey(conditiondata)
        homeaction.delete_admin_account_if_signed(link)

    })

    it('3. Create and active one user and get the password from the activation email log & ' +
        'Create surveys with all fields, invite to active user and verify the result',()=> {
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        mediaaction.create_media_on_mediapage(mediadata)
        useraction.create_one_user_by_admin(without_mail_user)
        useraction.activate_the_created_user_by_admin(without_mail_user)
        homeaction.admin_user_sign_in(admindatas)
        settingaction.get_password_from_preview_message_on_email_and_log_tab(without_mail_user, mail_sub)
        cy.fixture('user_reg_link').then(function (pwd) {
            let user_password = pwd.activated_user_password
            cy.log('my_stored_user_reg_link_is', user_password)
            useraction.login_by_user_after_getting_password(without_mail_user, settingdata, user_password)
            homeaction.admin_user_sign_in(admindatas)
            surveysaction.create_surveys(long_surveydata);
            surveysaction.create_all_fields(long_surveydata)
            surveysaction.trial_play_on_created_survey(long_surveydata)
            surveysaction.invite_survey_to_activated_user(long_surveydata, without_mail_user)
            useraction.login_by_user_after_getting_password_without_logout(without_mail_user, settingdata, user_password)
            useraction.click_survey_invitation_card_from_user_dashboard_page(long_surveydata)
            surveysaction.play_survey_by_user(long_surveydata)
            homeaction.admin_user_sign_in(admindatas)
            surveysaction.verify_survey_result_after_user_completed_form_test(long_surveydata, without_mail_user)
            homeaction.delete_admin_account_if_signed(link)
        })
    })

    // it('Login by activated user after getting the password', () => {
    //     cy.visit('/')
    //     cy.log(Cypress.env('my_passwords'))
    //     useraction.login_by_user_after_getting_password(without_mail_user, settingdata)
    // })

    // it('Create surveys with all fields, invite to active user and verify the result', () => {
    //     cy.visit('/')
    //     homeaction.admin_user_sign_in(admindatas)
    //     surveysaction.create_surveys(long_surveydata);
    //     surveysaction.create_all_fields(long_surveydata)
    //     surveysaction.trial_play_on_created_survey(long_surveydata)
    //     surveysaction.invite_survey_to_activated_user(long_surveydata, without_mail_user)
    //     useraction.login_by_user_after_getting_password_without_logout(without_mail_user, settingdata)
    //     useraction.click_survey_invitation_card_from_user_dashboard_page(long_surveydata)
    //     surveysaction.play_survey_by_user(long_surveydata)
    //     homeaction.admin_user_sign_in(admindatas)
    //     surveysaction.verify_survey_result_after_user_completed_form_test(long_surveydata,without_mail_user)
    //     homeaction.delete_admin_account_if_signed(link)
    //
    // })

    // it('Random number generated and stored in the json file', () => {
    //     useraction.rand()
    // })

    it('4. Create surveys with all fields, invite to contact user and verify the result',()=>{
        useraction.rand()
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        mediaaction.create_media_on_mediapage(mediadata)
        surveysaction.create_surveys(long_surveydata);
        surveysaction.create_all_fields(long_surveydata)
        surveysaction.trial_play_on_created_survey(long_surveydata)
        let randno
        cy.fixture('data').then(function (datas) {
            randno = datas
            cy.log(randno)
            surveysaction.invite_survey_to_contact_user(long_surveydata, contactuser, randno, mail_sub)
            homeaction.admin_user_sign_in(admindatas)
            settingaction.get_contact_user_of_survey_play_link_on_email_and_log_tab(contactuser, mail_sub, randno)
        })
        cy.fixture('user_reg_link').then(function (reg_link) {
            let user_reg_link = reg_link.user_reg_link
            cy.log('my_stored_user_reg_link_is', user_reg_link)
            cy.visit(user_reg_link)
            surveysaction.play_survey_by_contact_user(long_surveydata, link)
        })
        cy.visit('/')
        homeaction.admin_user_sign_in(admindatas)
        surveysaction.verify_survey_result_after_contactuser_completed_form_test(long_surveydata, contactuser)
        homeaction.delete_admin_account_if_signed(link)
    })


})